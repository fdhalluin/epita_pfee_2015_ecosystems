var colors = {"Power": "blue", "Score": "black", "Funds": "red", "FundsPerSec": "red", "Ecotax": "green", "Population": "brown"};
var range = [-100, 100];
var firsts = {"Power": 2000, "Funds": 30000, "Population": 650};
var log_range = [Math.exp(5), Math.exp(13)];

var vis = d3.select("#visualisation"),
    WIDTH = document.getElementById('visualisation').getAttribute('width');
    HEIGHT = document.getElementById('visualisation').getAttribute('height');
    MARGINS = {
        top: 50,
        right: 20,
        bottom: 50,
        left: 50
    },
    //lSpace = WIDTH/dataGroup.length;

    xScale = d3.scale.linear()
        .range([MARGINS.left, WIDTH - MARGINS.right])
        .domain([0, 180]),
    yScale = d3.scale.linear()
        .range([HEIGHT - MARGINS.top, MARGINS.bottom])
        .domain(range),
    popScale = d3.scale.log()
        .base(Math.E)
        .range([HEIGHT - MARGINS.top, MARGINS.bottom])
        .domain([Math.exp(5), Math.exp(13)]);


function getMaxOfArray(numArray)
{
    return Math.max.apply(null, numArray);
}


function getPurcent(value, type)
{
    var purcent = (value * 100) / firsts[type];
    var res = purcent - 100;
    return res;
}

//on va faire une échelle comme à la bourse avec les ordonnées en pourcentage

function graph(score, funds, power, population, ecotax)
{
    var container = d3.select("#visualisation");
    container.selectAll("g").remove();
    container.selectAll("path").remove();
    container.selectAll("axis").remove();
    container.selectAll("text").remove();

    funds = funds.split(" ");
    power = power.split(" ");
    score = score.split(" ");
    population = population.split(" ");
    data = [];
    power[0] = 2000;
    funds[0] = 30000;
    for (var len = power.length, i = 0; i < len; i++)
    {
        power[i] = getPurcent(power[i], "Power");
        funds[i] = getPurcent(funds[i], "Funds");
        if (power[i] > range[1])
            range[1] += 25;
        if (funds[i] > range[1])
            range[1] += 25;

        if (i == 1 || i == 0)
            population[i] = 650;

        if (score[i] < log_range[0])
            score[i] = log_range[0];

        var tempScore = {
            "Type": "Score",
            "ord": score[i],
            "abs": i
        };
        var tempFunds = {
            "Type": "Funds",
            "ord": funds[i],
            "abs": i
        };
        var tempPower = {
            "Type": "Power",
            "ord": power[i],
            "abs": i
        };
        var tempPopulation = {
            "Type": "Population",
            "ord": population[i],
            "abs": i
        };
        data.push(tempScore);
        data.push(tempPower);
        data.push(tempFunds);
        data.push(tempPopulation);
    }

    var dataGroup = d3.nest()
        .key(function(d) {
            return d.Type;
        })
        .entries(data);

    var superscript = "⁰¹²³⁴⁵⁶⁷⁸⁹",
        formatPower = function(d) { return (d + "").split("").map(function(c) { return superscript[c]; }).join(""); };

    var xAxis = d3.svg.axis()
            .scale(xScale)
            .orient("bottom");
    var yAxis = d3.svg.axis()
            .scale(yScale)
            .orient("left");
    var popAxis = d3.svg.axis()
            .scale(popScale)
            .orient("right")
            .tickFormat(function(d) { return "e" + formatPower(Math.round(Math.log(d))); });

    vis.append("svg:g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")")
        .call(xAxis)
        .append("text")
        .style("text-anchor", "middle")
        //.text("Time (seconds)");

    vis.append("svg:g")
        .attr("class", "axis")
        .attr("id", "yaxis")
        .attr("transform", "translate(" + (MARGINS.left) + ",0)")
        .call(yAxis)
        .append("text")
        .attr("id", "yaxis_label")
        .attr("transform", "rotate(-90)")
        .style("text-anchor", "end")
        .text("%");

    vis.append("svg:g")
        .attr("class", "axis")
        .attr("id", "popaxis")
        .attr("transform", "translate(" + (WIDTH - MARGINS.right) + ",0)")
        .call(popAxis)
        .append("text")
        .attr("id", "popaxis_label")
        .attr("transform", "rotate(-90)")
        .style("text-anchor", "end")
        .text("Population");

    var lineGen = d3.svg.line()
        .x(function(d) { return xScale(d.abs); })
        .y(function(d) { return yScale(d.ord); })
        .interpolate("basis");

    var lineGenPop = d3.svg.line()
        .x(function(d) { return xScale(d.abs); })
        .y(function(d) { return popScale(d.ord); })
        .interpolate("basis");


    dataGroup.forEach(function(d, i) {
            vis.append('svg:path')
                .attr('d', (d.key == "Population" || d.key == "Score") ? lineGenPop(d.values) : lineGen(d.values))
                .attr('stroke', colors[d.key])
                .attr('stroke-width', "1px")
                .attr('id', 'line_'+d.key)
                .attr("opacity", 1)
                .attr('fill', 'none')
                .on('click', "rescale(d.key)")
            vis.append("text")
                .attr("x", ((WIDTH/dataGroup.length) / 2) + i * (WIDTH/dataGroup.length))//(lSpace / 2) + i * lSpace)
                .attr("y", HEIGHT)
                .attr("class", "legend")
                .style("fill", colors[d.key])
                .text(d.key);
    });
}