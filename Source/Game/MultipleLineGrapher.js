var activity = {"Power": true, "Score": true, "Funds": true};


/*
CHAQUE INDICATEUR SUR UN GRAPHE DIFFÉRENT
*/


function getMaxOfArray(numArray)
{
    return Math.max.apply(null, numArray);
}

function graph(score, funds, power)
{
    var container = d3.select("#visualisation1");
    container.selectAll("g").remove();
    container.selectAll("path").remove();
    container = d3.select("#visualisation2");
    container.selectAll("g").remove();
    container.selectAll("path").remove();
    container = d3.select("#visualisation3");
    container.selectAll("g").remove();
    container.selectAll("path").remove();

    var colors = {"Power": "blue", "Score": "black", "Funds": "green"};

    score = score.split(" ");
    funds = funds.split(" ");
    power = power.split(" ");
    var nombre_valeurs_affichees = 20;
    var dataScore = [];
    var dataFunds = [];
    var dataPower = [];

    if (power.length >= nombre_valeurs_affichees)
    {
        for (var len = power.length, i = len - nombre_valeurs_affichees; i < len; i++)
        {
            var tempFunds = {
                "ord": funds[i],
                "abs": i
            };
            var tempPower = {
                "ord": power[i],
                "abs": i
            };
            var tempScore = {
                "ord": score[i],
                "abs": i
            };
            dataScore.push(tempScore);
            dataPower.push(tempPower);
            dataFunds.push(tempFunds);
        }
    }
    else
    {
        for (var i = 0, len = power.length; i < len; i++)
        {
            var tempFunds = {
                "ord": funds[i],
                "abs": i
            };
            var tempPower = {
                "ord": power[i],
                "abs": i
            };
            var tempScore = {
                "ord": score[i],
                "abs": i
            };
            dataScore.push(tempScore);
            dataPower.push(tempPower);
            dataFunds.push(tempFunds);
        }
    }
}