var activity = {"Power": true, "Score": true, "Funds": true};


/*
CHAQUE INDICATEUR SUR UN GRAPHE DIFFÉRENT
*/


function getMaxOfArray(numArray)
{
    return Math.max.apply(null, numArray);
}

function graph(score, funds, power)
{
    var container = d3.select("#visualisation1");
    container.selectAll("g").remove();
    container.selectAll("path").remove();
    container = d3.select("#visualisation2");
    container.selectAll("g").remove();
    container.selectAll("path").remove();
    container = d3.select("#visualisation3");
    container.selectAll("g").remove();
    container.selectAll("path").remove();

    var colors = {"Power": "blue", "Score": "black", "Funds": "green"};

    score = score.split(" ");
    funds = funds.split(" ");
    power = power.split(" ");
    var nombre_valeurs_affichees = 20;
    var dataScore = [];
    var dataFunds = [];
    var dataPower = [];
    var maxPower = 0;
    var maxFunds = 0;
    var maxScore = 0;
    var maxRange = 0;
    if (power.length >= nombre_valeurs_affichees)
    {
        for (var len = power.length, i = len - nombre_valeurs_affichees; i < len; i++)
        {
            if (funds[i] > maxFunds)
                maxFunds = funds[i];
            if (power[i] > maxPower)
                maxPower = power[i];
            if (score[i] > maxScore)
                maxScore = score[i];
            var tempFunds = {
                "Type": "Funds",
                "ord": funds[i],
                "abs": i
            };
            var tempPower = {
                "Type": "Power",
                "ord": power[i],
                "abs": i
            };
            var tempScore = {
                "Type": "Score",
                "ord": score[i],
                "abs": i
            };
            data.push(tempScore);
            data.push(tempPower);
            data.push(tempFunds);
        }
        maxRange = Math.max(Math.max(maxPower, maxScore), maxFunds);
    }
    else
    {
        maxRange = Math.max(Math.max(maxPower, maxScore), maxFunds);
        for (var i = 0, len = power.length; i < len; i++)
        {
            if (funds[i] > maxFunds)
                maxFunds = funds[i];
            if (power[i] > maxPower)
                maxPower = power[i];
            if (score[i] > maxScore)
                maxScore = score[i];
            var tempFunds = {
                "Type": "Funds",
                "ord": funds[i],
                "abs": i
            };
            var tempPower = {
                "Type": "Power",
                "ord": power[i],
                "abs": i
            };
            var tempScore = {
                "Type": "Score",
                "ord": score[i],
                "abs": i
            };
            data.push(tempScore);
            data.push(tempPower);
            data.push(tempFunds);
        }
        maxRange = Math.max(Math.max(maxPower, maxScore), maxFunds);
    }
    var dataGroup = d3.nest()
        .key(function(d) {
            return d.Type;
        })
        .entries(data);
    var vis1 = d3.select("#visualisation1"),
        WIDTH = document.getElementById('visualisation1').getAttribute('width');
        HEIGHT = document.getElementById('visualisation1').getAttribute('height');
        MARGINS = {
            top: 50,
            right: 20,
            bottom: 50,
            left: 50
        },
        lSpace = WIDTH/dataGroup.length;
        xScale = d3.scale.linear()
            .range([MARGINS.left, WIDTH - MARGINS.right])
            .domain([d3.min(data, function(d) {
                return d.abs;
            }), d3.max(data, function(d) {
                return d.abs;
        })]),
        /*yScale = d3.scale.linear()
            .range([HEIGHT - MARGINS.top, MARGINS.bottom])
            .domain([d3.min(data, function(d) {
                return d.ord;
            }), d3.max(data, function(d) {
                return d.ord;
        })]),*/
        yScale = d3.scale.linear()
            .range([HEIGHT - MARGINS.top, MARGINS.bottom])
            .domain([0, maxRange]),
        xAxis = d3.svg.axis()
            .scale(xScale),
        yAxis = d3.svg.axis()
            .scale(yScale)
            .orient("left");

    vis.append("svg:g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")")
        .call(xAxis);

    vis.append("svg:g")
        .attr("class", "axis")
        .attr("transform", "translate(" + (MARGINS.left) + ",0)")
        .call(yAxis);

    var lineGen = d3.svg.line()
        .x(function(d) {
            return xScale(d.abs);
        })
        .y(function(d) {
            return yScale(d.ord);
        })
        .interpolate("basis");

    dataGroup.forEach(function(d, i) {
        vis.append('svg:path')
            .attr('d', lineGen(d.values))
            .attr('stroke', colors[d.key])
            .attr('stroke-width', 2)
            .attr('id', 'line_'+d.key)
            .attr("opacity", activity[d.key] ? 1 : 0)
            .attr('fill', 'none');
        vis.append("text")
            .attr("x", (lSpace / 2) + i * lSpace)
            .attr("y", HEIGHT)
            .attr("class", "legend")
            .style("fill", colors[d.key])
            .on('click', function() {
                var active = activity[d.key] ? false : true;
                activity[d.key] = active;
                var opacity = active ? 0 : 1;
                d3.select("#line_" + d.key).style("opacity", opacity);
            })
            .text(d.key);
    });
}