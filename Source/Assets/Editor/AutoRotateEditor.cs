﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// Use EditorWindow for menus within Unity (e.g. parameters for generating a scene)
public class AutoRotateEditor : EditorWindow
{
	[MenuItem ("PFEE/AutoRotate Editor")]
	static void OpenWindow()
	{
		GetWindow<AutoRotateEditor>("AutoRotate Editor");
	}

	void OnSelectionChange()
	{
		Repaint();
	}

	void OnGUI()
	{
		var selected = Selection.activeGameObject;
		var autoRotate = selected == null ? null : selected.GetComponent<AutoRotate>();
		if (autoRotate == null)
		{
			EditorGUILayout.HelpBox("Select an object with an AutoRotate component.", MessageType.Info);
			return;
		}
		EditorGUILayout.LabelField("Name", autoRotate.name);
		autoRotate.RotationSpeed = EditorGUILayout.Vector3Field("Speed", autoRotate.RotationSpeed);
	}
}
