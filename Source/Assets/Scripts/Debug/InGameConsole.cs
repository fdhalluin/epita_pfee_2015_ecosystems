﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class InGameConsole : WindowContent
{
    public static List<string> history = new List<string>();

    public Text logs;
    public ScrollRect scrollRect;
    public InputField commandLine;

    public InGameConsoleController controller { get; private set; }

    private int _historyIndex;

    protected override void Start()
    {
        base.Start();

        if (commandLine != null)
        {
            commandLine.gameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(commandLine.gameObject, null);
            commandLine.OnPointerClick(new PointerEventData(EventSystem.current));
        }

        _historyIndex = history.Count;
        controller = GetComponentInParent<InGameConsoleController>();
    }

    void Update()
    {
        if (!active)
            return;

        if (Input.GetKeyDown(KeyCode.Return))
            ProcessCommand();
        else if (Input.GetKeyDown(KeyCode.UpArrow) && _historyIndex > 0)
            SetCommandLine(history[--_historyIndex]);
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (_historyIndex < history.Count - 1)
                SetCommandLine(history[++_historyIndex]);
            else if (_historyIndex == history.Count - 1)
            {
                ++_historyIndex;
                SetCommandLine("");
            }
        }
    }

    public override void OnParentClosed()
    {
        base.OnParentClosed();
        controller.ConsoleClosed();
    }

    public void ProcessCommand()
    {
        if (commandLine == null || commandLine.text == null)
            return;

        // Fetch command and log it
        string command = commandLine.text;
        logs.text += "> " + command + "\n";
        history.Add(command);
        _historyIndex = history.Count;

        // Execute command
        Execute(command.Split());

        // Clear command line
        commandLine.text = "";
        EventSystem.current.SetSelectedGameObject(commandLine.gameObject, null);

        // Scroll to bottom
        scrollRect.verticalScrollbar.value = 0;

        // Focus
        commandLine.gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(commandLine.gameObject, null);
        commandLine.OnPointerClick(new PointerEventData(EventSystem.current));
    }

    public void Log(string message)
    {
        if (logs != null)
            logs.text += message + "\n";
    }

    public void Clear()
    {
        logs.text = "";
        scrollRect.verticalScrollbar.value = 0;
    }

    public void Exit()
    {
        parentWindow.Close();
    }

    void SetCommandLine(string str)
    {
        if (commandLine != null && commandLine.text != null)
            commandLine.text = str;
    }

    void Execute(string[] tokens)
    {
        if (tokens.Length == 0)
            return;

        var command = ConsoleCommands.GetCommand(tokens[0]);
        if (command == null)
        {
            Log("Command not found");
            return;
        }

        string options = "";
        var arguments = new List<string>();
        for (int i = 1; i < tokens.Length; ++i)
        {
            if (tokens[i][0] == '-')
                options += tokens[i].Substring(1);
            else
                arguments.Add(tokens[i]);
        }
        options = StringUtils.RemoveDuplicateChars(options);

        command(this, options, arguments);
    }
}
