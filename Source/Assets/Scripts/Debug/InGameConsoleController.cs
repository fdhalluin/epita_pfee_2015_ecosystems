﻿using UnityEngine;
using System.Collections;

public class InGameConsoleController : MonoBehaviour
{
    public KeyCode hotkey = KeyCode.Return;
    public Transform consolePrefab;

    public InGameConsole console { get; private set; }

    static public InGameConsoleController instance { get; private set; }

    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (console == null && Input.GetKeyDown(hotkey))
            TogglePanel.instance.consoleToggle.isOn = true;
    }

    public void OpenConsole()
    {
        Debug.AssertFormat(console == null, "Console already opened!");

        var instance = Instantiate(consolePrefab) as Transform;
        var rectTransform = instance.GetComponent<RectTransform>();
        rectTransform.SetParent(transform);
        rectTransform.localPosition = new Vector3(rectTransform.sizeDelta.x * -.5f, rectTransform.sizeDelta.y * .5f);

        console = instance.GetComponentInChildren<InGameConsole>();
        ConsoleCommands.Init();
    }

    public void CloseConsole()
    {
        Debug.AssertFormat(console != null, "Console already closed!");
        console.Exit();
    }

    public void ConsoleClosed()
    {
        TogglePanel.instance.consoleToggle.isOn = false;
        console = null;
    }
}
