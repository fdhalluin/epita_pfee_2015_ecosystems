﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class WindowResizePanel : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    public Vector2 minSize;

    private Vector2 currentPointerPosition;
    private Vector2 previousPointerPosition;

    private RectTransform canvasRectTransform;
    private RectTransform windowRectTransform;

    void Start()
    {
        var canvas = GetComponentInParent<Canvas>();
        if (canvas != null)
            canvasRectTransform = canvas.transform as RectTransform;

        // Get parent window transform
        windowRectTransform = GetComponentInParent<Window>().transform as RectTransform;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Draw window last
        windowRectTransform.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(windowRectTransform, eventData.position, eventData.pressEventCamera, out previousPointerPosition);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (windowRectTransform == null)
            return;

        var sizeDelta = windowRectTransform.sizeDelta;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(windowRectTransform, eventData.position, eventData.pressEventCamera, out currentPointerPosition);
        var resizeValue = currentPointerPosition - previousPointerPosition;

        sizeDelta += new Vector2(resizeValue.x, -resizeValue.y);
        var maxSize = canvasRectTransform.sizeDelta;

        sizeDelta = new Vector2(Mathf.Clamp(sizeDelta.x, minSize.x, maxSize.x), Mathf.Clamp(sizeDelta.y, minSize.y, maxSize.y));

        windowRectTransform.sizeDelta = sizeDelta;
        previousPointerPosition = currentPointerPosition;
    }
}
