﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

public class ConsoleCommands
{
    public delegate void CommandDelegate(InGameConsole console, string options, List<string> arguments);

    private static Dictionary<string, CommandDelegate> commands;
    private static List<string> commandNames;
    private static bool _isInit = false;

    public static void Init()
    {
        if (_isInit)
            return;

        Type classType = typeof(ConsoleCommands);
        var methodInfoArray = classType.GetMethods(BindingFlags.Static | BindingFlags.NonPublic);

        commands = new Dictionary<string, CommandDelegate>();
        commandNames = new List<string>();
        for (int i = 0; i < methodInfoArray.Length; ++i)
        {
            var methodInfo = methodInfoArray[i];
            string name = methodInfo.Name;
            string key = StringUtils.ToUnderscoreCase(name);

            var command = Delegate.CreateDelegate(typeof(CommandDelegate), methodInfo) as CommandDelegate;
            commands.Add(key, command);
            commandNames.Add(key);
        }

        commandNames.Sort();
        _isInit = true;
    }

    public static CommandDelegate GetCommand(string name)
    {
        if (!_isInit)
            Init();

        if (commands.ContainsKey(name))
            return commands[name];
        return null;
    }

    private static void Help(InGameConsole console, string options, List<string> arguments)
    {
        foreach (var commandName in commandNames)
            console.Log(commandName);
    }


    private static void Clear(InGameConsole console, string options, List<string> arguments)
    {
        console.Clear();
    }

    private static void Exit(InGameConsole console, string options, List<string> arguments)
    {
        console.Exit();
    }

    private static void SetFunds(InGameConsole console, string options, List<string> arguments)
    {
        if (arguments.Count > 1)
        {
            console.Log("ERROR: Invalid argument count. Expected 0 or 1.");
        }
        else
        {
            if (arguments.Count == 0)
                Player.funds = 30000;
            else
            {
                int amount;
                if (int.TryParse(arguments[0], out amount))
                    Player.funds = amount;
                else
                    console.Log("ERROR: Invalid amount.");
            }
        }
    }

    private static void AddFunds(InGameConsole console, string options, List<string> arguments)
    {
        if (arguments.Count > 1)
        {
            console.Log("ERROR: Invalid argument count. Expected 0 or 1.");
        }
        else
        {
            if (arguments.Count == 0)
                Player.funds += 10000;
            else
            {
                int amount;
                if (int.TryParse(arguments[0], out amount))
                    Player.funds += amount;
                else
                    console.Log("ERROR: Invalid amount.");
            }
        }
    }

    private static void SetYear(InGameConsole console, string options, List<string> arguments)
    {
        if (arguments.Count != 1)
        {
            console.Log("ERROR: Invalid argument count. Expected 1.");
        }
        else
        {
            int year;
            if (int.TryParse(arguments[0], out year))
                SeasonController.instance.currentYear = year;
            else
                console.Log("ERROR: Invalid year.");
        }
    }
}
