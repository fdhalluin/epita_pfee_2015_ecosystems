﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class StringUtils
{
    public static string RemoveDuplicateChars(string key)
    {
        string table = "";
        string result = "";

        foreach (char value in key)
        {
            if (table.IndexOf(value) == -1)
            {
                table += value;
                result += value;
            }
        }
        return result;
    }

    public static string ToUnderscoreCase(string input)
    {
        var result = System.Text.RegularExpressions.Regex.Replace(input, "(?<=.)([A-Z])", "_$0", System.Text.RegularExpressions.RegexOptions.None);
        return result.ToLower();
    }

    public static string ToCamelCase(string input)
    {
        string[] words = input.Split('_');
        var sb = new System.Text.StringBuilder();
        for (int i = 0; i < words.Length; ++i)
        {
            string s = words[i];
            string firstLetter = s.Substring(0, 1);
            string rest = s.Substring(1, s.Length - 1);
            sb.Append(firstLetter.ToUpper() + rest);
            if (i < words.Length - 1)
                sb.Append(" ");
        }
        return sb.ToString();
    }
}
