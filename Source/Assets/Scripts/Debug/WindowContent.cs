﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class WindowContent : MonoBehaviour
{
    public Window parentWindow { get; private set; }
    public CanvasGroup canvasGroup { get; private set; }
    public bool active { get; private set; }

    protected virtual void Start()
    {
        parentWindow = GetComponentInParent<Window>();
        Debug.Assert(parentWindow != null, "A window content must have a parent window!");

        canvasGroup = GetComponent<CanvasGroup>();
        Debug.Assert(canvasGroup != null, "A window content must have a canvas group!");

        Activate();
    }

    public void Activate()
    {
        active = true;
        canvasGroup.interactable = true;
    }

    public void Deactivate()
    {
        active = false;
        canvasGroup.interactable = false;
    }

    public void Show()
    {
        canvasGroup.alpha = 1;
        canvasGroup.blocksRaycasts = true;
    }

    public void Hide()
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
    }

    public virtual void OnParentToggleMinimized()
    {
        if (active)
        {
            Deactivate();
            Hide();
        }
        else
        {
            Activate();
            Show();
        }
    }

    public virtual void OnParentClosed() { }
}
