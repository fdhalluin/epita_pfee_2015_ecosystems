﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Window : MonoBehaviour
{
    public WindowContent content;

    public Image background { get; private set; }

    void Awake()
    {
        background = GetComponent<Image>();
    }

    public void SetFocused()
    {
        transform.SetAsLastSibling();
    }
    
    public void ToggleMinimize()
    {
        if (content != null)
            content.OnParentToggleMinimized();

        if (background != null)
            background.enabled = !background.enabled;
    }

    public void Close()
    {
        if (content != null)
            content.OnParentClosed();
        Destroy(gameObject);
    }
}
