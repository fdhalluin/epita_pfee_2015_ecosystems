﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class WindowDragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    private Vector2 initialPointerPosition;

    private RectTransform canvasRectTransform;
    private RectTransform windowRectTransform;

    void Start()
    {
        var canvas = GetComponentInParent<Canvas>();
        if (canvas != null)
            canvasRectTransform = canvas.transform as RectTransform;

        // Get parent window transform
        windowRectTransform = GetComponentInParent<Window>().transform as RectTransform;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Draw window last
        windowRectTransform.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(windowRectTransform, eventData.position, eventData.pressEventCamera, out initialPointerPosition);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (windowRectTransform == null)
            return;

        var pointerPosition = ClampToWindow(eventData);
        Vector2 localPointerPosition;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, pointerPosition, eventData.pressEventCamera, out localPointerPosition))
        {
            windowRectTransform.localPosition = localPointerPosition - initialPointerPosition;
        }
    }

    Vector2 ClampToWindow(PointerEventData eventData)
    {
        var rawPosition = eventData.position;
        var canvasCorner = new Vector3[4];
        canvasRectTransform.GetWorldCorners(canvasCorner);

        float clampedX = Mathf.Clamp(rawPosition.x, canvasCorner[0].x, canvasCorner[2].x);
        float clampedY = Mathf.Clamp(rawPosition.y, canvasCorner[0].y, canvasCorner[2].y);

        return new Vector2(clampedX, clampedY);
    }
}
