﻿using UnityEngine;

public class WindController
{
    public static float minWindLevel;
    public static float maxWindLevel;
    public static float[,] windLevel;
    public static bool isDisplaying = false;

    public static Color lightColor = Color.white;
    public static Color darkColor = new Color(.2f, .5f, .8f);

    public static void InitWindLevels()
    {
        minWindLevel = Mathf.Infinity;
        maxWindLevel = Mathf.NegativeInfinity;

        windLevel = new float[World.gridWidth, World.gridWidth];

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
            {
                int checkedCells = 0;
                float blockedCells = 0;

                float height = World.hexagons[i, j].transform.position.y;
                if (height < 0)
                    height = 0; // For water

                for (int ii = i - 3; ii <= i + 3; ii++)
                    if (ii >= 0 && ii < World.gridWidth)
                        for (int jj = j - 3; jj <= j + 3; jj++)
                        {
                            if (jj >= 0 && jj < World.gridWidth)
                            {
                                checkedCells++;
                                if (World.hexagons[ii, jj].transform.position.y > height)
                                    blockedCells++;
                                else if (World.tiles[ii, jj].descriptor == World.instance.forestDescriptor &&
                                         World.hexagons[ii, jj].transform.position.y == height)
                                    blockedCells += 0.5f;
                            }
                        }
                windLevel[i, j] = 1 - blockedCells / checkedCells;

                maxWindLevel = Mathf.Max(maxWindLevel, windLevel[i, j]);
                minWindLevel = Mathf.Min(minWindLevel, windLevel[i, j]);
            }
    }

    public static void ToggleMap()
    {
        isDisplaying = !isDisplaying;

        if (isDisplaying)
        {
            if (HappinessMap.isDisplaying)
                HappinessMap.ToggleMap();

            float invertWindDelta = 1 / maxWindLevel - minWindLevel;
            if (OverlayHandler.lastOverlay != OverlayType.Wind)
            {
                for (int i = 0; i < World.gridWidth; i++)
                    for (int j = 0; j < World.gridWidth; j++)
                    {
                        float normalizedLevel = (windLevel[i, j] - minWindLevel) * invertWindDelta;
                        OverlayHandler.renderers[i, j].material.color = Color.Lerp(World.hexagons[i, j].GetComponent<Renderer>().material.color, Color.Lerp(lightColor, darkColor, normalizedLevel), 1);
                    }
                OverlayHandler.lastOverlay = OverlayType.Wind;
            }

            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                {
                    OverlayHandler.renderers[i, j].enabled = true;
                    OverlayHandler.crosses[i, j].ForEach(r => r.enabled = false);
                }
        }
        else
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                    OverlayHandler.renderers[i, j].enabled = false;
    }
}
