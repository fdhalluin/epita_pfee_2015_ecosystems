﻿using UnityEngine;
using System.Collections.Generic;

public class World : MonoBehaviour
{
    public static World instance { get; private set; }

    [Header("Recompute")]
    public bool recompute;
    public static bool softReset = false;
    public bool simpleMode;

    [Header("Prefabs")]
    public Object hexagon;
    public Object squareTile;
    public Object road;
    public Object pylonLine;
    public Object river;
    public Object selectionHexa;
    public Object unbuildableCross;
    public Object tree;
    public Transform city;

    [Header("Generation")]
    public AnimationCurve heightCurve;
    public float perlinX;
    public float perlinY;
    public float perlinScale;
    public bool onlyMountains;
    public bool deepenWater;
    public float heightMult;
    public int minCityDistance;

    [Space(10f)]
    public bool squareTiles;

    [Header("Tile Descriptors")]
    public TileDescriptor waterDescriptor;
    public TileDescriptor sandDescriptor;
    public TileDescriptor grassDescriptor;
    public TileDescriptor forestDescriptor;
    public TileDescriptor mountainDescriptor;
    public TileDescriptor snowDescriptor;

    public static GameObject[,] hexagons;
    public static Tile[,] tiles;
    public static int[,] adjacentPlayerBuildings;
    public static bool[,] canContainPlayerBuildings;
    public static int gridWidth = 50;

    public static List<GameObject> cities;
    public static List<int> citiesCoordX;
    public static List<int> citiesCoordY;

    float maxCellHeight;

    public void Awake()
    {
        World.instance = this;
        QualitySettings.SetQualityLevel(0, true);
    }

    void Start()
    {
        RecomputeMap();
    }

    void Update()
    {
        if (recompute)
        {
            recompute = false;

            RecomputeMap();
        }
        if (softReset)
        {
            softReset = false;

            SoftReset();
        }
    }

    void RecomputeMap()
    {
        // Clean up
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in transform)
            children.Add(child.gameObject);
        children.ForEach(DestroyImmediate);

        hexagons = new GameObject[gridWidth, gridWidth];
        tiles = new Tile[gridWidth, gridWidth];
        adjacentPlayerBuildings = new int[gridWidth, gridWidth];
        canContainPlayerBuildings = new bool[gridWidth, gridWidth];

        for (int i = 0; i < gridWidth; i++)
            for (int j = 0; j < gridWidth; j++)
            {
                adjacentPlayerBuildings[i, j] = 0;
                canContainPlayerBuildings[i, j] = true;

                GameObject tileObject = null;
                if (!squareTiles)
                {
                    tileObject = (GameObject)Object.Instantiate(hexagon,
                        new Vector3(i * 1.7f + 0.85f * (j % 2), -2, 1.45f * j),
                        Quaternion.identity);
                }
                else
                {
                    tileObject = (GameObject)Object.Instantiate(squareTile,
                        new Vector3(i, 0, j),
                        Quaternion.identity);
                }
                tileObject.transform.parent = transform;
                hexagons[i, j] = tileObject;

                Tile tile = tileObject.GetComponent<Tile>();
                tile.xIndex = i;
                tile.yIndex = j;
                tiles[i, j] = tile;
            }
                
        GenerateHeightMapPerlin();
        ColorHeightMap();
        CompactMap();
        AddCities();
        PaveRoads();
        OverlayHandler.InitOverlay();
        HappinessMap.InitMap();
        WindController.InitWindLevels();
        if (!simpleMode)
        {
            HexagonController.InitMouseOver();
            SelectionHandler.InitSelectionHandler();
            FoamHandler.AddFoam();
            PylonControler.InitPylonControler();
            RiverHandler.InitRivers();
            GetComponent<EcosystemHandler>().InitEcosystem();
            AddTileDecorations();
        }

        Player.hasStartedPlaying = false;
    }

    void SoftReset()
    {
        // Clean up
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in transform)
            if (!child.CompareTag("Keep On Soft Reset"))
                children.Add(child.gameObject);
        children.ForEach(DestroyImmediate);
        FoamHandler.foams.RemoveAll(obj => obj == null);

        adjacentPlayerBuildings = new int[gridWidth, gridWidth];
        canContainPlayerBuildings = new bool[gridWidth, gridWidth];
        for (int i = 0; i < gridWidth; i++)
            for (int j = 0; j < gridWidth; j++)
            {
                adjacentPlayerBuildings[i, j] = 0;
                canContainPlayerBuildings[i, j] = true;
            }

        AddCities();
        HappinessMap.InitMap();
        if (!simpleMode)
        {
            HexagonController.InitMouseOver();
            SelectionHandler.InitSelectionHandler();
            FoamHandler.AddFoam();
            PylonControler.InitPylonControler();
            RiverHandler.InitRivers();
            GetComponent<EcosystemHandler>().InitEcosystem();
            AddTileDecorations();
        }

        Player.hasStartedPlaying = false;
        Resources.UnloadUnusedAssets(); // Clean up anything that may have been forgotten
    }

    void AddCities()
    {
        cities = new List<GameObject>();
        citiesCoordX = new List<int>();
        citiesCoordY = new List<int>();

        for (int i = 5; i < gridWidth - 5; i++)
            for (int j = 5; j < gridWidth - 5; j++)
            {
                Transform t = hexagons[i, j].transform;
                Tile ti = tiles[i, j];

                bool isSurroundedByGrass = true;
                foreach (Tile tile in ti.GetNeighbors())
                {
                    if (tile.descriptor != grassDescriptor)
                    {
                        isSurroundedByGrass = false;
                        break;
                    }
                }

                if (isSurroundedByGrass)
                {

                    bool farFromOtherCities = true;
                    foreach (GameObject c in cities)
                        if (Mathf.Abs(c.transform.position.x - t.position.x) < minCityDistance && Mathf.Abs(c.transform.position.z - t.position.z) < minCityDistance)
                        {
                            farFromOtherCities = false;
                            break;
                        }

                    if (farFromOtherCities)
                    {
                        GameObject newCity = ti.AddBuilding(city, true);
                        City c = ti.building as City;
                        c.masterCity = c;
                        c.citiesInGroup = new List<City>{ c };
                        cities.Add(newCity);
                        citiesCoordX.Add(i);
                        citiesCoordY.Add(j);
                    }
                }
            }
    }

    void PaveRoads()
    {
        int len = cities.Count;
        for (int i = 1; i < len; i++)
            for (int j = 0; j < i; j++)
            {
                if (Mathf.Abs(citiesCoordX[i] - citiesCoordX[j]) < 20 && Mathf.Abs(citiesCoordY[i] - citiesCoordY[j]) < 20)
                {
                    PathfindingNode path = HexagonHelper.GetShortestPath(citiesCoordX[i], citiesCoordY[i], citiesCoordX[j], citiesCoordY[j]);
                    if (path != null)
                    {
                        tiles[path.x, path.y].containsRoad = true;
                        while (path.prevNode != null)
                        {
                            tiles[path.prevNode.x, path.prevNode.y].containsRoad = true;

                            float pX1 = hexagons[path.x, path.y].transform.position.x;
                            float pX2 = hexagons[path.prevNode.x, path.prevNode.y].transform.position.x;
                            float pY1 = hexagons[path.x, path.y].transform.position.z;
                            float pY2 = hexagons[path.prevNode.x, path.prevNode.y].transform.position.z;
                            float avgX = (pX1 + pX2) / 2;
                            float avgY = (pY1 + pY2) / 2;

                            GameObject newRoad = (GameObject)Object.Instantiate(road, 
                                                     new Vector3(avgX, 1.45f, avgY),
                                                     Quaternion.identity);
                            newRoad.transform.eulerAngles = new Vector3(0, Mathf.Atan2(pX2 - pX1, pY2 - pY1) * Mathf.Rad2Deg, 0);
                            newRoad.transform.parent = transform;
                            newRoad.GetComponent<Renderer>().material.color = Color.gray;

                            path = path.prevNode;
                        }
                    }
                }
            }
    }

    void CompactMap()
    {
        if (onlyMountains)
        {
            float threshold = 0.5f * maxCellHeight;
            float waterThreshold = -0.2999f * maxCellHeight;
            for (int i = 0; i < gridWidth; i++)
                for (int j = 0; j < gridWidth; j++)
                {
                    Vector3 old = World.hexagons[i, j].transform.position;
                    if (old.y < threshold)
                    {
                        if (old.y < waterThreshold)
                            World.hexagons[i, j].transform.position = new Vector3(old.x, -0.5f, old.z);
                        else
                            World.hexagons[i, j].transform.position = new Vector3(old.x, 0, old.z);
                    }
                    else
                        World.hexagons[i, j].transform.position = new Vector3(old.x, (old.y - threshold + 0.2f) * heightMult, old.z);
                }
        }
        else
        {
            for (int i = 0; i < gridWidth; i++)
                for (int j = 0; j < gridWidth; j++)
                {
                    Vector3 old = World.hexagons[i, j].transform.position;
                    World.hexagons[i, j].transform.position = new Vector3(old.x, old.y * heightMult, old.z);
                }
        }
    }

    void GenerateHeightMapPeaks()
    {
        for (int peak = 0; peak < 10; peak++)
        {
            int i2 = Random.Range(0, gridWidth);
            int j2 = Random.Range(0, gridWidth);
            float hMult = Random.Range(2f, 10f);

            for (int i = 0; i < gridWidth; i++)
                for (int j = 0; j < gridWidth; j++)
                {
                    float dist = HexagonHelper.DistanceFromIndex(i, j, i2, j2);
                    World.hexagons[i, j].transform.Translate(new Vector3(0,
                            hMult * 20 / (20 + dist),
                            0));
                }
        }

        CenterHeightMap();
    }

    void GenerateHeightMapPerlin()
    {
        for (int i = 0; i < gridWidth; i++)
            for (int j = 0; j < gridWidth; j++)
                World.hexagons[i, j].transform.Translate(new Vector3(0,
                        5 * heightCurve.Evaluate(Mathf.PerlinNoise(perlinX + i * perlinScale, perlinY + j * perlinScale)),
                        0));

        CenterHeightMap();
    }

    void CenterHeightMap()
    {
        float avgHeight = 0;
        for (int i = 0; i < gridWidth; i++)
            for (int j = 0; j < gridWidth; j++)
                avgHeight += World.hexagons[i, j].transform.position.y;

        avgHeight /= (gridWidth * gridWidth);
        Vector3 translate = new Vector3(0, -avgHeight, 0);

        for (int i = 0; i < gridWidth; i++)
            for (int j = 0; j < gridWidth; j++)
            {
                World.hexagons[i, j].transform.Translate(translate);
                if (World.hexagons[i, j].transform.position.y > maxCellHeight)
                    maxCellHeight = World.hexagons[i, j].transform.position.y;
            }
    }

    void ColorHeightMap()
    {
        float waterThreshold = -0.3f * maxCellHeight;
        float sandThreshold = -0.2f * maxCellHeight;
        float lightGrassThreshold = 0.3f * maxCellHeight;
        float darkGrassThreshold = 0.5f * maxCellHeight;
        float stoneThreshold = 0.8f * maxCellHeight;

        for (int i = 0; i < gridWidth; i++)
            for (int j = 0; j < gridWidth; j++)
            {
                GameObject obj = hexagons[i, j];
                Tile ti = tiles[i, j];
                Transform t = obj.transform;
                float h = t.position.y;

                if (h < waterThreshold)
                {
                    ti.descriptor = waterDescriptor;
                    t.position = new Vector3(t.position.x, waterThreshold, t.position.z);
                }
                else if (h < sandThreshold)
                    ti.descriptor = sandDescriptor;
                else if (h < lightGrassThreshold)
                    ti.descriptor = grassDescriptor;
                else if (h < darkGrassThreshold)
                    ti.descriptor = forestDescriptor;
                else if (h < stoneThreshold)
                    ti.descriptor = mountainDescriptor;
                else
                    ti.descriptor = snowDescriptor;

                obj.GetComponent<MeshRenderer>().material = ti.descriptor.material;
            }
    }

    void AddTileDecorations()
    {
        for (int i = 0; i < gridWidth; i++)
            for (int j = 0; j < gridWidth; j++)
            {
                Tile ti = tiles[i, j];

                ti.tileDecorations.ForEach(Destroy);

                if (ti.descriptor == forestDescriptor)
                {
                    GameObject newTree = (GameObject)Object.Instantiate(tree,
                                             new Vector3(i * 1.7f + 0.85f * (j % 2), 1.49f, 1.45f * j),
                                             Quaternion.identity);
                    newTree.transform.parent = transform;
                    ti.tileDecorations.Add(newTree);
                }
            }
    }

    public Tile GetTile(int x, int y)
    {
        if (x < 0 || x >= gridWidth || y < 0 || y >= gridWidth)
            return null;
        return tiles[x, y];
    }

    public static TileDescriptor GetGrassDescriptor()
    {
        return instance.grassDescriptor;
    }
}
