﻿using UnityEngine;

public class BuildingGhost : MonoBehaviour
{
    public Transform buildingPrefab;
    public Mesh defaultMesh;

    public Material buildableMaterial;
    public Material unbuildableMaterial;

    void Start()
    {
        var mesh = buildingPrefab.GetComponent<PlayerBuilding>().descriptor.mesh;

        if (mesh == null)
            mesh = defaultMesh;

        GetComponent<MeshFilter>().mesh = mesh;
    }

    public void MoveToTile(Tile tile)
    {
        Vector3 tilePosition = tile.transform.position;
        transform.position = new Vector3(tilePosition.x, tilePosition.y + 1.5f, tilePosition.z);

        var meshRender = GetComponent<MeshRenderer>();

        if (tile.canPlaceBuilding(buildingPrefab) && !tile.containsRoad && !tile.isOccupied)
            meshRender.material = buildableMaterial;
        else
            meshRender.material = unbuildableMaterial;
    }
}
