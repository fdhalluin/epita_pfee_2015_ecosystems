﻿using UnityEngine;
using System.Collections.Generic;

public static class HexagonHelper
{
    public static float DistanceFromIndex(int i1, int j1, int i2, int j2)
    {
        float x1 = i1 * 1.7f + 0.85f * (j1 % 2);
        float y1 = 1.45f * j1;
        float x2 = i2 * 1.7f + 0.85f * (j2 % 2);
        float y2 = 1.45f * j2;

        return Mathf.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static float DistanceFromIndexSquared(int i1, int j1, int i2, int j2)
    {
        float x1 = i1 * 1.7f + 0.85f * (j1 % 2);
        float y1 = 1.45f * j1;
        float x2 = i2 * 1.7f + 0.85f * (j2 % 2);
        float y2 = 1.45f * j2;

        return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    }

    public static float DistanceToPoint(int i1, int j1, float x2, float y2)
    {
        GameObject hexagon = World.hexagons[i1, j1];
        float x1 = hexagon.transform.position.x;
        float y1 = hexagon.transform.position.z;

        return Mathf.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static PathfindingNode GetShortestPath(int i1, int j1, int i2, int j2)
    {
        Stack<Tile> toHandle = new Stack<Tile>();
        toHandle.Push(World.tiles[i1, j1]);
 
        Dictionary<Tile, PathfindingNode> pathInfo = new Dictionary<Tile, PathfindingNode>();

        pathInfo.Add(World.tiles[i1, j1], new PathfindingNode(0, null, i1, j1));
    
        while (toHandle.Count > 0)
        {
            Tile handling = toHandle.Pop();
            int dist = pathInfo[handling].shortestDistance;

            if (dist < 20 && handling.xIndex < World.gridWidth && handling.yIndex < World.gridWidth)
            {
                foreach (Tile neighbour in handling.GetNeighbors())
                    if (neighbour.descriptor == World.GetGrassDescriptor())
                    {
                        if (!pathInfo.ContainsKey(neighbour))
                        {
                            pathInfo.Add(neighbour, new PathfindingNode(dist + 1, pathInfo[handling], neighbour.xIndex, neighbour.yIndex));
                            toHandle.Push(neighbour);
                        }
                        else if (dist + 1 < pathInfo[neighbour].shortestDistance)
                        {
                            pathInfo[neighbour] = new PathfindingNode(dist + 1, pathInfo[handling], neighbour.xIndex, neighbour.yIndex);
                            toHandle.Push(neighbour);
                        }
                    }
            }
        }

        return pathInfo.ContainsKey(World.tiles[i2, j2]) ? pathInfo[World.tiles[i2, j2]] : null;
    }
}

public class PathfindingNode
{
    public int shortestDistance;
    public PathfindingNode prevNode;
    public int x;
    public int y;

    public PathfindingNode(int dist, PathfindingNode prev, int px, int py)
    {
        shortestDistance = dist;
        prevNode = prev;
        x = px;
        y = py;
    }
}
