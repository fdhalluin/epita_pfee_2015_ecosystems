﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour
{
    public float minDuration;
    public float maxDuration;

    private float lived_ = 0f;
    
    void Update()
    {
        lived_ += Time.deltaTime;
        if (lived_ < minDuration)
            return;

        if (lived_ > maxDuration || Input.anyKeyDown)
            Destroy(gameObject);
    }
}
