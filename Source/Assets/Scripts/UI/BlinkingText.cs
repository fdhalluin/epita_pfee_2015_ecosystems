﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlinkingText : MonoBehaviour
{
    public float blinkSpeed;
    [Range(0f, 1f)]
    public float minAlpha;

    private float time_;
    private Text text_;
    private Color textColor_;

    void Awake()
    {
        time_ = 0;
        text_ = GetComponent<Text>();
        textColor_ = text_.color;
    }

    void Update()
    {
        time_ += Time.deltaTime * blinkSpeed;
        textColor_.a = minAlpha + (1 - minAlpha) * Mathf.Abs(Mathf.Sin(time_));
        text_.color = textColor_;
    }
}
