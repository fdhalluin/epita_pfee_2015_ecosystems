﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerEnergyDisplay : MonoBehaviour
{
    public Image energyBar;
    public Text energyText;
    public Text incomeText;

    public Color positiveIncomeColor;
    public Color negativeIncomeColor;

    public static bool forceDisplay = false;

    private float _maxWidth;
    private float _fillAmount;

    private Rect _screenRect;
    private Rect _displayRect;

    void Start()
    {
        _maxWidth = energyBar.rectTransform.sizeDelta.x;
        _fillAmount = 1;

        var rect = GetComponent<RectTransform>().rect;
        _screenRect = new Rect(transform.position, rect.size);
        _screenRect.x = _screenRect.x - _screenRect.width * .33f;
        _screenRect.y = _screenRect.y - _screenRect.height * .5f;

        _displayRect = new Rect(_screenRect);
        _displayRect.y = Screen.height - _displayRect.y + 65;
        _displayRect.height = 70;
        _displayRect.width *= .66f;
    }

    // Update is called once per frame
    void Update()
    {
        float fillAmount = Player.currentPower / Player.maximumPower;
        _fillAmount = Mathf.Lerp(_fillAmount, fillAmount, 0.3f);
        FillBar(energyBar, _fillAmount);

        if (energyText != null)
            energyText.text = string.Format("{0}/{1}kW", Player.currentPower.ToString("F0"), Player.maximumPower.ToString("F0"));

        float income = Player.powerProductionAtLastTick - Player.powerConsumptionAtLastTick;
        income *= Player.tickFrequency;
        if (income < 0)
        {
            incomeText.text = Mathf.RoundToInt(-income).ToString("-# ##0") + "kW/s";
            incomeText.color = negativeIncomeColor;
        }
        else
        {
            incomeText.text = Mathf.RoundToInt(income).ToString("+# ##0") + "kW/s";
            incomeText.color = positiveIncomeColor;
        }
    }

    void OnGUI()
    {
        bool isMouseOver = _screenRect.Contains(Input.mousePosition);
        if (isMouseOver || forceDisplay)
        {
            float diff = Player.powerProductionAtLastTick - Player.powerConsumptionAtLastTick;
            diff *= Player.tickFrequency;
            string diffStr;
            if (diff >= 0)
                diffStr = "<color=lime>" + Mathf.RoundToInt(diff) + "kW/s</color>";
            else
                diffStr = "<color=red>" + Mathf.RoundToInt(diff) + "kW/s</color>";

            GUI.skin.box.alignment = TextAnchor.UpperLeft;
            GUI.Box(_displayRect,
                "<b>Production:" +
                "\nConsumption:" +
                "\n---------------------------------" +
                "\nBalance:</b>");
            GUI.skin.box.alignment = TextAnchor.UpperRight;
            GUI.Box(_displayRect,
                "<b><color=lime>" + Mathf.RoundToInt(Player.powerProductionAtLastTick * Player.tickFrequency) + "kW/s</color>" +
                "\n<color=red>" + Mathf.RoundToInt(Player.powerConsumptionAtLastTick * Player.tickFrequency) + "kW/s</color>" +
                "\n\n" + diffStr + "</b>");
        }
        GUI.skin.box.alignment = TextAnchor.UpperLeft;
    }

    protected void FillBar(Image bar, float amount)
    {
        Debug.Assert(amount >= 0 && amount <= 1);

        if (bar.type == Image.Type.Filled)
            bar.fillAmount = amount;
        else
        {
            float height = bar.rectTransform.sizeDelta.y;
            bar.rectTransform.sizeDelta = new Vector2(_maxWidth * amount, height);
        }
    }
}
