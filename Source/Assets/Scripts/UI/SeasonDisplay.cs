﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SeasonDisplay : MonoBehaviour
{
    [Header("Color wheel")]
    public Image fillBar;
    public Color winterTint;
    public Color springTint;
    public Color summerTint;
    public Color fallTint;

    [Header("Season icon")]
    public Image seasonIcon;
    public Sprite winterIcon;
    public Sprite springIcon;
    public Sprite summerIcon;
    public Sprite fallIcon;

    private int season;

    void Start()
    {
        ChangeSeason();
    }

    void Update()
    {
        if (season != SeasonController.instance.currentSeason)
            ChangeSeason();
        fillBar.fillAmount = SeasonController.instance.timeOfSeason;
    }

    void ChangeSeason()
    {
        season = SeasonController.instance.currentSeason;
        if (season == 0)
        {
            fillBar.color = springTint;
            seasonIcon.sprite = springIcon;
        }
        else if (season == 1)
        {
            fillBar.color = summerTint;
            seasonIcon.sprite = summerIcon;
        }
        else if (season == 2)
        {
            fillBar.color = fallTint;
            seasonIcon.sprite = fallIcon;
        }
        else
        {
            fillBar.color = winterTint;
            seasonIcon.sprite = winterIcon;
        }

    }
}
