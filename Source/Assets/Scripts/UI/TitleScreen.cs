﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class TitleScreen : MonoBehaviour
{
    public List<GameObject> splashScreenPrefabs;

    void Start()
    {
        var splashScreenController = SplashScreenController.instance;
        foreach (var splashScreenPrefab in splashScreenPrefabs)
            splashScreenController.EnqueueSplashScreen(splashScreenPrefab);
    }

    void Update()
    {
        if (!SplashScreenController.instance.isActive() && Input.anyKeyDown)
            SceneManager.LoadScene("Grid", LoadSceneMode.Single);
    }
}
