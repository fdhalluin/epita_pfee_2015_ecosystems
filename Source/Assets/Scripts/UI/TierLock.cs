﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TierLock : MonoBehaviour
{
    public PlayerBuildingDescriptor.Tier lockedTier;
    public int unlockYear;
    public Image unlockWheel;
    public Text unlockText;

    private float timeToUnlock;
    private float invertUnlockDuration;

    // Use this for initialization
    void Start()
    {
        // Make sure lock is on top
        transform.SetAsLastSibling();

        ComputeTimeToUnlock();
        invertUnlockDuration = 1 / timeToUnlock;

        UpdateTimer();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToUnlock < 0)
            Destroy(gameObject);
        else
        {
            ComputeTimeToUnlock();
            unlockWheel.fillAmount = timeToUnlock * invertUnlockDuration;
            UpdateTimer();
        }
    }

    void ComputeTimeToUnlock()
    {
        var seasonController = SeasonController.instance;
        float remainingYears = unlockYear - (seasonController.currentYear + seasonController.timeOfYear);
        timeToUnlock = remainingYears * SeasonController.instance.seasonDuration * 4;
    }

    void UpdateTimer()
    {
        string minutes = Mathf.FloorToInt(timeToUnlock / 60f).ToString("00");
        string seconds = Mathf.FloorToInt(timeToUnlock % 60f).ToString("00");
        unlockText.text = string.Format("{0}:{1}", minutes, seconds);
    }
}
