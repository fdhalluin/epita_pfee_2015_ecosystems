﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuildMenuControler : MonoBehaviour
{
    public Player player;
    public Transform buttonPrefab;
    public Transform tierLockPrefab;

    public GameObject tier1Panel;
    public GameObject tier2Panel;
    public GameObject tier3Panel;

    public int tier2UnlockYear;
    public int tier3UnlockYear;

    private int lastUpdateYear;
    private GameObject tier2Lock;
    private GameObject tier3Lock;

    void Start()
    {
        foreach (Transform buildingPrefab in player.availableBuildings)
        {
            PlayerBuildingDescriptor descriptor = buildingPrefab.GetComponent<PlayerBuilding>().getDescriptor as PlayerBuildingDescriptor;

            GameObject obj = Instantiate(buttonPrefab).gameObject;

            if (descriptor.tier == PlayerBuildingDescriptor.Tier.Tier1)
                obj.transform.SetParent(tier1Panel.transform);
            else if (descriptor.tier == PlayerBuildingDescriptor.Tier.Tier2)
                obj.transform.SetParent(tier2Panel.transform);
            else
                obj.transform.SetParent(tier3Panel.transform);


            var buildingButton = obj.GetComponent<BuildingButton>();
            buildingButton.buildingPrefab = buildingPrefab;

            buildingButton.buildingCost.text = "$" + descriptor.cost.ToString() + "k";
            buildingButton.buildingIcon.sprite = descriptor.icon;
        }

        LockTiers();
    }

    void Update()
    {
        if (World.softReset)
            LockTiers();

        if (tier2Lock == null)
            SetTierPanelActive(tier2Panel.GetComponent<CanvasGroup>(), true);

        if (tier3Lock == null)
            SetTierPanelActive(tier3Panel.GetComponent<CanvasGroup>(), true);
    }

    void LockTiers()
    {
        SetTierPanelActive(tier2Panel.GetComponent<CanvasGroup>(), false);
        SetTierPanelActive(tier3Panel.GetComponent<CanvasGroup>(), false);

        if (tier2Lock == null)
        {
            tier2Lock = Instantiate(tierLockPrefab).gameObject;
            tier2Lock.transform.SetParent(tier2Panel.transform, false);
            var tier2LockComponent = tier2Lock.GetComponent<TierLock>();
            tier2LockComponent.lockedTier = PlayerBuildingDescriptor.Tier.Tier2;
            tier2LockComponent.unlockYear = tier2UnlockYear;
        }

        if (tier3Lock == null)
        {
            tier3Lock = Instantiate(tierLockPrefab).gameObject;
            tier3Lock.transform.SetParent(tier3Panel.transform, false);
            var tier3LockComponent = tier3Lock.GetComponent<TierLock>();
            tier3LockComponent.lockedTier = PlayerBuildingDescriptor.Tier.Tier3;
            tier3LockComponent.unlockYear = tier3UnlockYear;
        }
    }

    void SetTierPanelActive(CanvasGroup panel, bool active)
    {
        panel.interactable = active;
        panel.blocksRaycasts = active;
    }

    public bool IsTierUnlocked(PlayerBuildingDescriptor.Tier tier)
    {
        if (tier == PlayerBuildingDescriptor.Tier.Tier1)
            return true;
        else if (tier == PlayerBuildingDescriptor.Tier.Tier2)
            return tier2Lock == null;
        return tier3Lock == null;
    }
}
