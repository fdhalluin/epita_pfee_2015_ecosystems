﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerFundDisplay : MonoBehaviour
{
    public Player player;

    public Text currentFundsText;
    public Text incomeText;

    public Color positiveIncomeColor;
    public Color negativeIncomeColor;
    public static bool forceDisplay = false;

    private Rect _screenRect;
    private Rect _displayRect;

    void Start()
    {
        Debug.Assert(player != null);

        var rect = GetComponent<RectTransform>().rect;
        _screenRect = new Rect(transform.position, rect.size);
        _screenRect.x = _screenRect.x - _screenRect.width * .33f;
        _screenRect.y = _screenRect.y - _screenRect.height * .5f;

        _displayRect = new Rect(_screenRect);
        _displayRect.y = Screen.height - _displayRect.y + 4;
        _displayRect.height = 115;
        _displayRect.width *= .66f;
    }

    // Update is called once per frame
    void Update()
    {
        currentFundsText.text = Mathf.RoundToInt(Player.funds).ToString("### ### ### ###") + "k";

        float income = -Player.upkeepAtLastTick - Player.ecoTaxAtLastTick + Player.productionAtLastTick + player.baseIncome * Player.gameSpeed;
        income *= Player.tickFrequency;
        if (income < 0)
        {
            incomeText.text =Mathf.RoundToInt(-income).ToString("-$## ##0") + "k/s";
            incomeText.color = negativeIncomeColor;
        }
        else
        {
            incomeText.text =Mathf.RoundToInt(income).ToString("+$## ##0") + "k/s";
            incomeText.color = positiveIncomeColor;
        }
    }

    void OnGUI()
    {
        bool isMouseOver = _screenRect.Contains(Input.mousePosition);
        if (isMouseOver || forceDisplay)
        {
            float diff = -Player.upkeepAtLastTick - Player.ecoTaxAtLastTick + Player.productionAtLastTick + player.baseIncome * Player.gameSpeed;
            diff *= Player.tickFrequency;
            string diffStr;
            if (diff >= 0)
                diffStr = "<color=lime>$" + Mathf.RoundToInt(diff) + "k/s</color>";
            else
                diffStr = "<color=red>$" + Mathf.RoundToInt(diff) + "k/s</color>";

            GUI.skin.box.alignment = TextAnchor.UpperLeft;
            GUI.Box(_displayRect, 
                "<b>Base Income:" +
                "\nCity Income:" +
                "\nPower sold:" +
                "\nUpkeep:" +
                "\nEco Tax:" +
                "\n---------------------------------" +
                "\nBalance:</b>");
            GUI.skin.box.alignment = TextAnchor.UpperRight;
            GUI.Box(_displayRect, 
                "<b><color=lime>$" + Mathf.RoundToInt(player.baseIncome * Player.gameSpeed * Player.tickFrequency) + "k/s</color>" +
                "\n<color=lime>$" + Mathf.RoundToInt(Player.productionAtLastTick * Player.tickFrequency) + "k/s</color>" +
                "\n<color=lime>$" + Mathf.RoundToInt(Player.powerSoldAtLastTick * Player.tickFrequency) + "k/s</color>" +
                "\n<color=red>$" + Mathf.RoundToInt(Player.upkeepAtLastTick * Player.tickFrequency) + "k/s</color>" +
                "\n<color=red>$" + Mathf.RoundToInt(Player.ecoTaxAtLastTick * Player.tickFrequency) + "k/s</color>" +
                "\n\n" + diffStr + "</b>");
        }
        GUI.skin.box.alignment = TextAnchor.UpperLeft;
    }
}
