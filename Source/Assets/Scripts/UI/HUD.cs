﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour
{
    public static string boxText = null;
    public static string smallBoxText = null;
    public static int unmouse = -1;

    void OnGUI()
    {
        if (boxText != null)
        {
            GUI.Box(new Rect(Input.mousePosition.x + 30, Screen.height - Input.mousePosition.y - 25, 140, 70), "");
            GUI.Box(new Rect(Input.mousePosition.x + 30, Screen.height - Input.mousePosition.y - 25, 140, 70), boxText);
            if (unmouse-- < 0)
            {
                boxText = null;
                smallBoxText = null;
            }
        }
        else if (smallBoxText != null)
        {
            GUI.Box(new Rect(Input.mousePosition.x + 30, Screen.height - Input.mousePosition.y - 10, 120, 25), "");
            GUI.Box(new Rect(Input.mousePosition.x + 30, Screen.height - Input.mousePosition.y - 10, 120, 25), smallBoxText);
            if (unmouse-- < 0)
                smallBoxText = null;
        }
    }
}
