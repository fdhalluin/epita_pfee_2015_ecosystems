﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverPanel : MonoBehaviour
{
    static public GameOverPanel instance;
    public Text earningsText;
    public Text populationText;
    public bool isGameOver;

    private CanvasGroup _canvasGroup;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
        Hide();
    }

    public void Hide()
    {
        _canvasGroup.alpha = 0;
        _canvasGroup.blocksRaycasts = false;
        _canvasGroup.interactable = false;
    }

    public void Show()
    {
        _canvasGroup.alpha = 1;
        _canvasGroup.blocksRaycasts = true;
        _canvasGroup.interactable = true;
        Player.gameSpeed = 0;

        if (earningsText != null)
            earningsText.text = string.Format("Overall earninings: {0}k$", Mathf.FloorToInt(Player.score));
        if (populationText != null)
            populationText.text = string.Format("Final population: {0}k", Player.totalPopulation);

        isGameOver = true;
    }

    public void PlayAgain()
    {
        World.softReset = true;
        Player.InitiatePlayer();
        Hide();

        isGameOver = false;
        Player.gameSpeed = 1;
    }
}
