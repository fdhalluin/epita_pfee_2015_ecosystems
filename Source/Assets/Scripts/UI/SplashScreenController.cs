﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SplashScreenController : MonoBehaviour
{
    public static SplashScreenController instance { get; private set; }

    public CanvasGroup background;

    private  Queue<GameObject> splashScreenPrefabs_ = new Queue<GameObject>();
    private Transform splashScreen_;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        splashScreen_ = null;
        SetBackgroundActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (splashScreen_ == null)
        {
            if (splashScreenPrefabs_.Count > 0)
                DequeueSplashScreen();
            else
                SetBackgroundActive(false);
        }
    }

    public void EnqueueSplashScreen(GameObject prefab)
    {
        splashScreenPrefabs_.Enqueue(prefab);
    }

    public bool isActive()
    {
        return splashScreen_ != null;
    }

    private void SetBackgroundActive(bool active)
    {
        background.alpha = active ? 1 : 0;
        background.blocksRaycasts = active;
    }

    private void DequeueSplashScreen()
    {
        var splashScreenPrefab = splashScreenPrefabs_.Dequeue();
        splashScreen_ = Instantiate(splashScreenPrefab).transform;
        splashScreen_.SetParent(background.transform);
        splashScreen_.localPosition = Vector3.zero;
        SetBackgroundActive(true);
    }
}
