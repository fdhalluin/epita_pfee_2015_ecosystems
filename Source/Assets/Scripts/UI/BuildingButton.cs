﻿using UnityEngine;
using UnityEngine.UI;

public class BuildingButton : MonoBehaviour
{
    public Transform buildingPrefab;
    public Transform buildingGhostPrefab;

    public Text buildingCost;
    public Image buildingIcon;
    public Image buildingHappinessDisplay;

    private Button button;
    private PlayerBuilding playerBuilding;

    void Start()
    {
        button = GetComponent<Button>();
        playerBuilding = buildingPrefab.GetComponent<PlayerBuilding>();

        float unhapinessFactor = playerBuilding.descriptor.unhapinessFactor;
        if (unhapinessFactor < 3)
            buildingHappinessDisplay.color = Color.green;
        else if (unhapinessFactor < 8)
            buildingHappinessDisplay.color = Color.yellow;
        else if (unhapinessFactor < 15)
            buildingHappinessDisplay.color = Color.Lerp(Color.yellow, Color.red, 0.5f);
        else
            buildingHappinessDisplay.color = Color.red;
    }

    void Update()
    {
        button.interactable = playerBuilding.CanBeAfforded();

        if (HexagonController.buildingGhost == null && OverlayHandler.lastOverlay == OverlayType.Building)
        {
            OverlayHandler.lastOverlay = OverlayType.None;
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                    OverlayHandler.crosses[i, j].ForEach(r => r.enabled = false);
        }
    }

    public void EnterBuildMode()
    {
        if (HexagonController.buildingGhost != null)
            Destroy(HexagonController.buildingGhost);

        HexagonController.buildingGhost = Instantiate(buildingGhostPrefab).gameObject;
        HexagonController.buildingGhost.GetComponent<BuildingGhost>().buildingPrefab = buildingPrefab;
        SuggestionHandler.nextPlay = Time.time;


        if (HappinessMap.isDisplaying)
            HappinessMap.ToggleMap();
        if (WindController.isDisplaying)
            WindController.ToggleMap();

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
            {
                Tile tile = World.tiles[i, j];
                if (!tile.canPlaceBuilding(buildingPrefab) || tile.containsRoad || tile.isOccupied)
                    OverlayHandler.crosses[i, j].ForEach(r => r.enabled = true);
                else
                    OverlayHandler.crosses[i, j].ForEach(r => r.enabled = false);
                OverlayHandler.renderers[i, j].enabled = false;
            }
        OverlayHandler.lastOverlay = OverlayType.Building;
    }
}
