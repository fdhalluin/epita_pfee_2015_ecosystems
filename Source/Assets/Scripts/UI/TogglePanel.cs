﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TogglePanel : MonoBehaviour
{
    public InGameConsoleController consoleController;

    public Toggle happinessToggle;
    public Toggle windToggle;
    public Toggle fundToggle;
    public Toggle datavisToggle;
    public Toggle consoleToggle;
    public Toggle qualityToggle;

    static public TogglePanel instance { get; private set; }

    void Awake()
    {
        instance = this;
    }

    public void ToggleHappiness()
    {
        HappinessMap.ToggleMap();
    }

    public void ToggleWind()
    {
        WindController.ToggleMap();
    }

    public void ToggleFunds()
    {
        PlayerFundDisplay.forceDisplay = !PlayerFundDisplay.forceDisplay;
        PlayerEnergyDisplay.forceDisplay = !PlayerEnergyDisplay.forceDisplay;
    }

    public void ToggleQuality()
    {
        QualitySettings.SetQualityLevel(1 - QualitySettings.GetQualityLevel());
    }

    public void ToggleDataVisWindow()
    {
        DataVis.isDisplaying = !DataVis.isDisplaying;
    }

    public void ToggleConsoleWindow()
    {
        if (consoleController.console == null)
            consoleController.OpenConsole();
        else
            consoleController.CloseConsole();
    }
}
