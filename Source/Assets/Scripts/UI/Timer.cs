﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour
{
    public float startTime;
    public float currentTime;
    public Text displayText;

    // Use this for initialization
    void Start()
    {
        currentTime = startTime + .5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTime > 0)
        {
            if (Player.hasStartedPlaying && Player.gameSpeed != 0)
                currentTime = startTime - (Time.time - Player.timeOfStart);

            string minutes = Mathf.FloorToInt(currentTime / 60f).ToString("00");
            string seconds = Mathf.FloorToInt(currentTime % 60f).ToString("00");
            displayText.text = string.Format("{0}:{1}", minutes, seconds);
        }
        if (currentTime <= 0)
        {
            if (World.softReset)
                Start();
            else
            {
                GameOverPanel.instance.Show();
                displayText.text = string.Format("00:00");
            }
        }
	}
}
