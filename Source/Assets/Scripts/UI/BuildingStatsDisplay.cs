﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuildingStatsDisplay : MonoBehaviour
{

	Player player;
	private Rect _screenRect;
    private Rect _displayRect;

    private PlayerBuildingDescriptor descriptor_;
    private BuildMenuControler menuController_;

    void Start()
    {
        descriptor_ = GetComponent<BuildingButton>().buildingPrefab.GetComponent<PlayerBuilding>().getDescriptor as PlayerBuildingDescriptor;
        menuController_ = transform.GetComponentInParent<BuildMenuControler>();
    }

	void Update()
	{

		var rect = GetComponent<RectTransform>().rect;

		Vector3 tempPosition = transform.position;
		tempPosition.y = 0f;
		tempPosition.x -= rect.size.x;
		_screenRect = new Rect(tempPosition, rect.size);
	
		_displayRect = new Rect(_screenRect);
        _displayRect.height += 20;
        _displayRect.width *= 2;
        _displayRect.y = Screen.height - 2 * rect.height - 20;

	}


    void OnGUI()
    {
        bool isMouseOver = _screenRect.Contains(Input.mousePosition);
        if (isMouseOver && menuController_.IsTierUnlocked(descriptor_.tier))
        {
            GUI.skin.box.alignment = TextAnchor.UpperLeft;
            GUI.Box(_displayRect,
                "<b>" + descriptor_.inGameName +
			        "\n\nProduction:" +
			        "\nStorage:" +
			        "\nUpkeep:" + 
                "\nPrice:</b>");
            GUI.skin.box.alignment = TextAnchor.UpperRight;
            GUI.Box(_displayRect,
			        "<b>\n\n<color=lime>" + descriptor_.powerOutput + "kW/s</color>" +
                "\n<color=lime>" + descriptor_.storageCapacity + "kW</color>" +
			        "\n<color=red>$" + descriptor_.upkeep + "k</color>" +
			        "\n<color=red>$" + descriptor_.cost + "k</color>" + "</b>");
        }
        GUI.skin.box.alignment = TextAnchor.UpperCenter;
		
	}
}
