﻿using UnityEngine;
using System.Collections.Generic;

public class City : Building
{
    public City masterCity = null;
    public List<City> citiesInGroup;
    List<Tile> allHarvesteableTiles = null;
    bool hasOpenNeighbour = true;

    public CityDescriptor descriptor;

    public override BuildingDescriptor getDescriptor { get { return descriptor; } }

    public int population = 10;
    public int level = 1;
    public float foodStored = 0;
    public bool canGather = true;

    public int lastPopulationSum;
    public float lastIncome;
    public float lastConsumption;

    private float populationGrowth;

    public void Start()
    {
        if (masterCity == this)
        {
            float variance = descriptor.initialPopulationGrowthVariance;
            populationGrowth = descriptor.populationGrowth * (1 + Random.Range(-variance * .5f, variance * .5f));
        }
        else
            populationGrowth = masterCity.populationGrowth;
    }

    public void Update()
    {     
    }

    public void populationTick(Player playerData)
    {
        // Gather food from nearby areas
        if (masterCity == this)
        {
            if (allHarvesteableTiles == null)
            {
                allHarvesteableTiles = new List<Tile>();
                foreach (City c in citiesInGroup)
                    if (c.canGather && c.hasOpenNeighbour)
                        foreach (Tile t in c.tile.GetNeighbors())
                            if (t.building == null)
                            if (!allHarvesteableTiles.Contains(t))
                                allHarvesteableTiles.Add(t);
            }
        
            float gathered = 0;
            foreach (Tile t in allHarvesteableTiles)
                gathered += t.descriptor.townGrowthMultiplier * playerData.cityGrowthMult * Player.gameSpeed;
            foreach (City c in citiesInGroup)
                gathered -= c.population * 0.01f * Player.gameSpeed;

            float sumInvDist = 0f;
            foreach (City c in citiesInGroup)
                sumInvDist += 1f / (1 + Mathf.Abs(c.tile.xIndex - tile.xIndex) + Mathf.Abs(c.tile.yIndex - tile.yIndex));
            foreach (City c in citiesInGroup)
                c.foodStored += gathered / (1 + Mathf.Abs(c.tile.xIndex - tile.xIndex) + Mathf.Abs(c.tile.yIndex - tile.yIndex)) / sumInvDist;
        }

        float costToGrow = (300 * playerData.cityGrowthMult / playerData.cityGrowthSpeed) * Player.seasonConsumptionMult;
        for (int i = 0; i < Player.gameSpeed; i++)
        {
            if (foodStored > costToGrow)
            {
                float variance = descriptor.populationGrowthVariance;
                float popIncrease = populationGrowth * (1f + Random.Range(-variance * .5f, variance * .5f));
                addPopulation(Mathf.RoundToInt(popIncrease));
                foodStored -= costToGrow;
            }
            else if (foodStored < -100)  // Do something in case of starvation ? (impact happiness or other ?)
                foodStored = -100;
            else
                break;
        }
    }

    void populateNeighbor(int amount, City master)
    {
        if (hasOpenNeighbour)
        {
            var neighbors = tile.GetAvailableNeighbors();
            if (neighbors.Count > 0)
            {
                neighbors[Random.Range(0, neighbors.Count)].addPopulation(amount, master);
                allHarvesteableTiles = null;
            }
            else
                hasOpenNeighbour = false;
        }
        else if (World.adjacentPlayerBuildings[tile.xIndex, tile.yIndex] != 0)  // Can't grow because of a player building
            tile.DecrementHappiness(0.001f * Player.gameSpeed);
    }

    void addPopulation(int amount)
    {
        if (population > populationLimit / 4 && Random.Range(0, populationLimit) < population / 2)
            populateNeighbor(amount, masterCity);
        else if (population + amount <= populationLimit)
            population += amount;
        else
        {
            amount -= populationLimit - population;
            population = populationLimit;
            populateNeighbor(amount, masterCity);
        }

        // Check if population > 50% max && max level not reached
        // Note: Only the center can reach the max level
        if (population > (populationLimit >> 1) && (level < descriptor.maxLevel - 1 || level == descriptor.maxLevel - 1 && masterCity == this))
        {
            int neighborsOfSameLevel = 0;
            var neighbors = tile.GetNeighbors();
            foreach (Tile neighbor in neighbors)
            {
                if (neighbor.building != null && neighbor.building.getDescriptor.isACity)
                {
                    var city = neighbor.building as City;
                    if (city.level >= level)
                        ++neighborsOfSameLevel;
                }
            }

            // If the city is surrounded by at least 4 tile of the same level
            if (neighborsOfSameLevel >= descriptor.levelUpConnexity)
            {
                level++;
                gameObject.GetComponent<BuildingGrowthHandler>().currentLevel++;
            }

            if (population > descriptor.populationLimits[level - 1])
                population = descriptor.populationLimits[level - 1];
        }
    }

    public void SelectAllLinkedCities()
    {
        List<GameObject> tiles = new List<GameObject>();
        foreach (City c in masterCity.citiesInGroup)
            tiles.Add(c.tile.gameObject);
        SelectionHandler.SelectTiles(tiles, Color.red);
    }

    public override void OnTileMouseOver()
    {
        float income = masterCity.lastIncome * Player.tickFrequency;
        float consumption = masterCity.lastConsumption * Player.tickFrequency;
        HUD.boxText = "City\nPopulation: " + masterCity.lastPopulationSum + "k\nIncome: $" + income.ToString("F2") + "k/s\nConsumption: " + (-consumption).ToString("F2") + "kW/s";
        HUD.unmouse = 2;
    }

    public override void OnTileSelected()
    {
        SelectAllLinkedCities();

        if (PylonControler.isDragging)
        {
            PylonControler.isDragging = false;
            PylonControler.ConnectPoints(tile.xIndex, tile.yIndex, true);
        }
    }

    public int populationLimit { get { return descriptor.populationLimits[level - 1]; } }

    public override bool allowsGrowth { get { return population < descriptor.populationLimits[level - 1]; } }
}
