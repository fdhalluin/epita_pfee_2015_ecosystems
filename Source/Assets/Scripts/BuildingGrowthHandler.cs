﻿using UnityEngine;
using System.Collections;

public class BuildingGrowthHandler : MonoBehaviour
{
    public Object cube;

    public int currentLevel = 0;
    private int targetLevel = 0;

    private float[] towerHeights;
    private float[] towerTargetHeights;

    private CityDescriptor descriptor;
    private GameObject[] towers;
    private bool continueGrowth;

    void Awake()
    {
        descriptor = GetComponent<City>().descriptor;

        towers = new GameObject[4];
        for (int i = 0; i < 4; i++)
            towers[i] = null;
    }

    // Use this for initialization
    void Start()
    {
        towers[0] = Instantiate(cube, transform.position + new Vector3(0.25f, 0, 0.25f), Quaternion.identity) as GameObject;
        towers[1] = Instantiate(cube, transform.position + new Vector3(0.25f, 0, -0.25f), Quaternion.identity) as GameObject;
        towers[2] = Instantiate(cube, transform.position + new Vector3(-0.25f, 0, 0.25f), Quaternion.identity) as GameObject;
        towers[3] = Instantiate(cube, transform.position + new Vector3(-0.25f, 0, -0.25f), Quaternion.identity) as GameObject;

        for (int i = 0; i < 4; i++)
        {
            towers[i].transform.localScale = new Vector3(1f, 0f, 1f);
            towers[i].transform.parent = transform;
        }

        towerHeights = new float[4];
        for (int i = 0; i < 4; i++)
            towerHeights[i] = 0;

        towerTargetHeights = new float[4];
        for (int i = 0; i < 4; i++)
            towerTargetHeights[i] = 0;

        continueGrowth = true;
    }
	
    // Update is called once per frame
    void Update()
    {
        if (currentLevel > descriptor.maxLevel)
            currentLevel = descriptor.maxLevel;
        else if (targetLevel != currentLevel)
        {
            targetLevel = currentLevel;
            continueGrowth = true;

            for (int i = 0; i < 4; i++)
                towerTargetHeights[i] = descriptor.maxHeight[currentLevel - 1] * (1f + Random.Range(0f, descriptor.heightVariance));
        }

        if (continueGrowth)
        {
            continueGrowth = false;

            for (int i = 0; i < 4; i++)
            {
                if (Mathf.Abs(towerHeights[i] - towerTargetHeights[i]) > 0.02f)
                {
                    float sign = Mathf.Sign(towerTargetHeights[i] - towerHeights[i]);
                    towerHeights[i] += sign * 0.02f;
                    continueGrowth = true;
                    towers[i].transform.localScale = new Vector3(0.25f, towerHeights[i] * 10, 0.25f);
                    towers[i].transform.Translate(0f, sign * 0.01f, 0f);
                }
            }
        }
    }

    public void SetColor(Color col)
    {
        for (int i = 0; i < 4; i++)
            if (towers[i] != null)
                towers[i].GetComponent<Renderer>().material.color = col;
    }
}
