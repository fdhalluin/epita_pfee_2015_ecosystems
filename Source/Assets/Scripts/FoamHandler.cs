﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoamHandler : MonoBehaviour
{
    public static List<GameObject> foams = new List<GameObject>();

    public static void AddFoam()
    {
        for (int i = 1; i < World.gridWidth - 1; i++)
            for (int j = 1; j < World.gridWidth - 1; j++)
            {
                if (World.hexagons[i, j].transform.position.y == 0)
                {
                    bool requiresFoam = false;
                    List<Tile> neighbours = World.tiles[i, j].GetNeighbors();
                    foreach (Tile t in neighbours)
                        if (t.gameObject.transform.position.y < 0)
                        {
                            requiresFoam = true;
                            break;
                        }

                    if (requiresFoam)
                    {
                        GameObject hexa = World.hexagons[i, j];
                        GameObject newHexa = (GameObject)GameObject.Instantiate(World.instance.selectionHexa, new Vector3(hexa.transform.position.x, 1.02f, hexa.transform.position.z), Quaternion.identity);
                        newHexa.transform.localScale = new Vector3(2.5f, 0.1f, 2.5f);
                        newHexa.transform.parent = World.instance.transform;
                        newHexa.GetComponent<Renderer>().material.color = Color.white;

                        foams.Add(newHexa);
                    }
                }
            }
        
        AddFixedFoam();
    }

    public Object plane;
    public bool island = false;
    private bool oldIsland = false;

    void Update()
    {
        float scale = 2.4f + 0.1f * Mathf.Sin(Time.time);
        foreach (GameObject go in foams)
            go.transform.localScale = new Vector3(scale, 0.1f, scale);

        if (island != oldIsland)
        {
            oldIsland = island;

            if (island)
                TransformToIsland();
            else
                TransformFromIsland();
        }
    }

    GameObject waterPlane;
    List<GameObject> borderFoams;

    void TransformToIsland()
    {
        waterPlane = (GameObject)GameObject.Instantiate(plane, new Vector3(0, 1.05f, 0), Quaternion.identity);
        waterPlane.transform.localScale = new Vector3(2000, 1, 2000);
        waterPlane.GetComponent<Renderer>().material.color = Color.blue;

        borderFoams = new List<GameObject>();

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                if (j == 0 || j == World.gridWidth - 1 || i == 0 || i == World.gridWidth - 1)
                if (World.hexagons[i, j].transform.position.y >= 0)
                {
                    GameObject hexa = World.hexagons[i, j];
                    GameObject newHexa = (GameObject)GameObject.Instantiate(World.instance.selectionHexa, new Vector3(hexa.transform.position.x, 1.02f, hexa.transform.position.z), Quaternion.identity);
                    newHexa.transform.localScale = new Vector3(2.5f, 0.1f, 2.5f);
                    newHexa.transform.parent = World.instance.transform;
                    newHexa.GetComponent<Renderer>().material.color = Color.white;

                    foams.Add(newHexa);
                    borderFoams.Add(newHexa);
                }

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                if (World.hexagons[i, j].transform.position.y < 0)
                    World.hexagons[i, j].GetComponent<Renderer>().enabled = false;
    }

    void TransformFromIsland()
    {
        GameObject.Destroy(waterPlane);

        borderFoams.ForEach(foam =>
            {
                foams.Remove(foam);
                GameObject.Destroy(foam);
            });
        borderFoams.Clear();

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                if (World.hexagons[i, j].transform.position.y < 0)
                    World.hexagons[i, j].GetComponent<Renderer>().enabled = true;
    }

    static void AddFixedFoam()
    {
        float xOff;
        float yOff;

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                if (j == 0 || j == World.gridWidth - 1 || i == 0 || i == World.gridWidth - 1)
                if (World.hexagons[i, j].transform.position.y >= 0)
                {
                    bool requiresFoam = false;
                    List<Tile> neighbours = World.tiles[i, j].GetNeighbors();
                    foreach (Tile t in neighbours)
                        if (t.gameObject.transform.position.y < 0)
                        {
                            requiresFoam = true;
                            break;
                        }

                    if (requiresFoam)
                    {
                        xOff = 0;
                        yOff = 0;
                        if (i == 0)
                        {
                            xOff += 0.3f;
                            if (j % 2 == 1)
                                yOff -= 0.1f;
                        }
                        else if (i == World.gridWidth - 1)
                            xOff -= 0.3f;
                        if (j == 0)
                            yOff += 0.25f;

                        GameObject hexa = World.hexagons[i, j];
                        GameObject newHexa = (GameObject)GameObject.Instantiate(World.instance.selectionHexa, new Vector3(hexa.transform.position.x + xOff, 1.02f, hexa.transform.position.z + yOff), Quaternion.identity);
                        newHexa.transform.localScale = new Vector3(2.3f, 0.1f, 2.3f);
                        newHexa.transform.parent = World.instance.transform;
                        newHexa.GetComponent<Renderer>().material.color = Color.white;
                    }
                }
    }
}