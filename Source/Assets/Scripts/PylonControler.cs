﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PylonControler : MonoBehaviour
{
    public static bool isDragging = false;
    public static int lastX;
    public static int lastY;

    public static bool[,] containsPylon;

    public static void InitPylonControler()
    {
        containsPylon = new bool[World.gridWidth, World.gridWidth];

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                containsPylon[i, j] = false;
    }

    public static void ConnectPoints(int x, int y, bool isCity)
    {
        PathfindingNode path = GetOptimalPylonPath(lastX, lastY, x, y);
        while (path != null)
        {
            containsPylon[path.x, path.y] = true;
            float xx = World.hexagons[path.x, path.y].transform.position.x;
            float yy = World.hexagons[path.x, path.y].transform.position.z;


            GameObject newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                                      new Vector3(xx, 1.5f, yy + 0.97f),
                                      Quaternion.identity);

            newPylon.transform.parent = World.instance.transform;

            if (path.prevNode != null)
            {
                if (path.x == path.prevNode.x + 1 && path.y == path.prevNode.y)
                {
                    newPylon.transform.eulerAngles = new Vector3(0, 60, 0);
                    newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                        new Vector3(xx - 0.85f, 1.5f, yy + 0.48f),
                        Quaternion.identity);
                    newPylon.transform.eulerAngles = new Vector3(0, 120, 0);

                    newPylon.transform.parent = World.instance.transform;
                }
                else if (path.x == path.prevNode.x - 1 && path.y == path.prevNode.y)
                {
                    newPylon.transform.eulerAngles = new Vector3(0, -60, 0);
                    newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                        new Vector3(xx + 0.85f, 1.5f, yy + 0.48f),
                        Quaternion.identity);
                    newPylon.transform.eulerAngles = new Vector3(0, -120, 0);

                    newPylon.transform.parent = World.instance.transform;
                }
                else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x + 0.85f) < 0.1f && path.prevNode.y < path.y)
                {
                    newPylon.transform.eulerAngles = new Vector3(0, -60, 0);
                    newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                        new Vector3(xx + 0.85f, 1.5f, yy + 0.47f),
                        Quaternion.identity);
                    newPylon.transform.eulerAngles = new Vector3(0, 0, 0);

                    newPylon.transform.parent = World.instance.transform;
                }
                else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x + 0.85f) < 0.1f && path.prevNode.y > path.y)
                {
                    newPylon.transform.eulerAngles = new Vector3(0, 180, 0);
                    newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                        new Vector3(xx, 1.5f, yy + 1.95f),
                        Quaternion.identity);
                    newPylon.transform.eulerAngles = new Vector3(0, 240, 0);

                    newPylon.transform.parent = World.instance.transform;
                }
                else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x - 0.85f) < 0.1f && path.prevNode.y < path.y)
                {
                    newPylon.transform.eulerAngles = new Vector3(0, 60, 0);
                    newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                        new Vector3(xx - 0.85f, 1.5f, yy + 0.47f),
                        Quaternion.identity);
                    newPylon.transform.eulerAngles = new Vector3(0, 0, 0);

                    newPylon.transform.parent = World.instance.transform;
                }
                else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x - 0.85f) < 0.1f && path.prevNode.y > path.y)
                {
                    newPylon.transform.eulerAngles = new Vector3(0, 180, 0);
                    newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                        new Vector3(xx, 1.5f, yy + 1.95f),
                        Quaternion.identity);
                    newPylon.transform.eulerAngles = new Vector3(0, -240, 0);

                    newPylon.transform.parent = World.instance.transform;
                }
            }
            else
            {
                newPylon.transform.eulerAngles = new Vector3(0, 60, 0);
                newPylon = (GameObject)GameObject.Instantiate(World.instance.pylonLine, 
                    new Vector3(xx - 0.85f, 1.5f, yy + 0.48f),
                    Quaternion.identity);
                newPylon.transform.eulerAngles = new Vector3(0, -120, 0);

                newPylon.transform.parent = World.instance.transform;
            }

            path = path.prevNode;
        }

        lastX = x;
        lastY = y;
    }

    public static PathfindingNode GetOptimalPylonPath(int i1, int j1, int i2, int j2)
    {
        Stack<Tile> toHandle = new Stack<Tile>();
        toHandle.Push(World.tiles[i1, j1]);

        Dictionary<Tile, PathfindingNode> pathInfo = new Dictionary<Tile, PathfindingNode>();

        pathInfo.Add(World.tiles[i1, j1], new PathfindingNode(0, null, i1, j1));

        while (toHandle.Count > 0)
        {
            Tile handling = toHandle.Pop();
            int dist = pathInfo[handling].shortestDistance;
            int weight = (containsPylon[handling.xIndex, handling.yIndex]) ? 1 : 4;

            if (dist < 100 && handling.xIndex < World.gridWidth && handling.yIndex < World.gridWidth)
            {
                foreach (Tile neighbour in handling.GetNeighbors())
                    if (neighbour.descriptor == World.GetGrassDescriptor())
                    {
                        if (!pathInfo.ContainsKey(neighbour))
                        {
                            pathInfo.Add(neighbour, new PathfindingNode(dist + weight, pathInfo[handling], neighbour.xIndex, neighbour.yIndex));
                            toHandle.Push(neighbour);
                        }
                        else if (dist + weight < pathInfo[neighbour].shortestDistance)
                        {
                            pathInfo[neighbour] = new PathfindingNode(dist + weight, pathInfo[handling], neighbour.xIndex, neighbour.yIndex);
                            toHandle.Push(neighbour);
                        }
                    }
            }
        }

        if (pathInfo.ContainsKey(World.tiles[i2, j2]))
            return pathInfo[World.tiles[i2, j2]];
        return null;
    }
}