﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;

public class DescriptorCreator
{
    private const string DATA_PATH = "Assets/Resources/Data/";
    private const string MENU_PATH = "Assets/Create/Descriptor/";

    [MenuItem(MENU_PATH + "Tile")]
    public static void CreateTileDescriptor()
    {
        var descriptor = ScriptableObject.CreateInstance<TileDescriptor>();

        string assetName = "New Tile";

        AssetDatabase.CreateAsset(descriptor, DATA_PATH + "Tiles/" + assetName + ".asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = descriptor;
    }

    [MenuItem(MENU_PATH + "City")]
    public static void CreateCityDescriptor()
    {
        var descriptor = ScriptableObject.CreateInstance<CityDescriptor>();

        string assetName = "City";

        AssetDatabase.CreateAsset(descriptor, DATA_PATH + "Buildings/" + assetName + ".asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = descriptor;
    }
}
#endif //UNITY_EDITOR
