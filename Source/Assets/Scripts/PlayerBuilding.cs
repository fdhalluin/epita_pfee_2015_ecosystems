﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerBuilding : Building
{
    public PlayerBuildingDescriptor descriptor;

    public override BuildingDescriptor getDescriptor { get { return descriptor; } }

    public override bool allowsGrowth { get { return false; } }

    float buildingRatio = 0f;
    float actualPowerGeneration;

    // Use this for initialization
    void Start()
    {
        transform.position = new Vector3(transform.position.x, World.hexagons[tile.xIndex, tile.yIndex].transform.position.y, transform.position.z);

        Player.upkeep += descriptor.upkeep;
        Player.funds -= descriptor.cost;

        if (descriptor.solarEnergy)
            actualPowerGeneration = descriptor.powerOutput * tile.descriptor.sunlight;
        else if (descriptor.windEnergy)
            actualPowerGeneration = descriptor.powerOutput * tile.descriptor.windStrength * WindController.windLevel[tile.xIndex, tile.yIndex];
        else
            actualPowerGeneration = descriptor.powerOutput;
        Player.powerGeneration += actualPowerGeneration;

        Player.maximumPower += descriptor.storageCapacity;

        float buildingX = transform.position.x;
        float buildingY = transform.position.z;
        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                World.tiles[i, j].DecrementHappiness((descriptor.unhapinessFactor - HexagonHelper.DistanceToPoint(i, j, buildingX, buildingY)) * 0.05f);

        HappinessMap.UpdateMap();

        SetBuildableTiles();
    }

    // Update is called once per frame
    void Update()
    {
        if (buildingRatio < 1)
            buildingRatio += Time.deltaTime;
        else
            buildingRatio = 1;
        transform.position = new Vector3(transform.position.x, World.hexagons[tile.xIndex, tile.yIndex].transform.position.y + 0.5f + buildingRatio, transform.position.z);
    }

    public override void OnTileMouseOver()
    {
        float upkeep = descriptor.upkeep * Player.tickFrequency;
        float power = actualPowerGeneration * Player.tickFrequency;
        HUD.boxText = descriptor.name + "\nUpkeep: $" + upkeep + "k/s\nPower: " + power.ToString("F2") + "/s\nStorage: " + descriptor.storageCapacity + "kW";
        HUD.unmouse = 2;
    }

    public override void OnTileSelected()
    {
        PylonControler.isDragging = true;
        PylonControler.lastX = tile.xIndex;
        PylonControler.lastY = tile.yIndex;
        SelectionHandler.SelectTiles(new List<GameObject>{ tile.gameObject }, Color.blue);
    }

    public bool CanBeAfforded()
    {
        return descriptor.cost <= Mathf.FloorToInt(Player.funds);
    }

    void SetBuildableTiles()    // To prevent clumping
    {
        foreach (Tile t in tile.GetNeighbors())
        {
            World.adjacentPlayerBuildings[t.xIndex, t.yIndex] += 1;
            if (World.adjacentPlayerBuildings[t.xIndex, t.yIndex] == 2 && World.tiles[t.xIndex, t.yIndex].building != null)
            {
                foreach (Tile t2 in t.GetNeighbors())
                    World.canContainPlayerBuildings[t2.xIndex, t2.yIndex] = false;
            }
            else if (World.adjacentPlayerBuildings[t.xIndex, t.yIndex] == 3)
                World.canContainPlayerBuildings[t.xIndex, t.yIndex] = false;
        }
        if (World.adjacentPlayerBuildings[tile.xIndex, tile.yIndex] >= 2)
        {
            foreach (Tile t2 in tile.GetNeighbors())
                World.canContainPlayerBuildings[t2.xIndex, t2.yIndex] = false;
        }
    }
}
