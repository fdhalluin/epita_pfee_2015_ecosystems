﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EcosystemHandler : MonoBehaviour
{
    public bool spawn = false;
    bool hasSpawned = false;

    public Object wolf;
    public Object sheep;

    List<GameObject>[,] wolfs;
    List<GameObject>[,] sheeps;

    static int wolfCount = 0;
    static int ticksSinceLastKill;

    float _timeToTick;

    public void InitEcosystem()
    {
        wolfs = new List<GameObject>[World.gridWidth, World.gridWidth];
        sheeps = new List<GameObject>[World.gridWidth, World.gridWidth];

        _timeToTick = 5f / Player.gameSpeed;

        wolfCount = 0;
        ticksSinceLastKill = 0;
    }
	
    // Update is called once per frame
    void Update()
    {
        _timeToTick -= Time.deltaTime;
        if (_timeToTick < 0)
        {
            ExecuteTick();
            _timeToTick += 5f / Player.gameSpeed;
        }

        if (spawn)
        {
            spawn = false;

            if (hasSpawned)
                for (int i = 0; i < World.gridWidth; i++)
                    for (int j = 0; j < World.gridWidth; j++)
                    {
                        wolfs[i, j].ForEach(go => Destroy(go));
                        sheeps[i, j].ForEach(go => Destroy(go));
                    }
            hasSpawned = true;

            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                {
                    wolfs[i, j] = new List<GameObject>();
                    sheeps[i, j] = new List<GameObject>();
                }

            for (int i = 0; i < 30; i++)
            {
                AddSheeps(Random.Range(0, World.gridWidth), Random.Range(0, World.gridWidth), Random.Range(1, 5));
                AddWolfs(Random.Range(0, World.gridWidth), Random.Range(0, World.gridWidth), Random.Range(1, 5));
            }
        }

        if (hasSpawned)
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                {
                    float posX = World.hexagons[i, j].transform.position.x;
                    float posY = World.hexagons[i, j].transform.position.z;
                    float countWolfs = (float)wolfs[i, j].Count;
                    float countSheeps = (float)sheeps[i, j].Count;
                    float cur = 0;
                    float t = Time.time;

                    wolfs[i, j].ForEach(go =>
                        {
                            go.transform.position = Vector3.Lerp(go.transform.position,
                                new Vector3(posX + 0.5f * Mathf.Cos(cur * 6.28f / countWolfs + t * 0.5f),
                                    1.53f, 
                                    posY + 0.5f * Mathf.Sin(cur * 6.28f / countWolfs + t * 0.5f)), 0.01f);
                            cur++;
                        });

                    cur = 0;

                    sheeps[i, j].ForEach(go =>
                        {
                            go.transform.position = Vector3.Lerp(go.transform.position, 
                                new Vector3(posX + 0.35f * Mathf.Cos(cur * 6.28f / countSheeps + t * 0.5f),
                                    1.53f, 
                                    posY + 0.35f * Mathf.Sin(cur * 6.28f / countSheeps + t * 0.5f)), 0.01f);
                            cur++;
                        });
                }
    }

    void AddSheeps(int x, int y, int count)
    {
        if (CanContainAnimals(x, y))
        {
            float posX = World.hexagons[x, y].transform.position.x;
            float posY = World.hexagons[x, y].transform.position.z;

            if (sheeps[x, y].Count == 0)
                for (int i = 0; i < count; i++)
                    sheeps[x, y].Add((GameObject)GameObject.Instantiate(sheep,
                            new Vector3(posX, 1.53f, posY),
                            Quaternion.identity));
            else
                for (int i = 0; i < count; i++)
                    sheeps[x, y].Add((GameObject)GameObject.Instantiate(sheep,
                            sheeps[x, y][0].transform.position,
                            Quaternion.identity));
        }
    }

    void AddWolfs(int x, int y, int count)
    {
        if (CanContainAnimals(x, y))
        {
            float posX = World.hexagons[x, y].transform.position.x;
            float posY = World.hexagons[x, y].transform.position.z;

            if (wolfs[x, y].Count == 0)
                for (int i = 0; i < count; i++)
                    wolfs[x, y].Add((GameObject)GameObject.Instantiate(wolf,
                            new Vector3(posX, 1.53f, posY),
                            Quaternion.identity));
            else
                for (int i = 0; i < count; i++)
                    wolfs[x, y].Add((GameObject)GameObject.Instantiate(wolf,
                            wolfs[x, y][0].transform.position,
                            Quaternion.identity));

            wolfCount += count;
        }
    }

    void KillSheeps(int x, int y, int count)
    {
        for (int i = 0; i < Mathf.Min(count, sheeps[x, y].Count); i++)
        {
            GameObject.Destroy(sheeps[x, y][0]);
            sheeps[x, y].RemoveAt(0);
        }
    }

    void KillWolfs(int x, int y, int count)
    {
        for (int i = 0; i < Mathf.Min(count, wolfs[x, y].Count); i++)
        {
            GameObject.Destroy(wolfs[x, y][0]);
            wolfs[x, y].RemoveAt(0);
            wolfCount--;
        }
    }

    void MoveSheeps(int x1, int y1, int x2, int y2, int count)
    {
        for (int i = 0; i < Mathf.Min(count, sheeps[x1, y1].Count); i++)
        {
            GameObject sheepToMove = sheeps[x1, y1][0];
            sheeps[x1, y1].Remove(sheepToMove);
            sheeps[x2, y2].Add(sheepToMove);
        }
    }

    void MoveWolfs(int x1, int y1, int x2, int y2, int count)
    {
        for (int i = 0; i < Mathf.Min(count, wolfs[x1, y1].Count); i++)
        {
            GameObject wolfToMove = wolfs[x1, y1][0];
            wolfs[x1, y1].Remove(wolfToMove);
            wolfs[x2, y2].Add(wolfToMove);
        }
    }

    bool CanContainAnimals(int x, int y)
    {
        return CanContainAnimals(World.tiles[x, y]);
    }

    bool CanContainAnimals(Tile t)
    {
        if (t.descriptor == World.instance.grassDescriptor)
            //if (t.containsRoad == false)
        if (t.building == null)
            return true;
        return false;
    }

    void ExecuteTick()
    {
        ticksSinceLastKill++;
        if (hasSpawned)
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                {
                    if (wolfs[i, j].Count > 0)
                    {
                        if (sheeps[i, j].Count > 0)
                        {
                            ticksSinceLastKill = 0;
                            KillSheeps(i, j, Mathf.Max(1, wolfs[i, j].Count / 3));
                            if (Random.Range(0, wolfs[i, j].Count) == 0)
                                AddWolfs(i, j, 1);
                        }
                        else
                        {
                            List<Tile> neighbours = World.tiles[i, j].GetNeighbors();
                            int preys = 0;
                            foreach (Tile t in neighbours)
                                if (sheeps[t.xIndex, t.yIndex].Count > 0)
                                    preys++;
                        
                            if (preys == 0)
                            {
                                if (wolfCount > 10)
                                    KillWolfs(i, j, 1);
                                int attempts = 5;
                                while (attempts-- > 0)
                                {
                                    Tile t = neighbours[Random.Range(0, neighbours.Count)];
                                    if (CanContainAnimals(t.xIndex, t.yIndex))
                                    {
                                        int transfer = Random.Range(2, wolfs[i, j].Count + 1);
                                        MoveWolfs(i, j, t.xIndex, t.yIndex, transfer);
                                        attempts = 0;
                                    }
                                }
                            }
                            else
                            {
                                int wolvesLeft = wolfs[i, j].Count;
                                foreach (Tile t in neighbours)
                                    if (sheeps[t.xIndex, t.yIndex].Count > 0)
                                    {
                                        int toMove = wolvesLeft / preys;
                                        wolvesLeft -= toMove;
                                        preys--;
                                        MoveWolfs(i, j, t.xIndex, t.yIndex, toMove);
                                    }
                            }
                        }
                    }

                    if (sheeps[i, j].Count > 0)
                    {
                        if (ticksSinceLastKill > 2)
                        {
                            AddWolfs(i, j, 1);
                            ticksSinceLastKill = 0;
                        }

                        List<Tile> neighbours = World.tiles[i, j].GetNeighbors();

                        int transfer = Random.Range(0, sheeps[i, j].Count + 1);
                        if (wolfs[i, j].Count == 0)
                        {
                            float c = 0;
                            int c2 = sheeps[i, j].Count;
                            foreach (Tile t in neighbours)
                                if (CanContainAnimals(t) && wolfs[t.xIndex, t.yIndex].Count == 0 && !t.containsRoad)
                                {
                                    c += t.happinessFactor;
                                    c2 += sheeps[t.xIndex, t.yIndex].Count;
                                }
                                else if (t.building != null)
                                    c -= 2;

                            if (c2 < c)
                                AddSheeps(i, j, 1);
                            else if (c2 > c + 2)
                                KillSheeps(i, j, 1);
                        }
                        else
                            transfer = sheeps[i, j].Count;

                        Tile tile = neighbours[Random.Range(0, neighbours.Count)];
                        if (CanContainAnimals(tile.xIndex, tile.yIndex) && wolfs[tile.xIndex, tile.yIndex].Count == 0)
                            MoveSheeps(i, j, tile.xIndex, tile.yIndex, transfer);
                    }
                }
    }
}