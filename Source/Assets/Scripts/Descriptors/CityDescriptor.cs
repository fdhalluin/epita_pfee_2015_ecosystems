﻿using UnityEngine;
using System.Collections;

public class CityDescriptor : BuildingDescriptor
{
    public override bool isACity { get { return true; } }

    public int maxLevel { get { return maxHeight.Length; } }
    public float[] maxHeight;
    public float heightVariance;

    [Range(1, 6)]
    public int levelUpConnexity;

    public int[] populationLimits;
    public float populationGrowth;
    public float initialPopulationGrowthVariance;
    public float populationGrowthVariance;
}
