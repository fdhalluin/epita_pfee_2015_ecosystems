﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerBuildingDescriptor : BuildingDescriptor
{
    public enum Tier
    {
        Tier1,
        Tier2,
        Tier3
    }

    public override bool isACity { get { return false; } }

    public string inGameName;
    
    public Tier tier;
    public int cost;
    public int upkeep;

    public float powerOutput;
    public int storageCapacity;

    public float unhapinessFactor;

    public bool solarEnergy = false;
    public bool windEnergy = false;

    public List<TileDescriptor> buildableTiles;
    public Sprite icon;
}
