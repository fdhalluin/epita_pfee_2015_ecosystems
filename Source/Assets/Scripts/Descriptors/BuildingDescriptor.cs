﻿using UnityEngine;
using System.Collections;

public abstract class BuildingDescriptor : ScriptableObject
{
    public Mesh mesh;

    public abstract bool isACity { get; }
}
