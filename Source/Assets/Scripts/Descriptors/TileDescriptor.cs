﻿using UnityEngine;

public class TileDescriptor : ScriptableObject
{
    public Material material;

    [Space(10f)]

    public float windStrength;
    public float sunlight;

    [Range(0, 1)]
    public float townGrowthMultiplier;
    public bool canHoldTown;
}
