﻿using UnityEngine;
using System.Collections.Generic;

public class HexagonController : MonoBehaviour
{
    public static List<GameObject> oldHexagons1;
    public static List<GameObject> oldHexagons2;
    public static GameObject buildingGhost;

    public static void InitMouseOver()
    {
        oldHexagons1 = new List<GameObject>();
        oldHexagons2 = new List<GameObject>();
    }

    void OnMouseOver()
    {
        Tile t = gameObject.GetComponent<Tile>();
        if (t.building != null)
            t.building.OnTileMouseOver();

        MouseOverGrid(false);
		
        if (buildingGhost != null)
        {
            BuildingGhost buildingGhostComp = buildingGhost.GetComponent<BuildingGhost>();
            // Exit build mode if the current building cannot be afforded
            if (!buildingGhostComp.buildingPrefab.GetComponent<PlayerBuilding>().CanBeAfforded())
            {
                Destroy(buildingGhost);
                buildingGhost = null;
                return;
            }

            // Box with production information for tile specific variations (solar, wind)
            PlayerBuildingDescriptor desc = buildingGhostComp.buildingPrefab.GetComponent<PlayerBuilding>().descriptor;
            if (desc.windEnergy)
            {
                float power = desc.powerOutput * t.descriptor.windStrength * WindController.windLevel[t.xIndex, t.yIndex] * Player.tickFrequency;
                HUD.smallBoxText = "Production: " + power.ToString("F2") + "/s";
                HUD.unmouse = 2;
            }
            else if (desc.solarEnergy)
            {
                float power = desc.powerOutput * t.descriptor.sunlight * Player.tickFrequency;
                HUD.smallBoxText = "Production: " + power.ToString("F2") + "/s";
                HUD.unmouse = 2;
            }

            buildingGhostComp.MoveToTile(GetComponent<Tile>());
    
            if (Input.GetMouseButtonDown(0) && Input.mousePosition.y > 82)    // Placing building
            {
                if (t.canPlaceBuilding(buildingGhostComp.buildingPrefab))
                {
                    t.AddBuilding(buildingGhostComp.buildingPrefab, false);
                    if (!(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
                    {
                        Destroy(buildingGhost);
                        buildingGhost = null;
                    }
                }
            }
            else if (Input.GetMouseButtonDown(1))   // Deselecting building
            {
                Destroy(buildingGhost);
                buildingGhost = null;
            }
        }
        else  // No building selected
        {
            if (Input.GetMouseButtonDown(0) && Input.mousePosition.y > 82)    // Tile selection
            {
                SelectionHandler.SelectTiles(new List<GameObject>{ gameObject }, Color.blue);
                MouseOverGrid(true);

                if (t.building != null)
                    t.building.OnTileSelected();
            }
            else if (Input.GetMouseButtonDown(1))   // Tile deselection
                SelectionHandler.SelectTiles(new List<GameObject>(), Color.magenta);
        }
    }

    public static GameObject lastHover = null;

    public void MouseOverGrid(bool forced)
    {
        if (World.instance.simpleMode)
            return;

        if (lastHover != gameObject || forced)
        {
            lastHover = gameObject;

            oldHexagons1.ForEach(Destroy);
            oldHexagons1.Clear();
            oldHexagons2.ForEach(go => go.transform.localScale = new Vector3(1.963f, 8f, 1.963f));
            SelectionHandler.oldHexagons2.ForEach(go => go.transform.localScale = new Vector3(1.7f, 8f, 1.7f));
            oldHexagons2.Clear();

            List<Tile> hover = new List<Tile>();
            hover.AddRange(GetComponent<Tile>().GetNeighbors());
            hover.Add(GetComponent<Tile>());
            foreach (Tile t in hover)
            {
                GameObject newHexa = (GameObject)Object.Instantiate(World.instance.selectionHexa, t.gameObject.transform.position, Quaternion.identity);
                newHexa.transform.localScale = new Vector3(1.962f, 7.95f, 1.962f);
                newHexa.GetComponent<Renderer>().material.color = Color.black;
                newHexa.transform.parent = World.instance.transform;
                oldHexagons1.Add(newHexa);
                oldHexagons2.Add(t.gameObject);
                t.gameObject.transform.localScale = new Vector3(1.7f, 8f, 1.7f);
            }
        }
    }
}