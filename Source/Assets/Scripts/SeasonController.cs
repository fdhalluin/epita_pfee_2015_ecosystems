﻿using UnityEngine;

public class SeasonController : MonoBehaviour
{
    public float seasonDuration;

    public float currentSeasonMultiplier;
    public float maxSeasonMultiplier;

    public int currentYear;
    public float timeOfYear;
    public float timeOfSeason;
    [Range(0, 3)] public int startingSeason;
    [Range(0, 3)] public int currentSeason;

    public static SeasonController instance;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        currentSeason = startingSeason;
        timeOfYear = startingSeason * .25f;
    }

    void Update()
    {
        if (Player.hasStartedPlaying)
        {
            // Compute current time of year
            float time = (Time.time - Player.timeOfStart) * Player.gameSpeed;
            float previousTime = timeOfYear;
            timeOfYear = ((float)startingSeason + (time / seasonDuration)) % 4 * .25f;

            // check if year was reset
            if (previousTime > timeOfYear)
                ++currentYear;
        
            // Update multiplier
            currentSeasonMultiplier = maxSeasonMultiplier * Mathf.Cos((timeOfYear + .5f) * Mathf.PI * .5f);

            // Update current season
            currentSeason = Mathf.FloorToInt(timeOfYear * 4f);
            timeOfSeason = (timeOfYear - .25f * currentSeason) * 4f;
        }
        else
        {
            currentSeason = startingSeason;
            timeOfSeason = 0;
            timeOfYear = startingSeason * .25f;
            currentYear = 0;
        }
    }

    public string GetSeasonName()
    {
        if (currentSeason == 0)
            return "SPRING";
        if (currentSeason == 1)
            return "SUMMER";
        if (currentSeason == 2)
            return "FALL";
        return "WINTER";
    }
}
