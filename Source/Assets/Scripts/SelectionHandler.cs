﻿using UnityEngine;
using System.Collections.Generic;

public class SelectionHandler : MonoBehaviour
{
    public static List<GameObject> oldHexagons1 = new List<GameObject>();
    public static List<GameObject> oldHexagons2 = new List<GameObject>();

    public static void InitSelectionHandler()
    {
        oldHexagons1.ForEach(Destroy);
        oldHexagons1.Clear();
        oldHexagons2.ForEach(go => go.transform.localScale = new Vector3(1.963f, 8f, 1.963f));
        oldHexagons2.Clear();
    }

    public static void SelectTiles(List<GameObject> tiles, Color col)
    {
        if (World.instance.simpleMode)
            return;

        oldHexagons1.ForEach(Destroy);
        oldHexagons1.Clear();
        oldHexagons2.ForEach(go => go.transform.localScale = new Vector3(1.963f, 8f, 1.963f));
        oldHexagons2.Clear();

        foreach (GameObject go in tiles)
        {
            GameObject newHexa = (GameObject)Object.Instantiate(World.instance.selectionHexa, go.transform.position, Quaternion.identity);
            newHexa.transform.localScale = new Vector3(1.963f, 7.97f, 1.963f);
            newHexa.GetComponent<Renderer>().material.color = col;
            oldHexagons1.Add(newHexa);
            oldHexagons2.Add(go);
            go.transform.localScale = new Vector3(1.7f, 8f, 1.7f);
        }
    }
}
