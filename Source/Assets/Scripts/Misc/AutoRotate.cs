﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour
{
	public Vector3 RotationSpeed;

	// Update is called once per frame
	void Update()
	{
		transform.localRotation = Quaternion.Euler(RotationSpeed * Time.deltaTime * 360) * transform.localRotation;
	}

	// Display debug info in editor
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, transform.position + transform.right);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(transform.position, transform.position + transform.up);
		Gizmos.color = Color.blue;
		Gizmos.DrawLine(transform.position, transform.position + transform.forward);
	}

	// Use this for debug UI in-game (for normal UI, use the UI components)
	void OnGUI()
	{
		GUI.Window(0, new Rect(Screen.width - 210, 10, 200, 150), DrawGUI, gameObject.name);
	}

	void DrawGUI(int id)
	{
		GUILayout.BeginVertical();
		GUILayout.Label("X:");
		RotationSpeed.x = GUILayout.HorizontalSlider(RotationSpeed.x, -1, 1);
		GUILayout.Label("Y:");
		RotationSpeed.y = GUILayout.HorizontalSlider(RotationSpeed.y, -1, 1);
		GUILayout.Label("Z:");
		RotationSpeed.z = GUILayout.HorizontalSlider(RotationSpeed.z, -1, 1);
		GUILayout.EndVertical();
	}
}
