﻿using UnityEngine;
using System.Collections;

public class ResetButton : MonoBehaviour
{
    public void QuitGame()
    {
        Application.Quit();
    }

    public void SoftReset()
    {
        GameOverPanel.instance.PlayAgain();
    }
}
