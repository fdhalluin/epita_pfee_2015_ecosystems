﻿using UnityEngine;
using System.Collections;

// Follow the coding style used in this file
// This is for consistency within your team and between all PFEEs
public class ChangeColor : MonoBehaviour
{
	// Use public variables for all object properties that should be serialized (editable in inspector)
	// For this project and in Unity in general, avoid properties because they won't be serialized.
	// Name them LikeThis.
	public float ChangePeriod = 1;
	public float Damping = 0.5f;
	public Material Material;

	// Use private variables for internal state
	// This is lost when reloading code at runtime and won't be serialized into prefabs.
	// Name them _likeThis.
	private float _nextChangeTime;
	private Color _targetColor;

	void Start()
	{
		_nextChangeTime = Time.time;
	}

	void Update()
	{
		// For timing operations, prefer comparing the current time to a target time
		// rather than accumulating Dt (this is more stable)
		if (Time.time > _nextChangeTime)
		{
			_targetColor = new Color(Random.value, Random.value, Random.value);
			_nextChangeTime = Time.time + ChangePeriod;
		}

		FadeMaterialColor(Material, _targetColor);
	}

	void FadeMaterialColor(Material material, Color targetColor)
	{
		// Prefer early outs to nested blocks
		if (material == null)
			return;

		const string colorId = "_Color";
		var color = material.GetColor(colorId);

		// Notice the same damping function as in Character.cs
		color = Color.Lerp(color, targetColor, Time.deltaTime / (Time.deltaTime + Damping));

		// This will affect all objects that use this material.
		// In the editor, it also overwrites the material on disk
		material.SetColor(colorId, color);
	}
}
