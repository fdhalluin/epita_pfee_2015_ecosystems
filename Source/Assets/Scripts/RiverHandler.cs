﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RiverHandler
{
    public static bool[,] containsRiver;

    public static void InitRivers()
    {
        containsRiver = new bool[World.gridWidth, World.gridWidth];

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                containsRiver[i, j] = false;

        for (int i = 0; i < 300; i++)
            containsRiver[Random.Range(0, World.gridWidth), Random.Range(0, World.gridWidth)] = true;

        int rivers = 5;
        while (rivers > 0)
        {
            int x1 = Random.Range(0, World.gridWidth);
            int y1 = Random.Range(0, World.gridWidth);
            int x2 = Random.Range(0, World.gridWidth);
            int y2 = Random.Range(0, World.gridWidth);
            if (World.hexagons[x1, y1].transform.position.y < 0 && World.hexagons[x2, y2].transform.position.y < 0)
            {
                ConnectPoints(x1, y1, x2, y2);
                rivers--;
            }
        }
    }

    public static void ConnectPoints(int x1, int y1, int x2, int y2)
    {
        PathfindingNode path = GetOptimalRiverPath(x1, y1, x2, y2);
        while (path != null)
        {
            containsRiver[path.x, path.y] = true;
            float xx = World.hexagons[path.x, path.y].transform.position.x;
            float yy = World.hexagons[path.x, path.y].transform.position.z;

            if (path.prevNode != null)
            {
                float h = Mathf.Min(0, Mathf.Min(World.hexagons[path.x, path.y].transform.position.y, World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.y)) + 0.571f;
                if (h > 0.5f)
                {
                    GameObject newRiver = (GameObject)GameObject.Instantiate(World.instance.river, 
                                              new Vector3(xx, h, yy + 0.97f),
                                              Quaternion.identity);
                    newRiver.transform.parent = World.instance.transform;

                    if (path.x == path.prevNode.x + 1 && path.y == path.prevNode.y)
                    {
                        newRiver.transform.eulerAngles = new Vector3(0, 60, 0);
                        newRiver = (GameObject)GameObject.Instantiate(World.instance.river, 
                            new Vector3(xx - 0.85f, h, yy + 0.48f),
                            Quaternion.identity);
                        newRiver.transform.eulerAngles = new Vector3(0, 120, 0);

                        newRiver.transform.parent = World.instance.transform;
                    }
                    else if (path.x == path.prevNode.x - 1 && path.y == path.prevNode.y)
                    {
                        newRiver.transform.eulerAngles = new Vector3(0, -60, 0);
                        newRiver = (GameObject)GameObject.Instantiate(World.instance.river, 
                            new Vector3(xx + 0.85f, h, yy + 0.48f),
                            Quaternion.identity);
                        newRiver.transform.eulerAngles = new Vector3(0, -120, 0);

                        newRiver.transform.parent = World.instance.transform;
                    }
                    else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x + 0.85f) < 0.1f && path.prevNode.y < path.y)
                    {
                        newRiver.transform.eulerAngles = new Vector3(0, -60, 0);
                        newRiver = (GameObject)GameObject.Instantiate(World.instance.river, 
                            new Vector3(xx + 0.85f, h, yy + 0.47f),
                            Quaternion.identity);
                        newRiver.transform.eulerAngles = new Vector3(0, 0, 0);

                        newRiver.transform.parent = World.instance.transform;
                    }
                    else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x + 0.85f) < 0.1f && path.prevNode.y > path.y)
                    {
                        newRiver.transform.eulerAngles = new Vector3(0, 180, 0);
                        newRiver = (GameObject)GameObject.Instantiate(World.instance.river, 
                            new Vector3(xx, h, yy + 1.95f),
                            Quaternion.identity);
                        newRiver.transform.eulerAngles = new Vector3(0, 240, 0);

                        newRiver.transform.parent = World.instance.transform;
                    }
                    else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x - 0.85f) < 0.1f && path.prevNode.y < path.y)
                    {
                        newRiver.transform.eulerAngles = new Vector3(0, 60, 0);
                        newRiver = (GameObject)GameObject.Instantiate(World.instance.river, 
                            new Vector3(xx - 0.85f, h, yy + 0.47f),
                            Quaternion.identity);
                        newRiver.transform.eulerAngles = new Vector3(0, 0, 0);

                        newRiver.transform.parent = World.instance.transform;
                    }
                    else if (Mathf.Abs(xx - World.hexagons[path.prevNode.x, path.prevNode.y].transform.position.x - 0.85f) < 0.1f && path.prevNode.y > path.y)
                    {
                        newRiver.transform.eulerAngles = new Vector3(0, 180, 0);
                        newRiver = (GameObject)GameObject.Instantiate(World.instance.river, 
                            new Vector3(xx, h, yy + 1.95f),
                            Quaternion.identity);
                        newRiver.transform.eulerAngles = new Vector3(0, -240, 0);

                        newRiver.transform.parent = World.instance.transform;
                    }
                }
            }

            path = path.prevNode;
        }
    }

    public static PathfindingNode GetOptimalRiverPath(int i1, int j1, int i2, int j2)
    {
        Stack<Tile> toHandle = new Stack<Tile>();
        toHandle.Push(World.tiles[i1, j1]);

        Dictionary<Tile, PathfindingNode> pathInfo = new Dictionary<Tile, PathfindingNode>();

        pathInfo.Add(World.tiles[i1, j1], new PathfindingNode(0, null, i1, j1));

        while (toHandle.Count > 0)
        {
            Tile handling = toHandle.Pop();
            int dist = pathInfo[handling].shortestDistance;
            int weight = (containsRiver[handling.xIndex, handling.yIndex]) ? 1 : 4;

            if (dist < 100 && handling.xIndex < World.gridWidth && handling.yIndex < World.gridWidth)
            {
                foreach (Tile neighbour in handling.GetNeighbors())
                    if (!pathInfo.ContainsKey(neighbour))
                    {
                        pathInfo.Add(neighbour, new PathfindingNode(dist + weight, pathInfo[handling], neighbour.xIndex, neighbour.yIndex));
                        toHandle.Push(neighbour);
                    }
                    else if (dist + weight < pathInfo[neighbour].shortestDistance)
                    {
                        pathInfo[neighbour] = new PathfindingNode(dist + weight, pathInfo[handling], neighbour.xIndex, neighbour.yIndex);
                        toHandle.Push(neighbour);
                    }
            }
        }

        if (pathInfo.ContainsKey(World.tiles[i2, j2]))
            return pathInfo[World.tiles[i2, j2]];
        return null;
    }
}
