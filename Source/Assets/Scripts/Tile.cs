﻿using UnityEngine;
using System.Collections.Generic;

public class Tile : MonoBehaviour
{
    public Transform city;
    public TileDescriptor descriptor;
    public Building building = null;
    public bool containsRoad = false;
    public List<GameObject> tileDecorations = new List<GameObject>();

    public int xIndex;
    public int yIndex;

    public float happinessFactor = 1f;

    public List<Tile> neighbours = new List<Tile>();

    public GameObject AddBuilding(Transform prefab, bool canBuildOnRoad)
    {
        if (prefab == null || isOccupied)
            return null;
        if (!canBuildOnRoad && containsRoad)
            return null;
        Player.hasStartedPlaying = true;

        Vector3 position = transform.position;
        var buildingObject = Instantiate(prefab.gameObject, new Vector3(position.x, position.y + 1.5f, position.z), Quaternion.identity) as GameObject;
        buildingObject.transform.parent = World.instance.transform;

        building = buildingObject.GetComponent<Building>();
        building.tile = this;

        tileDecorations.ForEach(Destroy);
        if (OverlayHandler.lastOverlay == OverlayType.Building)
            OverlayHandler.crosses[xIndex, yIndex].ForEach(r => r.enabled = true);

        return buildingObject;
    }

    public void addPopulation(int amount, City masterCity)
    {
        if (!isOccupied && descriptor.canHoldTown)
        {
            GameObject cityObject = AddBuilding(city, true);
            if (cityObject == null)
                return;

            City cityComponent = cityObject.GetComponent<Building>() as City;
            cityComponent.population = amount;
            cityComponent.masterCity = masterCity;
            masterCity.citiesInGroup.Add(cityComponent);

            if (!World.instance.simpleMode && SelectionHandler.oldHexagons2.Contains(masterCity.tile.gameObject))
                masterCity.SelectAllLinkedCities();
        }
    }

    public List<Tile> GetNeighbors()
    {
        if (neighbours.Count == 0)
        {
            var list = new List<Tile>();
            World world = World.instance;
            Tile tile = null;

            if (tile = world.GetTile(xIndex + 1, yIndex))
                list.Add(tile);
            if (tile = world.GetTile(xIndex - 1, yIndex))
                list.Add(tile);
            if (tile = world.GetTile(xIndex, yIndex - 1))
                list.Add(tile);
            if (tile = world.GetTile(xIndex, yIndex + 1))
                list.Add(tile);

            if (yIndex % 2 == 0)
            {
                if (tile = world.GetTile(xIndex - 1, yIndex - 1))
                    list.Add(tile);
                if (tile = world.GetTile(xIndex - 1, yIndex + 1))
                    list.Add(tile);
            }
            else
            {
                if (tile = world.GetTile(xIndex + 1, yIndex - 1))
                    list.Add(tile);
                if (tile = world.GetTile(xIndex + 1, yIndex + 1))
                    list.Add(tile);
            }

            foreach (Tile t in list)
                neighbours.Add(t);
        }
        return neighbours;
    }

    public List<Tile> GetAvailableNeighbors()
    {
        return GetNeighbors().FindAll(obj => obj.allowsGrowth);
    }

    public void DecrementHappiness(float val)
    {
        happinessFactor -= Mathf.Max(0, val);
        if (happinessFactor < 0)
        {
            Player.excessivePollutionSum -= happinessFactor;
            happinessFactor = 0;
        }
    }

    public bool canPlaceBuilding(Transform buildingPrefab)
    {
        if (buildingPrefab.GetComponent<PlayerBuilding>().descriptor.buildableTiles.Contains(descriptor))
        if (World.canContainPlayerBuildings[xIndex, yIndex])
            return true;

        return false;
    }

    public bool isOccupied { get { return building != null; } }

    public bool allowsGrowth { get { return descriptor.canHoldTown && (!isOccupied || building.allowsGrowth); } }
}
