﻿using UnityEngine;
using System.Collections.Generic;

public class SuggestionHandler : MonoBehaviour
{
    static SuggestionHandler instance;
    // Neural network informations
    const int inputCount = 21;
    public static float[] inputValues = new float[inputCount];
    const int outputCount = 7;
    public static List<float[,]> weightsPerLayer;

    // Information related to the map
    static float[,] distanceToNearestCity;
    static int[,] nearestCityPositionX;
    static int[,] nearestCityPositionY;
    static float[,] distanceToSecondNearestCity;
    static int[,] secondNearestCityPositionX;
    static int[,] secondNearestCityPositionY;

    // Activation
    public TextAsset file;
    public bool activate = false;
    public static bool isOn = false;

    // Run information
    public static float nextPlay;

    void Start()
    {
        instance = this;
    }

    void Update()
    {
        if (activate && !isOn)
        {
            isOn = true;
            nextPlay = Time.time + 1f;
            InitNeuralNetwork();
            SetMinimumDistancesToCities();
        }
        else if (isOn && activate && Time.time >= nextPlay)
        {
            nextPlay += 1f;
            PlayRound();
        }

        if (isOn)
            CameraOverride();
    }

    static void SetMinimumDistancesToCities()
    {
        distanceToNearestCity = new float[World.gridWidth, World.gridWidth];
        nearestCityPositionX = new int[World.gridWidth, World.gridWidth];
        nearestCityPositionY = new int[World.gridWidth, World.gridWidth];
        distanceToSecondNearestCity = new float[World.gridWidth, World.gridWidth];
        secondNearestCityPositionX = new int[World.gridWidth, World.gridWidth];
        secondNearestCityPositionY = new int[World.gridWidth, World.gridWidth];

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
            {
                float minDistSquared = HexagonHelper.DistanceFromIndexSquared(i, j, World.citiesCoordX[0], World.citiesCoordY[0]);
                float secondMinDistSquared = HexagonHelper.DistanceFromIndexSquared(i, j, World.citiesCoordX[1], World.citiesCoordY[1]);
                if (minDistSquared < secondMinDistSquared)
                {
                    nearestCityPositionX[i, j] = World.citiesCoordX[0];
                    nearestCityPositionY[i, j] = World.citiesCoordY[0];
                    secondNearestCityPositionX[i, j] = World.citiesCoordX[1];
                    secondNearestCityPositionY[i, j] = World.citiesCoordY[1];
                }
                else
                {
                    float tmp = minDistSquared;
                    minDistSquared = secondMinDistSquared;
                    secondMinDistSquared = tmp;
                    nearestCityPositionX[i, j] = World.citiesCoordX[1];
                    nearestCityPositionY[i, j] = World.citiesCoordY[1];
                    secondNearestCityPositionX[i, j] = World.citiesCoordX[0];
                    secondNearestCityPositionY[i, j] = World.citiesCoordY[0];
                }

                for (int k = 2; k < World.cities.Count; k++)
                {
                    float d = HexagonHelper.DistanceFromIndexSquared(i, j, World.citiesCoordX[k], World.citiesCoordY[k]);
                    if (d < minDistSquared)
                    {
                        secondMinDistSquared = minDistSquared;
                        secondNearestCityPositionX[i, j] = nearestCityPositionX[i, j];
                        secondNearestCityPositionY[i, j] = nearestCityPositionY[i, j];

                        minDistSquared = d;
                        nearestCityPositionX[i, j] = World.citiesCoordX[k];
                        nearestCityPositionY[i, j] = World.citiesCoordY[k];
                    }
                    else if (d < secondMinDistSquared)
                    {
                        secondMinDistSquared = d;
                        secondNearestCityPositionX[i, j] = World.citiesCoordX[k];
                        secondNearestCityPositionY[i, j] = World.citiesCoordY[k];
                    }
                }
                distanceToNearestCity[i, j] = Mathf.Sqrt(minDistSquared);
                distanceToSecondNearestCity[i, j] = Mathf.Sqrt(secondMinDistSquared);
            }
    }

    public static void InitNeuralNetwork()
    {
        weightsPerLayer = new List<float[,]>();
        weightsPerLayer.Add(new float[28, inputCount + 1]); // NN trained on 28 neurons in the hidden layer
        weightsPerLayer.Add(new float[outputCount, 29]);

        SetWeightsFromFile();
    }

    // Not a general code, works only as an optimized hack for the current context (1 layer, 3 neurons per output)
    public static float GetOutputForTileForBuilding(int x, int y, int building)
    {
        SetInputForTile(x, y);

        // First hidden layer, hardcoded to take from input directly
        const int oldLayerCount = inputCount;
        float[] newLayer = new float[28];

        float a = 0;
        for (int j = building * 4; j < building * 4 + 4; j++)
        {
            a = 0;
            for (int k = 0; k < oldLayerCount; k++)
                a += weightsPerLayer[0][j, k] * inputValues[k];
            a -= weightsPerLayer[0][j, oldLayerCount];  // Bias
            newLayer[j] = a / (1 + Mathf.Abs(a));       // True sigmoid is slower : 1f / (1f + Mathf.Exp(-a));
        }

        // Output layer, hardcoded to store directly in output
        a = 0;
        for (int k = building * 4; k < building * 4 + 4; k++) // Ugly hack, each output actually takes into account 4 neurons from the hidden layer
            a += weightsPerLayer[1][building, k] * newLayer[k];
        return (a / (1 + Mathf.Abs(a)));       // True sigmoid is slower : 1f / (1f + Mathf.Exp(-a));
    }

    public static void SetInputForPlayer()
    {
        inputValues[0] = Player.funds * 0.00001f;
        inputValues[1] = Player.currentPower * 0.001f;
        inputValues[2] = Player.maximumPower * 0.001f;
        inputValues[3] = Player.seasonConsumptionMult;
        inputValues[4] = Player.seasonConsumptionMultDif;
        inputValues[5] = (1 - Player.currentPower / Player.maximumPower);
        inputValues[6] = Player.nonProductiveCities;
        inputValues[7] = 10000 / Player.funds;
        inputValues[8] = Player.upkeepAtLastTick * 0.01f;
        inputValues[9] = Player.productionAtLastTick * 0.01f;
        inputValues[10] = Player.powerConsumptionAtLastTick * 0.01f;
        inputValues[11] = Player.powerProductionAtLastTick * 0.01f;
        inputValues[12] = Player.ecoTaxAtLastTick * 0.01f;
    }

    public static void SetInputForTile(int x, int y)
    {
        Tile t = World.tiles[x, y];
        City c1 = World.tiles[nearestCityPositionX[x, y], nearestCityPositionY[x, y]].building as City;
        City c2 = World.tiles[secondNearestCityPositionX[x, y], secondNearestCityPositionY[x, y]].building as City;

        inputValues[13] = t.descriptor.sunlight;
        inputValues[14] = t.descriptor.townGrowthMultiplier;
        inputValues[15] = (c1.lastIncome / ((c1.lastPopulationSum + 1) * 0.01f));
        inputValues[16] = distanceToNearestCity[x, y] * 0.1f;
        inputValues[17] = (c2.lastIncome / ((c2.lastPopulationSum + 1) * 0.01f));
        inputValues[18] = distanceToSecondNearestCity[x, y] * 0.1f;
        inputValues[19] = t.happinessFactor;
        inputValues[20] = WindController.windLevel[x, y] * t.descriptor.windStrength;
    }

    public static void SetWeightsFromFile()
    {
        string[] lines = instance.file.text.Split('\n');

        int currentLayer = 0;
        int currentNeurone = 0;

        for (int l = 4; l < lines.Length; l++)
        {
            string[] entries = lines[l].Split(' ');

            if (entries.Length == 1)
            {
                currentLayer++;
                currentNeurone = 0;
            }
            else if (!entries[0].Contains("%"))
            {  // Comment
                for (int i = 0; i < entries.Length; i++)
                    weightsPerLayer[currentLayer][currentNeurone, i] = float.Parse(entries[i]);
                currentNeurone++;
            }
        }
    }

    public void PlayRound()
    {
        transform.position = new Vector3(-1000, -1000, -1000);

        if (HexagonController.buildingGhost != null)
        {
            int buildingToPlace = 0;
            while (Player.instance.availableBuildings[buildingToPlace] != HexagonController.buildingGhost.GetComponent<BuildingGhost>().buildingPrefab)
                buildingToPlace++;

            float highestResult = float.MinValue;
            int keptXIndex = -1;
            int keptYIndex = -1;

            SetInputForPlayer();
            Transform building = Player.instance.availableBuildings[buildingToPlace];
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                {
                    Tile t = World.tiles[i, j];
                    if (!t.containsRoad && !t.isOccupied && t.canPlaceBuilding(building))
                    {
                        float neuralResult = GetOutputForTileForBuilding(i, j, buildingToPlace);
                        if (neuralResult > highestResult)
                        {
                            highestResult = neuralResult;
                            keptXIndex = i;
                            keptYIndex = j;
                        }
                    }
                }
            
            // Show kept solution
            if (highestResult > 0)
            {
                Transform b = Player.instance.availableBuildings[buildingToPlace];
                Vector3 tilePosition = World.tiles[keptXIndex, keptYIndex].transform.position;
                transform.position = new Vector3(tilePosition.x, tilePosition.y + 1.5f, tilePosition.z);
                GetComponent<MeshFilter>().mesh = b.GetComponent<PlayerBuilding>().descriptor.mesh;
            }
        }
    }

    bool isCenteringSuggestion = false;

    void CameraOverride()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
            isCenteringSuggestion = true;

        if (isCenteringSuggestion)
        {
            if (System.Math.Abs(transform.position.x - -1000) > 0.1f)
            {
                Vector3 viewPos = Camera.main.WorldToViewportPoint(transform.position);
                float xMovement = viewPos.x - 0.5f;
                float yMovement = viewPos.y - 0.5f;
                Vector3 movement = new Vector3(xMovement + yMovement, 0f, yMovement - xMovement) * 10;
                CameraControls._centerTargetPosition += movement * CameraControls.zoom * Time.deltaTime;
                if (Mathf.Abs(xMovement) + Mathf.Abs(yMovement) < 0.1f)
                    isCenteringSuggestion = false;
            }
            else
                isCenteringSuggestion = false;
        }
    }
}
