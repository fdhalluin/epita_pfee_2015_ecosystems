﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif //UNITY_EDITOR
using System.Collections.Generic;
using System.Text;
using System.IO;

public class NeuralNetwork : MonoBehaviour
{
    public static NeuralNetwork instance;

    // Neural network informations
    const int inputCount = 21;
    public static float[] inputValues = new float[inputCount];
    const int outputCount = 7;
    static List<int> neuronsCountPerLayer;
    static int hiddenLayerCount;
    public static List<float[,]> weightsPerLayer;

    // Backup to avoid reloading every failed attempts
    static List<float[,]> backupWeightsPerLayer;
    static string backupPath;

    // Information related to the map
    static float[,] distanceToNearestCity;
    static int[,] nearestCityPositionX;
    static int[,] nearestCityPositionY;
    static float[,] distanceToSecondNearestCity;
    static int[,] secondNearestCityPositionX;
    static int[,] secondNearestCityPositionY;

    // Activation
    public TextAsset file;
    public bool activate = false;
    public bool saturateComputations = true;
    public static bool isOn = false;

    // Run information
    float nextPlay;
    public static int roundsSurvived = 0;
    static int errorsThisRound = 0;
    static int currentRunnerRecord = 0;
    static string currentRunnerPath;
    static int numberOfGenerations;
    SortedDictionary<string, int> playerBuildings = new SortedDictionary<string, int>();
    SortedDictionary<string, int> causeOfLoss = new SortedDictionary<string, int>();

    // Editor inputs
    public bool saveCurrent = false;
    [Header("Hidden Layers")]
    public List<int> neuronsPerLayer;
    [Header("Mutation")]
    public int minMutationPerIteration = 4;
    public int maxMutationPerIteration = 30;
    public bool loopMutationCount = true;
    int currentMutationPerIteration = 0;
    public float lowMutation = 0.8f;
    public float highMutation = 1.2f;
    public int negPercent = 20;
    public int randPercent = 5;
    public int fullRandomThreshold = 200;
    [Header("Sampling")]
    public int sampleSize = 5;
    int currentSampleScore = 0;
    public static int currentSampleIndex = 0;
    int currentSampleExtreme = 0;
    [Header("Display")]
    public int graphSize = 3;
    public bool showGraph = false;
    public bool showProgress = false;

	
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        neuronsCountPerLayer = neuronsPerLayer;    // Set the static variable from the editor variable
        hiddenLayerCount = neuronsPerLayer.Count;
    }

    #if UNITY_EDITOR
    void Update()
    {
        if (activate && !isOn)
        {
            isOn = true;
            nextPlay = Time.time + 0.05f;
            currentRunnerPath = AssetDatabase.GetAssetPath(file);
            roundsSurvived = 0;
            InitNeuralNetwork();
            SetMinimumDistancesToCities();
            NeuralNetworkDisplay.SetGraphFromNN(graphSize);
            Player.gameSpeed = saturateComputations ? 5f : 20f;
            numberOfGenerations = 0;
            currentMutationPerIteration = minMutationPerIteration;
        }
        else if (isOn && activate)
        {
            if (saturateComputations && !World.softReset)
            {
                QualitySettings.SetQualityLevel(1);
                float start = Time.realtimeSinceStartup;
                while (Time.realtimeSinceStartup - start < 0.1f)
                {
                    roundsSurvived++;
                    Player.instance.PlayRound();
                    PlayRound();

                    CheckGameOver();
                }

            }
            else if (Time.time > nextPlay)
            {
                nextPlay += 0.05f;
                roundsSurvived++;
                PlayRound();

                CheckGameOver();
            }

            if (saveCurrent)
            {
                saveCurrent = false;
                string path = currentRunnerPath;
                string newPath = path.Split('.')[0] + "_bkp.txt";
                Debug.Log(newPath + " saved");
                SaveNeuralNetwork(newPath, roundsSurvived);
            }
        }
    }

    void CheckGameOver()
    {
        if (Player.powerProductionAtLastTick >= Player.powerConsumptionAtLastTick * 1.3f)
            errorsThisRound++;      // Excessive production
        if (-Player.upkeepAtLastTick - Player.ecoTaxAtLastTick + Player.instance.baseIncome * Player.gameSpeed + Player.productionAtLastTick < -10)
            errorsThisRound++;      // Losing too much money
        if (Player.powerProductionAtLastTick < Player.upkeepAtLastTick)
            errorsThisRound += 2;   // Not a good build order

        if (Player.funds < 0 || Player.currentPower <= 0 || Player.nonProductiveCities > 2 || errorsThisRound > 60)
            GameOver();
    }

    static void SetMinimumDistancesToCities()
    {
        distanceToNearestCity = new float[World.gridWidth, World.gridWidth];
        nearestCityPositionX = new int[World.gridWidth, World.gridWidth];
        nearestCityPositionY = new int[World.gridWidth, World.gridWidth];
        distanceToSecondNearestCity = new float[World.gridWidth, World.gridWidth];
        secondNearestCityPositionX = new int[World.gridWidth, World.gridWidth];
        secondNearestCityPositionY = new int[World.gridWidth, World.gridWidth];

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
            {
                float minDistSquared = HexagonHelper.DistanceFromIndexSquared(i, j, World.citiesCoordX[0], World.citiesCoordY[0]);
                float secondMinDistSquared = HexagonHelper.DistanceFromIndexSquared(i, j, World.citiesCoordX[1], World.citiesCoordY[1]);
                if (minDistSquared < secondMinDistSquared)
                {
                    nearestCityPositionX[i, j] = World.citiesCoordX[0];
                    nearestCityPositionY[i, j] = World.citiesCoordY[0];
                    secondNearestCityPositionX[i, j] = World.citiesCoordX[1];
                    secondNearestCityPositionY[i, j] = World.citiesCoordY[1];
                }
                else
                {
                    float tmp = minDistSquared;
                    minDistSquared = secondMinDistSquared;
                    secondMinDistSquared = tmp;
                    nearestCityPositionX[i, j] = World.citiesCoordX[1];
                    nearestCityPositionY[i, j] = World.citiesCoordY[1];
                    secondNearestCityPositionX[i, j] = World.citiesCoordX[0];
                    secondNearestCityPositionY[i, j] = World.citiesCoordY[0];
                }

                for (int k = 2; k < World.cities.Count; k++)
                {
                    float d = HexagonHelper.DistanceFromIndexSquared(i, j, World.citiesCoordX[k], World.citiesCoordY[k]);
                    if (d < minDistSquared)
                    {
                        secondMinDistSquared = minDistSquared;
                        secondNearestCityPositionX[i, j] = nearestCityPositionX[i, j];
                        secondNearestCityPositionY[i, j] = nearestCityPositionY[i, j];

                        minDistSquared = d;
                        nearestCityPositionX[i, j] = World.citiesCoordX[k];
                        nearestCityPositionY[i, j] = World.citiesCoordY[k];
                    }
                    else if (d < secondMinDistSquared)
                    {
                        secondMinDistSquared = d;
                        secondNearestCityPositionX[i, j] = World.citiesCoordX[k];
                        secondNearestCityPositionY[i, j] = World.citiesCoordY[k];
                    }
                }
                distanceToNearestCity[i, j] = Mathf.Sqrt(minDistSquared);
                distanceToSecondNearestCity[i, j] = Mathf.Sqrt(secondMinDistSquared);
            }
    }

    public static void InitNeuralNetwork()
    {
        weightsPerLayer = new List<float[,]>();
        weightsPerLayer.Add(new float[neuronsCountPerLayer[0], inputCount + 1]);
        for (int i = 1; i < hiddenLayerCount; i++)
            weightsPerLayer.Add(new float[neuronsCountPerLayer[i], neuronsCountPerLayer[i - 1] + 1]);
        weightsPerLayer.Add(new float[outputCount, neuronsCountPerLayer[hiddenLayerCount - 1] + 1]);

        SetWeightsFromFile();
    }

    public static float[] GetOutputForTile(int x, int y)
    {
        SetInputForTile(x, y);

        // First hidden layer, hardcoded to take from input directly
        int oldLayerCount = inputCount;
        float[] oldLayer;
        int l = neuronsCountPerLayer[0];
        float[] newLayer = new float[l];

        for (int j = 0; j < l; j++)
        {
            float a = 0;
            for (int k = 0; k < oldLayerCount; k++)
                a += weightsPerLayer[0][j, k] * inputValues[k];
            a -= weightsPerLayer[0][j, oldLayerCount];  // Bias
            newLayer[j] = a / (1 + Mathf.Abs(a));       // True sigmoid is slower : 1f / (1f + Mathf.Exp(-a));
        }

        oldLayer = newLayer;
        oldLayerCount = l;

        // All other hidden layers
        for (int i = 1; i < hiddenLayerCount; i++)
        {
            l = neuronsCountPerLayer[i];
            newLayer = new float[l];

            for (int j = 0; j < l; j++)
            {
                float a = 0;
                for (int k = 0; k < oldLayerCount; k++)
                    a += weightsPerLayer[i][j, k] * oldLayer[k];
                a -= weightsPerLayer[i][j, oldLayerCount];  // Bias
                newLayer[j] = a / (1 + Mathf.Abs(a));       // True sigmoid is slower : 1f / (1f + Mathf.Exp(-a));
            }

            oldLayer = newLayer;
            oldLayerCount = l;
        }

        // Output layer, hardcoded to store directly in output
        float[] outputLayer = new float[outputCount];
        for (int i = 0; i < outputCount; i++)
        {
            float a = 0;
            for (int k = i * 4; k < i * 4 + 4; k++) // Ugly hack, each output actually takes into account 4 neurons from the hidden layer
                a += weightsPerLayer[hiddenLayerCount][i, k] * oldLayer[k];
            outputLayer[i] = a / (1 + Mathf.Abs(a));       // True sigmoid is slower : 1f / (1f + Mathf.Exp(-a));
        }

        return outputLayer;
    }

    // Not a general code, works only as an optimized hack for the current context (1 layer, 3 neurons per output)
    public static float GetOutputForTileForBuilding(int x, int y, int building)
    {
        SetInputForTile(x, y);

        // First hidden layer, hardcoded to take from input directly
        const int oldLayerCount = inputCount;
        float[] newLayer = new float[neuronsCountPerLayer[0]];

        float a = 0;
        for (int j = building * 4; j < building * 4 + 4; j++)
        {
            a = 0;
            for (int k = 0; k < oldLayerCount; k++)
                a += weightsPerLayer[0][j, k] * inputValues[k];
            a -= weightsPerLayer[0][j, oldLayerCount];  // Bias
            newLayer[j] = a / (1 + Mathf.Abs(a));       // True sigmoid is slower : 1f / (1f + Mathf.Exp(-a));
        }

        // Output layer, hardcoded to store directly in output
        a = 0;
        for (int k = building * 4; k < building * 4 + 4; k++) // Ugly hack, each output actually takes into account 4 neurons from the hidden layer
            a += weightsPerLayer[1][building, k] * newLayer[k];
        return (a / (1 + Mathf.Abs(a)));       // True sigmoid is slower : 1f / (1f + Mathf.Exp(-a));
    }

    public static void SetInputForPlayer()
    {
        inputValues[0] = Player.funds * 0.00001f;
        inputValues[1] = Player.currentPower * 0.001f;
        inputValues[2] = Player.maximumPower * 0.001f;
        inputValues[3] = Player.seasonConsumptionMult;
        inputValues[4] = Player.seasonConsumptionMultDif;
        inputValues[5] = (1 - Player.currentPower / Player.maximumPower);
        inputValues[6] = Player.nonProductiveCities;
        inputValues[7] = 10000 / Player.funds;
        inputValues[8] = Player.upkeepAtLastTick * 0.01f;
        inputValues[9] = Player.productionAtLastTick * 0.01f;
        inputValues[10] = Player.powerConsumptionAtLastTick * 0.01f;
        inputValues[11] = Player.powerProductionAtLastTick * 0.01f;
        inputValues[12] = Player.ecoTaxAtLastTick * 0.01f;
    }

    public static void SetInputForTile(int x, int y)
    {
        Tile t = World.tiles[x, y];
        City c1 = World.tiles[nearestCityPositionX[x, y], nearestCityPositionY[x, y]].building as City;
        City c2 = World.tiles[secondNearestCityPositionX[x, y], secondNearestCityPositionY[x, y]].building as City;

        inputValues[13] = t.descriptor.sunlight;
        inputValues[14] = t.descriptor.townGrowthMultiplier;
        inputValues[15] = (c1.lastIncome / ((c1.lastPopulationSum + 1) * 0.01f));
        inputValues[16] = distanceToNearestCity[x, y] * 0.1f;
        inputValues[17] = (c2.lastIncome / ((c2.lastPopulationSum + 1) * 0.01f));
        inputValues[18] = distanceToSecondNearestCity[x, y] * 0.1f;
        inputValues[19] = t.happinessFactor;
        inputValues[20] = WindController.windLevel[x, y] * t.descriptor.windStrength;
    }

    public static void SetWeightsFromFile()
    {
        if (backupPath == currentRunnerPath)
        {
            for (int i = 0; i < hiddenLayerCount + 1; i++)
            {
                int l1 = backupWeightsPerLayer[i].GetLength(0);
                int l2 = backupWeightsPerLayer[i].GetLength(1);

                for (int n = 0; n < l1; n++)
                    for (int k = 0; k < l2; k++)
                        weightsPerLayer[i][n, k] = backupWeightsPerLayer[i][n, k];
            }
        }
        else
        {
            StreamReader r = new StreamReader(currentRunnerPath, Encoding.Default);

            string[] lines = r.ReadToEnd().Split('\n');

            int currentLayer = 0;
            int currentNeurone = 0;

            currentRunnerRecord = int.Parse(lines[0].Split(' ')[0]);

            for (int l = 4; l < lines.Length; l++)
            {
                string[] entries = lines[l].Split(' ');

                if (entries.Length == 1)
                {
                    currentLayer++;
                    currentNeurone = 0;
                }
                else if (!entries[0].Contains("%"))
                {  // Comment
                    for (int i = 0; i < entries.Length; i++)
                        weightsPerLayer[currentLayer][currentNeurone, i] = float.Parse(entries[i]);
                    currentNeurone++;
                }
            }

            r.Close();

            // Save backup for fallback on failed mutations
            backupPath = currentRunnerPath;
            backupWeightsPerLayer = new List<float[,]>();
            for (int i = 0; i < hiddenLayerCount + 1; i++)
            {
                int l1 = weightsPerLayer[i].GetLength(0);
                int l2 = weightsPerLayer[i].GetLength(1);

                backupWeightsPerLayer.Add(new float[l1, l2]);

                for (int n = 0; n < l1; n++)
                    for (int k = 0; k < l2; k++)
                        backupWeightsPerLayer[i][n, k] = weightsPerLayer[i][n, k];
            }
        }
    }

    static int buildingToPlace = 0;

    public void PlayRound()
    {
        SetInputForPlayer();
        int sum = 0;
        foreach (int v in playerBuildings.Values)
            sum += v;
        string s = Player.instance.availableBuildings[buildingToPlace].GetComponent<PlayerBuilding>().descriptor.inGameName;

        int iter = 2;   // If the first building isn't placed, attempt to place the following one directly
        while (iter-- > 0)
        {
            // The following makes sure a combination of at least 3 different types of buildings is used
            // And that all buildings appear at least 2% of the time
            if (sum < 50 || (playerBuildings.ContainsKey(s) && playerBuildings[s] > Mathf.FloorToInt(sum * 0.02f)))
            {
                while (true)
                {
                    buildingToPlace = (buildingToPlace + 1) % outputCount;
                    s = Player.instance.availableBuildings[buildingToPlace].GetComponent<PlayerBuilding>().descriptor.inGameName;
                    if (playerBuildings.ContainsKey(s) && playerBuildings[s] > sum * 0.33)
                        continue;
                    break;
                }
            }

            float highestResult = float.MinValue;
            int keptXIndex = -1;
            int keptYIndex = -1;

            Transform building = Player.instance.availableBuildings[buildingToPlace];
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                {
                    Tile t = World.tiles[i, j];
                    if (!t.containsRoad && !t.isOccupied && t.canPlaceBuilding(building))
                    {
                        float neuralResult = GetOutputForTileForBuilding(i, j, buildingToPlace);
                        if (neuralResult > highestResult)
                        {
                            highestResult = neuralResult;
                            keptXIndex = i;
                            keptYIndex = j;
                        }
                    }
                }

            // Build kept solution
            if (highestResult > 0)
            {
                Tile t = World.tiles[keptXIndex, keptYIndex];
                t.AddBuilding(building, false);
                if (t.building == null) // Wanted to place but failed, major error
                errorsThisRound += 10;
                else
                {
                    if (!playerBuildings.ContainsKey(building.GetComponent<PlayerBuilding>().descriptor.inGameName))
                        playerBuildings.Add(building.GetComponent<PlayerBuilding>().descriptor.inGameName, 1);
                    else
                        playerBuildings[building.GetComponent<PlayerBuilding>().descriptor.inGameName] += 1;
                }
                break;
            }
        }
        Player.hasStartedPlaying = true;
    }

    static string FloatArrayToString(float[] array)
    {
        string ret = "";
        for (int i = 0; i < array.Length; i++)
            ret += array[i].ToString("F3") + " ";
        return ret;
    }

    void ChangeNeuralNetwork(bool mutate)
    {
        currentSampleScore = 0;
        currentSampleIndex = 0;
        currentSampleExtreme = 0;
        playerBuildings = new SortedDictionary<string, int>();
        causeOfLoss = new SortedDictionary<string, int>();

        World.softReset = true;
        Player.InitiatePlayer();
        InitNeuralNetwork();

        if (mutate)
            MutateNeuralNetwork();
        else
            BuildRandomNetwork();
        
        NeuralNetworkDisplay.SetGraphFromNN(graphSize);
        NeuralNetworkDisplay.toDisplayScore[0] = "";
    }

    void GameOver()
    {
        if (roundsSurvived > currentSampleExtreme)
            currentSampleExtreme = roundsSurvived;
        currentSampleScore += roundsSurvived;
        currentSampleIndex++;
        StoreEndOfGameState();

        if (currentSampleIndex == 1 && currentRunnerRecord <= fullRandomThreshold && roundsSurvived <= fullRandomThreshold)
            ChangeNeuralNetwork(false);     // Full random until you get a good start
        else if (currentSampleIndex >= sampleSize)
        {
            numberOfGenerations++;
            int score = (currentSampleScore - currentSampleExtreme) / (sampleSize - 1);
                
            if (score > currentRunnerRecord && score > fullRandomThreshold)
            {
                string path = currentRunnerPath;
                string[] paths = path.Split('_');
                string newPath = paths[0] + "_" + (int.Parse(paths[1].Split('.')[0]) + 1) + ".txt";
                Debug.Log(newPath + " written, new record " + score + " (" + currentSampleScore + ") (" + Mathf.Floor(Time.time / 60) + "m)");

                SaveNeuralNetwork(newPath, score);
                currentRunnerPath = newPath;
                NeuralNetworkDisplay.PushAsGood(score);
                currentMutationPerIteration = minMutationPerIteration;
            }

            ChangeNeuralNetwork(true);      // Mutate this winner
        }
        else
        {    // Keep testing this sample
            int weightedScore = currentSampleScore;
            if (currentSampleIndex > 1)
                weightedScore = ((currentSampleScore - currentSampleExtreme) / (currentSampleIndex - 1));

            if (weightedScore <= Mathf.Max(fullRandomThreshold, currentRunnerRecord) * 0.9f * (1 + 0.12f * (float)currentSampleIndex / (float)sampleSize))
                ChangeNeuralNetwork(currentRunnerRecord > fullRandomThreshold);
            else
            {
                World.softReset = true;
                Player.InitiatePlayer();
                NeuralNetworkDisplay.toDisplayScore[0] = weightedScore + " (" + currentMutationPerIteration + ")";
            }
        }
        roundsSurvived = 0;
        errorsThisRound = 0;
    }

    void MutateNeuralNetwork()
    {
        for (int mutation = 0; mutation < currentMutationPerIteration; mutation++)
        {
            int i = Random.Range(0, hiddenLayerCount + 1);
            int n = Random.Range(0, weightsPerLayer[i].GetLength(0));
            int k = Random.Range(0, weightsPerLayer[i].GetLength(1));

            weightsPerLayer[i][n, k] *= Random.Range(lowMutation, highMutation);

            if (Random.Range(0, 100) < negPercent)
                weightsPerLayer[i][n, k] *= -1;
            if (Random.Range(0, 100) < randPercent)
                weightsPerLayer[i][n, k] = Random.Range(-10f, 10f);
        }

        if (currentMutationPerIteration < maxMutationPerIteration)
            currentMutationPerIteration++;
        else if (loopMutationCount)
            currentMutationPerIteration = minMutationPerIteration;
    }

    static void BuildRandomNetwork()
    {
        for (int i = 0; i < hiddenLayerCount + 1; i++)
        {
            int l1 = weightsPerLayer[i].GetLength(0);
            int l2 = weightsPerLayer[i].GetLength(1);

            for (int n = 0; n < l1; n++)
                for (int k = 0; k < l2; k++)
                    weightsPerLayer[i][n, k] = Random.Range(-10f, 10f);
        }
    }

    void StoreEndOfGameState()
    {
        string loss;
        if (Player.funds < 0)
            loss = "No money";
        else if (Player.currentPower <= 0)
            loss = "No power";
        else if (Player.nonProductiveCities > 2)
            loss = "Cities polluted";
        else if (errorsThisRound > 60)
            loss = "Too many errors";
        else
            loss = "Undefined";
        loss += " (" + SeasonController.instance.GetSeasonName().Substring(0, 3) + ")";

        if (!causeOfLoss.ContainsKey(loss))
            causeOfLoss.Add(loss, 1);
        else
            causeOfLoss[loss] += 1;
    }

    void SaveNeuralNetwork(string path, int score)
    {
        string toWrite = score + " % Rounds survived, generated after " + Mathf.RoundToInt(Time.time / 60) + " minutes (Gen: " + numberOfGenerations + ")\n";

        toWrite += "% ";
        foreach (string key in playerBuildings.Keys)
            toWrite += key + ": " + playerBuildings[key] + " \t";
        toWrite += "\n";
        toWrite += "% ";
        foreach (string key in causeOfLoss.Keys)
            toWrite += key + ": " + causeOfLoss[key] + " \t";
        toWrite += "\n\n";

        for (int i = 0; i < hiddenLayerCount + 1; i++)
        {
            float[,] w = weightsPerLayer[i];
            int l1 = w.GetLength(0);
            int l2 = w.GetLength(1);

            for (int n = 0; n < l1; n++)
            {
                for (int k = 0; k < l2; k++)
                {
                    if (k != 0)
                        toWrite += " ";
                    if (w[n, k] >= 0)
                        toWrite += "+";
                    toWrite += w[n, k].ToString("F4");
                }
                toWrite += "\n";
            }
            toWrite += "\n";
        }

        #if !UNITY_WEBPLAYER
            File.WriteAllText(path, toWrite);
        #endif
    }

    void OnGUI()
    {
        if (showGraph)
            NeuralNetworkDisplay.ShowNeuralNetwork();
        if (showProgress)
            NeuralNetworkDisplay.ShowNeuralNetworkProgress();
    }
    #endif //UNITY_EDITOR
}