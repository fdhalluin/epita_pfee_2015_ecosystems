﻿using UnityEngine;
using System.Collections.Generic;

public class NeuralNetworkDisplay
{
    public static Texture2D[] toDisplay = new Texture2D[8];
    public static string[] toDisplayScore = new string[8];
    public static Texture2D graph;
    public static int graphWidth;
    public static int graphHeight;
    public static List<Texture2D> nonDestroyedTextures = new List<Texture2D>();
    public static List<Texture2D> evolutionTextures = new List<Texture2D>();

    public static void SetGraphFromNN(int graphSize)
    {
        List<float[,]> network = NeuralNetwork.weightsPerLayer;

        int w = 0;
        int h = 0;
        float max = 0;

        for (int i = 0; i < network.Count; i++)
        {
            int l1 = network[i].GetLength(0);
            int l2 = network[i].GetLength(1);

            w = Mathf.Max(w, l2);
            h += l1;

            for (int n = 0; n < l1; n++)
                for (int k = 0; k < l2; k++)
                    if (Mathf.Abs(network[i][n, k]) > max)
                        max = Mathf.Abs(network[i][n, k]);
        }

        graphWidth = w * graphSize;
        graphHeight = h * graphSize;

        graph = new Texture2D(graphWidth, graphHeight);
        for (int i = 0; i < graphWidth; i++)
            for (int j = 0; j < graphHeight; j++)
                graph.SetPixel(i, j, Color.black);

        float invMax = 1f / max;

        int line = 0;
        for (int i = 0; i < network.Count; i++)
        {
            int l1 = network[i].GetLength(0);
            int l2 = network[i].GetLength(1);

            for (int n = 0; n < l1; n++)
            {
                for (int k = 0; k < l2; k++)
                {
                    int x = k * graphSize;
                    int y = line * graphSize;
                    float val = network[i][n, k];
                    Color col;
                    if (val >= 0)
                        col = Color.Lerp(Color.black, Color.green, Mathf.Sqrt(val * invMax));
                    else
                        col = Color.Lerp(Color.black, Color.red, Mathf.Sqrt(-val * invMax));
                    for (int xx = 0; xx < graphSize; xx++)
                        for (int yy = 0; yy < graphSize; yy++)
                            graph.SetPixel(x + xx, y + yy, col);
                }
                line++;
            }
        }

        graph.Apply();
        nonDestroyedTextures.Add(graph);

        toDisplay[0] = graph;
    }

    public static void PushAsGood(int score)
    {
        // Lerp the evolution of textures
        if (evolutionTextures.Count > 0)    // You can't lerp if there is nothing to lerp from
            for (int i = 1; i < 10; i++)
            {
                float lerp = i * 0.1f;
                Texture2D lerpedTexture = new Texture2D(graphWidth, graphHeight);
                for (int x = 0; x < graphWidth; x++)
                    for (int y = 0; y < graphHeight; y++)
                        lerpedTexture.SetPixel(x, y, Color.Lerp(toDisplay[1].GetPixel(x, y), graph.GetPixel(x, y), lerp));
                lerpedTexture.Apply();
                evolutionTextures.Add(lerpedTexture);
            }

        for (int i = 7; i > 1; i--)
        {
            toDisplay[i] = toDisplay[i - 1];
            toDisplayScore[i] = toDisplayScore[i - 1];
        }
        toDisplay[1] = graph;
        toDisplayScore[1] = score.ToString();

        evolutionTextures.Add(graph);

        nonDestroyedTextures.ForEach(t =>
            {
                if (!evolutionTextures.Contains(t))
                    Object.Destroy(t);
            });
        nonDestroyedTextures.RemoveAll(t =>
            (!evolutionTextures.Contains(t)));
    }

    public static void ShowNeuralNetwork()
    {
        if (NeuralNetwork.isOn)
        {
            GUI.Box(new Rect(0, Screen.height - graphHeight - 20, Screen.width, graphHeight + 20), "");
            for (int i = 0; i < 8; i++)
            {
                GUI.Box(new Rect(Screen.width - (i + 1) * graphWidth, Screen.height - graphHeight, graphWidth, graphHeight),
                    toDisplay[i]);
                GUI.Box(new Rect(Screen.width - (i + 1) * graphWidth, Screen.height - graphHeight - 20, graphWidth, 20),
                    toDisplayScore[i]);
            }
            GUI.Box(new Rect(Screen.width - graphWidth, Screen.height - graphHeight - 40, graphWidth, 20),
                NeuralNetwork.roundsSurvived.ToString());
        }
    }

    public static void ShowNeuralNetworkProgress()
    {
        if (evolutionTextures.Count > 0)
        {
            GUI.Box(new Rect(0, Screen.height - 2 * graphHeight - 20, graphWidth, graphHeight), "");
            GUI.Box(new Rect(0, Screen.height - 2 * graphHeight - 20, graphWidth, graphHeight),
                evolutionTextures[Mathf.RoundToInt(Time.time * 10) % evolutionTextures.Count]);
        }
    }
}