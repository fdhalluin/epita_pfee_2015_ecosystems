﻿using UnityEngine;
using System.Collections;

public class CameraControls : MonoBehaviour
{
    public float moveSpeed = 3;
    public float dampingFactor = .25f;
    public float axisThreshold = .25f;
	public int posMax;

    [Space(10f)]

    public static float zoom = 10;
    private float targetZoom;
    public float zoomSpeed = 3;
    public float zoomDampingFactor = .25f;
    public float minZoom = 1;
    public float maxZoom = 10;

    public float zoomOrthographicSizeFactor = 1;
    public float zoomDistanceFactor = 10;

    [Space(10f)]

    public int panButtonId = 2;

    private Vector2 _pressedMousePosition;
    private Vector3 _pressedCenterPosition;
    public Vector3 _centerPosition;
    public static Vector3 _centerTargetPosition;

    void Start()
    {
        targetZoom = zoom;
		posMax = World.gridWidth * 2 - 20;
    }

    void Update()
    {
        bool consoleIsClosed = InGameConsoleController.instance.console == null;

        float mouseScrollWheel = Input.GetAxis("Mouse ScrollWheel");
        if (consoleIsClosed && Input.GetKey(KeyCode.O))
            mouseScrollWheel += 0.1f;
        if (consoleIsClosed && Input.GetKey(KeyCode.L))
            mouseScrollWheel -= 0.1f;

        if (Mathf.Abs(mouseScrollWheel) > float.Epsilon)
        {
            targetZoom = Mathf.Clamp(zoom - zoomSpeed * mouseScrollWheel, minZoom, maxZoom);
        }
        if (Mathf.Abs(zoom - targetZoom) > float.Epsilon)
        {
            zoom = Utils.Damp(zoom, targetZoom, zoomDampingFactor, Time.deltaTime);
        }

        if (Input.GetMouseButtonDown(panButtonId))
        {
            _pressedMousePosition = Input.mousePosition;
            _pressedCenterPosition = _centerPosition;
        }

        if (Input.GetMouseButton(panButtonId))
        {
            var oldRay = GetComponent<Camera>().ScreenPointToRay(_pressedMousePosition);
            var oldGroundPosition = oldRay.origin + oldRay.direction * -oldRay.origin.y / oldRay.direction.y;
            var newRay = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            var newGroundPosition = newRay.origin + newRay.direction * -newRay.origin.y / newRay.direction.y;
            var offset = newGroundPosition - oldGroundPosition;
            _centerPosition = _pressedCenterPosition - offset;
            _centerTargetPosition = _centerPosition;
        }
        
        float xAxis = consoleIsClosed ? Input.GetAxis("Horizontal") : 0;
        float yAxis = consoleIsClosed ? Input.GetAxis("Vertical") : 0;

		float xMovement = Mathf.Abs (xAxis) > axisThreshold ? Mathf.Sign (xAxis) : 0;
		float yMovement = Mathf.Abs (yAxis) > axisThreshold ? Mathf.Sign (yAxis) : 0;
		

		// Convert to isometric movement
		Vector3 movement = new Vector3 (xMovement + yMovement, 0f, yMovement - xMovement) * moveSpeed;
		_centerTargetPosition += movement * zoom * Time.deltaTime;

		if (_centerTargetPosition.x < -5)
			_centerTargetPosition.x = -5;

		if (_centerTargetPosition.z < 10)
			_centerTargetPosition.z = 10;

		if (_centerTargetPosition.x > posMax)
			_centerTargetPosition.x = posMax;
		
		if (_centerTargetPosition.z > posMax)
			_centerTargetPosition.z = posMax;

		_centerPosition = Utils.Damp (_centerPosition, _centerTargetPosition, dampingFactor, Time.deltaTime);

	
		transform.position = _centerPosition - transform.localRotation * Vector3.forward * zoom * zoomDistanceFactor;
        GetComponent<Camera>().orthographicSize = zoom * zoomOrthographicSizeFactor;
    }
}
