﻿using UnityEngine;
using System.Collections;

public abstract class Building : MonoBehaviour
{
    [HideInInspector]
    public Tile tile;

    public abstract BuildingDescriptor getDescriptor { get; }

    void Awake()
    {
        Debug.Assert(getDescriptor != null);
        if (getDescriptor.mesh != null)
        {
            var meshFilter = GetComponent<MeshFilter>();
            Debug.Assert(meshFilter != null);
            meshFilter.mesh = getDescriptor.mesh;
        }
    }

    public abstract void OnTileSelected();

    public abstract void OnTileMouseOver();

    public abstract bool allowsGrowth { get; }
}
