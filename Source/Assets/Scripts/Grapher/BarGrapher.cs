﻿using UnityEngine;
using System.Collections;

public class BarGrapher : MonoBehaviour
{
	public Texture2D progressBarEmpty;
	public Texture2D progressBarFull;
	public int nmBar = 10;

	private int prevNmBar;
	private float[] barValues;
	private float panelWidth;
	private float panelHeight;
	float interLength = 2;
	private float barLength;

	void Start()
	{
		prevNmBar = nmBar;
		barValues = new float[nmBar];
		RectTransform rt = GetComponent<RectTransform> ();
		panelWidth = rt.rect.width;
		panelHeight = rt.rect.height;

		for (int i = 0; i < nmBar; ++i) {
			barValues[i] = Random.Range(0, panelHeight);
		}

		barLength = (panelWidth - (nmBar - 1)*interLength) / nmBar;
	}

	void OnGUI()
	{
		//GUI.BeginGroup(new Rect (0, 0, panelWidth, panelHeight));
		//GUI.Box(new Rect (0, 0, panelWidth, panelHeight), progressBarEmpty);

		GUI.BeginGroup(new Rect(0, 0, barLength, panelHeight));
		GUI.Box (new Rect(0, 0, barLength, barValues[0]), progressBarFull);
		GUI.EndGroup();
		float prev = 0;
		for (int i = 1; i < nmBar; ++i)
		{
			GUI.BeginGroup(new Rect(interLength + barLength + prev, 0,  barLength, panelHeight));
			GUI.Box (new Rect(0, 0,  barLength, barValues[i]), progressBarFull);
			GUI.EndGroup();
			prev = interLength + barLength + prev;
		}
		//GUI.EndGroup();
	}

	void Update()
	{
		if (prevNmBar != nmBar)
		{
			prevNmBar = nmBar;
			barValues = new float[nmBar];
			RectTransform rt = GetComponent<RectTransform> ();
			panelWidth = rt.rect.width;
			panelHeight = rt.rect.height;
			
			for (int i = 0; i < nmBar; ++i) {
				barValues [i] = Random.Range (0, panelHeight);
			}
			
			barLength = (panelWidth - (nmBar - 1) * interLength) / nmBar;
		}
	}
}