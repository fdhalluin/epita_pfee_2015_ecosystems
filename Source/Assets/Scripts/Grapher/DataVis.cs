﻿using UnityEngine;
using System.Collections.Generic;

public class DataVis : MonoBehaviour
{
    public static bool isDisplaying = false;
    public static bool requiresRedraw = false;
    public static Texture2D displayTexture;

    public static List<float> storedMoney = new List<float>();
    public static List<float> storedEnergy = new List<float>();
    public static List<float> storedMoneyTick = new List<float>();
    public static List<float> storedEnergyTick = new List<float>();
    /* public static List<float> storedUpkeep = new List<float>();
    public static List<float> storedEcoTax = new List<float>();
    public static List<float> storedProduction = new List<float>();
    public static List<float> storedPowerSold = new List<float>();
    public static List<float> storedPowerConsumption = new List<float>();
    public static List<float> storedPowerProduction = new List<float>();*/

    public static float storedMoneyRatio;
    public static float storedEnergyRatio;
    public static float storedMoneyTickRatio;
    public static float storedPowerTickRatio;

    Rect moneyRect;
    Rect powerRect;
    Rect moneyTickRect;
    Rect powerTickRect;

    public static void ResetValues()
    {
        storedMoney = new List<float>();
        storedEnergy = new List<float>();
        /*storedUpkeep = new List<float>();
        storedEcoTax = new List<float>();
        storedProduction = new List<float>();
        storedPowerSold = new List<float>();
        storedPowerConsumption = new List<float>();
        storedPowerProduction = new List<float>();*/
        storedMoneyTick = new List<float>();
        storedEnergyTick = new List<float>();
        storedMoneyRatio = 0.003f;
        storedEnergyRatio = 0.09f;
        storedMoneyTickRatio = 1f;
        storedPowerTickRatio = 0.75f;

        displayTexture = new Texture2D(250, 200);
        isDisplaying = false;
        requiresRedraw = true;
    }

    public static void PushNewValues()
    {
        if (NeuralNetwork.isOn)
            return;

        storedMoney.Add(Player.funds);
        storedEnergy.Add(Player.currentPower);
        storedMoneyTick.Add(5 * Player.gameSpeed + Player.productionAtLastTick + Player.powerSoldAtLastTick - Player.upkeepAtLastTick - Player.ecoTaxAtLastTick);
        storedEnergyTick.Add(Player.powerProductionAtLastTick - Player.powerConsumptionAtLastTick);
        /*storedUpkeep.Add(Player.upkeepAtLastTick);
        storedEcoTax.Add(Player.ecoTaxAtLastTick);
        storedProduction.Add(Player.productionAtLastTick);
        storedPowerSold.Add(Player.powerSoldAtLastTick);
        storedPowerConsumption.Add(Player.powerConsumptionAtLastTick);
        storedPowerProduction.Add(Player.powerProductionAtLastTick);*/

        if (Player.funds * storedMoneyRatio > 175f)
            storedMoneyRatio = 175f / Player.funds;
        if (Player.currentPower * storedEnergyRatio > 175f)
            storedEnergyRatio = 175f / Player.currentPower;
        if (Mathf.Abs(storedMoneyTick[storedMoneyTick.Count - 1]) * storedMoneyTickRatio > 85f)
            storedMoneyTickRatio = 85f / Mathf.Abs(storedMoneyTick[storedMoneyTick.Count - 1]);
        if (Mathf.Abs(storedEnergyTick[storedEnergyTick.Count - 1]) * storedPowerTickRatio > 85f)
            storedPowerTickRatio = 85f / Mathf.Abs(storedEnergyTick[storedEnergyTick.Count - 1]);
        /*if (Player.upkeepAtLastTick * storedMoneyTickRatio > 175f)
            storedMoneyTickRatio = 175f / Player.upkeepAtLastTick;
        if (Player.ecoTaxAtLastTick * storedMoneyTickRatio > 175f)
            storedMoneyTickRatio = 175f / Player.ecoTaxAtLastTick;
        if (Player.productionAtLastTick * storedMoneyTickRatio > 175f)
            storedMoneyTickRatio = 175f / Player.productionAtLastTick;
        if (Player.powerSoldAtLastTick * storedMoneyTickRatio > 175f)
            storedMoneyTickRatio = 175f / Player.powerSoldAtLastTick;
        if (Player.powerConsumptionAtLastTick * storedPowerTickRatio > 175f)
            storedPowerTickRatio = 175f / Player.powerConsumptionAtLastTick;
        if (Player.powerProductionAtLastTick * storedPowerTickRatio > 175f)
            storedPowerTickRatio = 175f / Player.powerProductionAtLastTick;*/

        requiresRedraw = true;
    }
    
    void OnGUI()
    {
        if (isDisplaying)
        {
            GUI.DrawTexture(new Rect(Screen.width * 0.5f - displayTexture.width, Screen.height * 0.5f - displayTexture.height, 2 * displayTexture.width, 2 * displayTexture.height),
                displayTexture);

            if (storedMoney.Count > 0)
            {
                GUI.Box(moneyRect, "Funds: " + (int)(storedMoney[storedMoney.Count - 1]));
                while (powerRect.Overlaps(moneyRect))
                    powerRect.x += 150;
                GUI.Box(powerRect, "Energy: " + (int)(storedEnergy[storedEnergy.Count - 1]));
                while (moneyTickRect.Overlaps(moneyRect) || moneyTickRect.Overlaps(powerRect))
                    moneyTickRect.x += 150;
                GUI.Box(moneyTickRect, "Funds variations: " + (int)(storedMoneyTick[storedMoneyTick.Count - 1]));
                while (powerTickRect.Overlaps(moneyRect) || powerTickRect.Overlaps(powerRect) || powerTickRect.Overlaps(moneyTickRect))
                    powerTickRect.x += 150;
                GUI.Box(powerTickRect, "Energy variations: " + (int)(storedEnergyTick[storedEnergyTick.Count - 1]));
            }
        }

        if (requiresRedraw)
        {
            requiresRedraw = false;
            displayTexture = new Texture2D(250, 200);
            int dataLen = storedMoney.Count;

            // Exit is there is no data
            if (dataLen == 0)
                return;

            float dataProgression = ((int)(Mathf.Max(dataLen / 250f, 1) * 100)) * 0.01f;

            int[] moneys = new int[250];
            int[] energies = new int[250];
            int[] moneyTicks = new int[250];
            int[] energyTicks = new int[250];
            /*int[] upkeeps = new int[250];
            int[] ecotaxs = new int[250];
            int[] productions = new int[250];
            int[] powerSolds = new int[250];
            int[] powerConsumptions = new int[250];
            int[] powerProductions = new int[250];*/

            for (int i = 0; i < Mathf.Min(250, dataLen); i++)
            {
                moneys[i] = (int)(storedMoney[(int)(i * dataProgression)] * storedMoneyRatio);
                energies[i] = (int)(storedEnergy[(int)(i * dataProgression)] * storedEnergyRatio);
                moneyTicks[i] = (int)(storedMoneyTick[(int)(i * dataProgression)] * storedMoneyTickRatio);
                energyTicks[i] = (int)(storedEnergyTick[(int)(i * dataProgression)] * storedPowerTickRatio);
                /*upkeeps[i] = (int)(storedUpkeep[(int)(i * dataProgression)] * storedMoneyTickRatio);
                ecotaxs[i] = (int)(storedEcoTax[(int)(i * dataProgression)] * storedMoneyTickRatio);
                productions[i] = (int)(storedProduction[(int)(i * dataProgression)] * storedMoneyTickRatio);
                powerSolds[i] = (int)(storedPowerSold[(int)(i * dataProgression)] * storedMoneyTickRatio);
                powerConsumptions[i] = (int)(storedPowerConsumption[(int)(i * dataProgression)] * storedPowerTickRatio);
                powerProductions[i] = (int)(storedPowerProduction[(int)(i * dataProgression)] * storedPowerTickRatio);*/
            }

            int minI = Mathf.Min(250, dataLen);
            for (int i = 1; i < minI; i++)
            {
                for (int j = Mathf.Min(moneys[i - 1], moneys[i]); j <= Mathf.Max(moneys[i - 1], moneys[i]); j++)
                    displayTexture.SetPixel(i, j, Color.green);
                for (int j = Mathf.Min(energies[i - 1], energies[i]); j <= Mathf.Max(energies[i - 1], energies[i]); j++)
                    displayTexture.SetPixel(i, j, Color.yellow);
                for (int j = 85 + Mathf.Min(moneyTicks[i - 1], moneyTicks[i]); j <= 85 + Mathf.Max(moneyTicks[i - 1], moneyTicks[i]); j++)
                    if (j >= 85)
                        displayTexture.SetPixel(i, j, Color.blue);
                    else
                        displayTexture.SetPixel(i, j, Color.red);
                for (int j = 85 + Mathf.Min(energyTicks[i - 1], energyTicks[i]); j <= 85 + Mathf.Max(energyTicks[i - 1], energyTicks[i]); j++)
                    if (j >= 85)
                        displayTexture.SetPixel(i, j, Color.cyan);
                    else
                        displayTexture.SetPixel(i, j, Color.red);
                /*for (int j = Mathf.Min(upkeeps[i - 1], upkeeps[i]); j <= Mathf.Max(upkeeps[i - 1], upkeeps[i]); j++)
                    displayTexture.SetPixel(i, j, Color.red);
                for (int j = Mathf.Min(ecotaxs[i - 1], ecotaxs[i]); j <= Mathf.Max(ecotaxs[i - 1], ecotaxs[i]); j++)
                    displayTexture.SetPixel(i, j, Color.red);
                for (int j = Mathf.Min(productions[i - 1], productions[i]); j <= Mathf.Max(productions[i - 1], productions[i]); j++)
                    displayTexture.SetPixel(i, j, Color.green);
                for (int j = Mathf.Min(powerSolds[i - 1], powerSolds[i]); j <= Mathf.Max(powerSolds[i - 1], powerSolds[i]); j++)
                    displayTexture.SetPixel(i, j, Color.green);
                for (int j = Mathf.Min(powerConsumptions[i - 1], powerConsumptions[i]); j <= Mathf.Max(powerConsumptions[i - 1], powerConsumptions[i]); j++)
                    displayTexture.SetPixel(i, j, Color.red);
                for (int j = Mathf.Min(powerProductions[i - 1], powerProductions[i]); j <= Mathf.Max(powerProductions[i - 1], powerProductions[i]); j++)
                    displayTexture.SetPixel(i, j, Color.green);*/
            }

            displayTexture.Apply();

            moneyRect = new Rect(Screen.width * 0.5f - displayTexture.width + 2 * minI, Screen.height * 0.5f - displayTexture.height + 390 - 2 * moneys[minI - 1], 150, 22);
            powerRect = new Rect(Screen.width * 0.5f - displayTexture.width + 2 * minI, Screen.height * 0.5f - displayTexture.height + 390 - 2 * energies[minI - 1], 150, 22);
            moneyTickRect = new Rect(Screen.width * 0.5f - displayTexture.width + 2 * minI, Screen.height * 0.5f - displayTexture.height + 220 - 2 * moneyTicks[minI - 1], 150, 22);
            powerTickRect = new Rect(Screen.width * 0.5f - displayTexture.width + 2 * minI, Screen.height * 0.5f - displayTexture.height + 220 - 2 * energyTicks[minI - 1], 150, 22);
        }
    }
}
