using UnityEngine;
using System.Collections;
using System.IO;


public class Logger : MonoBehaviour
{
	public static int[] score;
	public static int[] funds;
	public static int[] power;
	public static int[] population;
	public static int[] ecotax;
	int begining = 0;
	float frequency = 1f;
	bool begun = false;

	void Start()
	{
		score = new int[1];
		funds = new int[1];
		power = new int[1];
		population = new int[1];
		ecotax = new int[1];
	}

	void Update()
	{
		if (Player.hasStartedPlaying && begun == false)
		{
			begun = true;
			StartGraph();
		}
	}

	void StartGraph()
	{
		InvokeRepeating("ToRepeat", begining, frequency);
	}

	void ToRepeat()
	{
		//SCORE
		int tmp = (int)Player.score;
		int new_len = score.Length + 1;
		int[] new_score = new int[new_len];
		for (int i = 0; i < new_len - 1; i++)
			new_score[i] = score[i];
		new_score[new_len - 1] = tmp;
		score = new_score;
		string score_to_send = Concat(score);
		//FUNDS
		tmp = (int)Player.funds;
		new_len = funds.Length + 1;
		int[] new_funds = new int[new_len];
		for (int i = 0; i < new_len - 1; i++)
			new_funds[i] = funds[i];
		new_funds[new_len - 1] = tmp;
		funds = new_funds;
		string funds_to_send = Concat(funds);
		//POWER
		tmp = (int)Player.currentPower;
		new_len = power.Length + 1;
		int[] new_power = new int[new_len];
		for (int i = 0; i < new_len - 1; i++)
			new_power[i] = power[i];
		new_power[new_len - 1] = tmp;
		power = new_power;
		string power_to_send = Concat(power);
		//POPULATION
		tmp = (int)Player.totalPopulation;
		new_len = population.Length + 1;
		int[] new_population = new int[new_len];
		for (int i = 0; i < new_len - 1; i++)
			new_population[i] = population[i];
		new_population[new_len - 1] = tmp;
		population = new_population;
		string population_to_send = Concat(population);
		//ECOTAX
		tmp = (int)Player.ecoTaxAtLastTick;
		new_len = ecotax.Length + 1;
		int[] new_ecotax = new int[new_len];
		for (int i = 0; i < new_len - 1; i++)
			new_ecotax[i] = ecotax[i];
		new_ecotax[new_len - 1] = tmp;
		ecotax = new_ecotax;
		string ecotax_to_send = Concat(ecotax);
		Application.ExternalCall("graph", score_to_send, funds_to_send, power_to_send, population_to_send, ecotax_to_send);
	}

	string Concat(int[] score)
	{
		string res = System.String.Empty;
		for (int i = 0; i < score.Length - 1; ++i)
			res += score[i].ToString() + " ";
		res += score[score.Length - 1];
		return res;
	}

	void DisplayScore()
	{
		for (int i = 0; i < score.Length; i++)
			Debug.Log(score[i].ToString() + " ");
		Debug.Log("\n");
	}
}