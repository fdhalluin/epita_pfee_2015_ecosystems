﻿using UnityEngine;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    public static Player instance;

    public static float funds = 30000;
    public static int upkeep = 0;
    public static float gameSpeed = 1f;

    public static float powerGeneration = 0;
    public static float currentPower = 2000;
    public static int maximumPower = 2000;

    public float consumptionMult = 1;
    public float cityGrowthMult = 1;
    public float cityGrowthSpeed = 1;
    public float ecoTaxMult = 5;
    public int baseIncome = 5;
    // Anything higher than 0.5 can be abused for sale price
    public float powerSalePrice = 0.5f;

    public static float excessivePollutionSum = 0;
    public static int nonProductiveCities = 0;

    public int minFunds = 0;
    public List<Transform> availableBuildings;

    float _timeToTick;

    public static float tickFrequency { get; private set; }

    private static float tickDuration;

    public static bool hasStartedPlaying = false;
    public static float timeOfStart;
    public static float seasonConsumptionMult;
    public static float seasonConsumptionMultDif;

    // Information from last tick
    public static float upkeepAtLastTick;
    public static float ecoTaxAtLastTick;
    public static float productionAtLastTick;
    public static float powerSoldAtLastTick;
    public static float powerProductionAtLastTick;
    public static float powerConsumptionAtLastTick;

    public static float score;
    public static int totalPopulation;

    public void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        InitiatePlayer();
    }

    // Update is called once per frame
    void Update()
    {
        _timeToTick -= Time.deltaTime;
        if (_timeToTick < 0 && hasStartedPlaying && (!NeuralNetwork.instance.saturateComputations || !NeuralNetwork.isOn))
            PlayRound();
        if (!hasStartedPlaying)
        {
            timeOfStart = Time.time;
            _timeToTick = tickDuration;
        }

        // If console is open, ignore hotkeys
        if (InGameConsoleController.instance.console == null)
             HandleDebugInput();

        if (funds < minFunds && !GameOverPanel.instance.isGameOver && !NeuralNetwork.isOn)
            GameOverPanel.instance.Show();
    }

    public void PlayRound()
    {
        _timeToTick += tickDuration;
        float nextSeasonConsumptionMult = 1 + SeasonController.instance.currentSeasonMultiplier;
        seasonConsumptionMultDif = nextSeasonConsumptionMult - seasonConsumptionMult;
        seasonConsumptionMult = nextSeasonConsumptionMult;
        float overallConsumptionMult = seasonConsumptionMult * consumptionMult * 0.01f;

        // For data vis, these are important data to store once their value is set
        upkeepAtLastTick = 0;
        ecoTaxAtLastTick = 0;
        productionAtLastTick = 0;
        powerProductionAtLastTick = 0;
        powerSoldAtLastTick = 0;
        powerConsumptionAtLastTick = 0;

        upkeepAtLastTick = upkeep * gameSpeed;                                // Cout des infrastructures
        ecoTaxAtLastTick = excessivePollutionSum * gameSpeed * ecoTaxMult;    // Eco-taxe
        powerProductionAtLastTick = powerGeneration * gameSpeed;

        funds -= upkeepAtLastTick + ecoTaxAtLastTick;
        float income = baseIncome * gameSpeed + powerSoldAtLastTick;
        currentPower += powerProductionAtLastTick;

        // Get money from city and have them consume
        nonProductiveCities = 0;
        totalPopulation = 0;
        List<City> citiesToGrow = new List<City>();
        foreach (GameObject c in World.cities)
        {
            City master = c.GetComponent<City>();
            master.lastPopulationSum = 0;
            master.lastIncome = 0;
            master.lastConsumption = 0;

            foreach (City city in master.citiesInGroup)
            {
                citiesToGrow.Add(city);
                master.lastPopulationSum += city.population;
                master.lastConsumption -= city.population * overallConsumptionMult;

                currentPower -= city.population * overallConsumptionMult * gameSpeed;
                powerConsumptionAtLastTick += city.population * overallConsumptionMult * gameSpeed;
                if (currentPower >= 0)
                {
                    if (!city.canGather)
                    {
                        city.canGather = true;
                        city.GetComponent<BuildingGrowthHandler>().SetColor(Color.white);
                    }
                    master.lastIncome += city.population * city.tile.happinessFactor * 0.01f;
                }
                else
                {
                    if (city.canGather)
                    {
                        city.canGather = false;
                        city.GetComponent<BuildingGrowthHandler>().SetColor(Color.gray);
                    }
                    master.lastIncome -= city.population * 0.005f;
                    currentPower = 0;
                }
            }
            if (master.lastIncome < master.lastPopulationSum * 0.001f)  // Under 10% productivity
                nonProductiveCities++;
            income += master.lastIncome * gameSpeed;
            productionAtLastTick += master.lastIncome * gameSpeed;
            totalPopulation += master.lastPopulationSum;
        }

        funds += income;
        score += income;

        if (currentPower > maximumPower)
        {
            powerSoldAtLastTick = powerSalePrice * (currentPower - maximumPower);
            currentPower = maximumPower;
        }

        citiesToGrow.ForEach(city => city.populationTick(this));

        DataVis.PushNewValues();
    }

    public static void InitiatePlayer()
    {
        funds = 30000;
        upkeep = 0;
        powerGeneration = 0;
        currentPower = 2000;
        maximumPower = 2000;
        excessivePollutionSum = 0;
        nonProductiveCities = 0;
        tickFrequency = 10;
        tickDuration = 1 / tickFrequency;
        instance._timeToTick = tickDuration;
        timeOfStart = Time.time;
        seasonConsumptionMult = 1;

        powerProductionAtLastTick = 0;
        productionAtLastTick = 0;
        powerConsumptionAtLastTick = 0;
        powerSoldAtLastTick = 0;
        ecoTaxAtLastTick = 0;
        upkeepAtLastTick = 0;

        score = 0;
        DataVis.ResetValues();
    }

    static void HandleDebugInput()
    {
        // Debug change time flow
        if (Input.GetKeyDown(KeyCode.T))
            gameSpeed = (gameSpeed == 1f) ? 10f : 1f;
        // Pause game
        if (Input.GetKeyDown(KeyCode.P))
            gameSpeed = (gameSpeed == 1f) ? 0f : 1f;
        // Debug build a power plant
        if (!World.instance.simpleMode && SelectionHandler.oldHexagons2.Count == 1)
        {
            Transform building = Player.instance.availableBuildings[Random.Range(0, 4)];
            Tile t = SelectionHandler.oldHexagons2[0].GetComponent<Tile>();
            if (Input.GetKeyDown(KeyCode.B))
            if (building.GetComponent<PlayerBuilding>().descriptor.buildableTiles.Contains(t.descriptor))
            if (World.canContainPlayerBuildings[t.xIndex, t.yIndex])
                t.AddBuilding(building, false);
        }
        // Debug show happiness
        if (Input.GetKeyDown(KeyCode.H))
        {
            UnityEngine.UI.Toggle toggle = TogglePanel.instance.happinessToggle;
            toggle.isOn = !toggle.isOn;
        }
        // Debug show wind
        if (Input.GetKeyDown(KeyCode.J))
        {
            UnityEngine.UI.Toggle toggle = TogglePanel.instance.windToggle;
            toggle.isOn = !toggle.isOn;
        }
        // Debug change quality
        if (Input.GetKeyDown(KeyCode.Q))
        {
            UnityEngine.UI.Toggle toggle = TogglePanel.instance.qualityToggle;
            toggle.isOn = !toggle.isOn;
        }
        // Debug display upkeep, cash
        if (Input.GetKeyDown(KeyCode.D))
        {
            UnityEngine.UI.Toggle toggle = TogglePanel.instance.fundToggle;
            toggle.isOn = !toggle.isOn;
        }
        // Show ingame graph
        if (Input.GetKeyDown(KeyCode.G))
        {
            UnityEngine.UI.Toggle toggle = TogglePanel.instance.datavisToggle;
            toggle.isOn = !toggle.isOn;
        }
    }

    public float GetScore()
    {
        return score;
    }
}
