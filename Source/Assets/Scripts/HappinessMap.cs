﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HappinessMap
{
    public static bool isDisplaying = false;

    public static void InitMap()
    {
        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
                World.tiles[i, j].happinessFactor = 1;

        if (isDisplaying)
        {
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                    OverlayHandler.renderers[i, j].material.color = World.tiles[i, j].descriptor.material.color;
        }
    }

    public static void ToggleMap()
    {
        isDisplaying = !isDisplaying;

        if (isDisplaying)
        {
            if (WindController.isDisplaying)
                WindController.ToggleMap();

            if (OverlayHandler.lastOverlay != OverlayType.Happiness)
            {
                for (int i = 0; i < World.gridWidth; i++)
                    for (int j = 0; j < World.gridWidth; j++)
                        OverlayHandler.renderers[i, j].material.color = Color.Lerp(Color.red, World.tiles[i, j].descriptor.material.color, World.tiles[i, j].happinessFactor);
                OverlayHandler.lastOverlay = OverlayType.Happiness;
            }

            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                    if (World.hexagons[i, j].transform.position.y == 0)
                    {
                        OverlayHandler.renderers[i, j].enabled = true;
                        OverlayHandler.crosses[i, j].ForEach(r => r.enabled = false);
                    }
        }
        else
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                    OverlayHandler.renderers[i, j].enabled = false;
    }

    public static void UpdateMap()
    {
        if (isDisplaying)
        {
            for (int i = 0; i < World.gridWidth; i++)
                for (int j = 0; j < World.gridWidth; j++)
                    if (World.hexagons[i, j].transform.position.y == 0)
                        OverlayHandler.renderers[i, j].material.color = Color.Lerp(Color.red, World.tiles[i, j].descriptor.material.color, World.tiles[i, j].happinessFactor);
        }
        else if (OverlayHandler.lastOverlay == OverlayType.Happiness)
            OverlayHandler.lastOverlay = OverlayType.None;
    }
}
