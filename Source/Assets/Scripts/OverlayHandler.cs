﻿using UnityEngine;
using System.Collections.Generic;

public enum OverlayType
{
    None,
    Happiness,
    Wind,
    Building,
}

public class OverlayHandler
{
    public static Renderer[,] renderers = new Renderer[0, 0];
    public static List<Renderer>[,] crosses = new List<Renderer>[0, 0];
    public static OverlayType lastOverlay = OverlayType.None;

    public static void InitOverlay()
    {      
        for (int i = 0; i < renderers.GetLength(0); i++)
            for (int j = 0; j < renderers.GetLength(1); j++)
            {
                Object.Destroy(renderers[i, j].gameObject);
                crosses[i, j].ForEach(r => Object.Destroy(r.gameObject));
            }

        renderers = new Renderer[World.gridWidth, World.gridWidth];
        crosses = new List<Renderer>[World.gridWidth, World.gridWidth];

        for (int i = 0; i < World.gridWidth; i++)
            for (int j = 0; j < World.gridWidth; j++)
            {
                GameObject hexa = World.hexagons[i, j];
                GameObject newHexa = (GameObject)Object.Instantiate(World.instance.selectionHexa, hexa.transform.position + new Vector3(0, 1.53f, 0), Quaternion.identity);
                newHexa.transform.localScale = new Vector3(1.963f, 0.05f, 1.963f);
                newHexa.transform.parent = hexa.transform;
                newHexa.GetComponent<Renderer>().enabled = false;
                renderers[i, j] = newHexa.GetComponent<Renderer>();
                GameObject newUnbuildable = (GameObject)Object.Instantiate(World.instance.unbuildableCross, hexa.transform.position + new Vector3(0, 1.53f, 0), Quaternion.identity);
                newUnbuildable.transform.parent = hexa.transform;
                foreach (Renderer r in newUnbuildable.GetComponentsInChildren<Renderer>())
                    r.enabled = false;
                crosses[i, j] = new List<Renderer>(newUnbuildable.GetComponentsInChildren<Renderer>());
            }
    }
}
