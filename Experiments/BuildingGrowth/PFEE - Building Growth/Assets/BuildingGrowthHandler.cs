﻿using UnityEngine;
using System.Collections;

public class BuildingGrowthHandler : MonoBehaviour
{
	public Object cube;
	public float heightInterval = 0.3f;

    public int currentLevel = 0;
    private int targetLevel = 0;

    private float[] towerHeights;
	private float[] towerTargetHeights;
	private GameObject[] towers;
	private bool continueGrowth;

	// Use this for initialization
	void Start ()
	{
		towers = new GameObject[4];

		towers [0] = (GameObject) GameObject.Instantiate (cube, transform.position + new Vector3 (0.25f, 0, 0.25f), Quaternion.identity);
		towers [1] = (GameObject) GameObject.Instantiate (cube, transform.position + new Vector3 (0.25f, 0, -0.25f), Quaternion.identity);
		towers [2] = (GameObject) GameObject.Instantiate (cube, transform.position + new Vector3 (-0.25f, 0, 0.25f), Quaternion.identity);
		towers [3] = (GameObject) GameObject.Instantiate (cube, transform.position + new Vector3 (-0.25f, 0, -0.25f), Quaternion.identity);

		for (int i = 0; i < 4; i++) {
			towers [i].transform.localScale = new Vector3 (0f, 0f, 0f);
			towers [i].transform.parent = transform;
		}

		towerHeights = new float[4];
		for (int i = 0; i < 4; i++)
			towerHeights [i] = 0;

		towerTargetHeights = new float[4];
		for (int i = 0; i < 4; i++)
			towerTargetHeights [i] = 0;

		continueGrowth = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (targetLevel != currentLevel) {
			targetLevel = currentLevel;
			continueGrowth = true;

			for (int i = 0; i < 4; i++)
				towerTargetHeights [i] = targetLevel * heightInterval + Random.Range(0f, heightInterval);
		}

		if (continueGrowth) {
			continueGrowth = false;

			for (int i = 0; i < 4; i++) {
				if (Mathf.Abs(towerHeights [i] - towerTargetHeights [i]) > 0.02f) {
					float sign = Mathf.Sign (towerTargetHeights [i] - towerHeights [i]);
					towerHeights [i] += sign * 0.02f;
					continueGrowth = true;
					towers [i].transform.localScale = new Vector3(0.25f, towerHeights[i] * 10, 0.25f);
					towers [i].transform.Translate (0f, sign * 0.01f, 0f);
				}
			}
		}
	}

	// When clicked
	void OnMouseOver()
	{
		if (Input.GetMouseButtonDown (0))
			currentLevel++;
		else if (Input.GetMouseButtonDown (1))
			currentLevel = Mathf.Max(0, currentLevel - 1);
	}
}
