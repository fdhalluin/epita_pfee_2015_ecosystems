﻿using UnityEngine;
using System.Collections;

public class BoidController : MonoBehaviour
{
	public float minVelocity = 5;
	public float maxVelocity = 20;

	public float separationDist = 1.0f;
	public float neighborhoodRadius = 3.0f;

	public float steeringSpeed = 1.0f;
	public float maxVelocityDelta = 1.0f;

	public float cohesionFactor = 1.0f;
	public float separationFactor = 1.0f;
	public float alignmentFactor = 1.0f;
	public float targetFactor = 1.0f;

	public int flockSize = 20;

	public GameObject prefab;
	public GameObject chasee;
	public GameObject floor;

	public Vector3 flockCenter;
	public Vector3 flockVelocity;

	public GameObject[] boids { get; private set; }

	void Start()
	{
		boids = new GameObject[flockSize];
		for (int i = 0; i < flockSize; ++i)
		{
			Vector3 position = new Vector3
				(
					Random.value * GetComponent<Collider>().bounds.size.x,
					Random.value * GetComponent<Collider>().bounds.size.y,
					Random.value * GetComponent<Collider>().bounds.size.z
				) - GetComponent<Collider>().bounds.extents;

			GameObject boid = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
			boid.transform.parent = transform;
			boid.transform.localPosition = position;
			boid.GetComponent<Boid>().SetController(gameObject);
			boids[i] = boid;
		}
	}

	void Update()
	{
		Vector3 center = Vector3.zero;
		Vector3 velocity = Vector3.zero;

		foreach (GameObject boid in boids)
		{
			center = center + boid.transform.localPosition;
			velocity = velocity + boid.GetComponent<Rigidbody>().velocity;
		}

		flockCenter = center / (flockSize);
		flockVelocity = velocity / (flockSize);
	}
}