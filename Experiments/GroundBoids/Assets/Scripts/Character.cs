﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
    public enum EMovementType
    {
        CameraSpace,
        LocalSpace
    }

    public float RotationSpeed;
    public float MoveSpeed;
    public float RotationDamping;
    public EMovementType MovementType;

    private Vector3 _targetLookDirection;

    // Update is called once per frame
    void Update()
    {
        if (MovementType == EMovementType.CameraSpace)
        {
            MoveCameraSpace();
        }
        else
        {
            MoveLocalSpace();
        }
    }

    void MoveLocalSpace()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position = transform.position + transform.forward * Time.deltaTime * MoveSpeed;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position = transform.position - transform.forward * Time.deltaTime * MoveSpeed;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, -90 * Time.deltaTime * RotationSpeed, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, 90 * Time.deltaTime * RotationSpeed, 0);
        }
    }

    void MoveCameraSpace()
    {
        // Project camera vectors onto ground and normalize
        Vector3 forward = Camera.main.transform.forward;
        forward.y = 0;
        forward.Normalize();

        Vector3 right = Camera.main.transform.right;
        right.y = 0;
        right.Normalize();

        Vector3 moveDirection = Vector3.zero;

        if (Input.GetKey(KeyCode.UpArrow))
        {
            moveDirection += forward;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            moveDirection += -forward;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveDirection += -right;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            moveDirection += right;
        }

        transform.position = transform.position + moveDirection * Time.deltaTime * MoveSpeed;
        if (moveDirection.magnitude > 0)
            _targetLookDirection = moveDirection;

        // Slerp = Lerp = rotate towards movement direction gradually, with damping
        // You could use current = Lerp(current, target, dt * speed)
        // or current = Lerp(current, target, dt / (dt + damping))
        // #2 is more stable for large dt, but you should check for dt == 0 && damping == 0
        if (_targetLookDirection.magnitude > 0)
            transform.rotation = Quaternion.Slerp(transform.rotation,
                                                  Quaternion.LookRotation(_targetLookDirection),
                                                  Time.deltaTime / (Time.deltaTime + RotationDamping));
    }

    // Display debug info in editor
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.right);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + transform.up);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
    }
}
