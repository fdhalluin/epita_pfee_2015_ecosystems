﻿using UnityEngine;
using System.Collections;

public class BoidWatcher : MonoBehaviour
{
	public Transform boidController;

	void FixedUpdate()
	{
		if (boidController)
		{
			Vector3 watchPoint = boidController.GetComponent<BoidController>().flockCenter;
			GetComponent<OrbitCamera>().CenterPosition = watchPoint;
		}
	}
}