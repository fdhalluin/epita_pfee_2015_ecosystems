﻿using UnityEngine;
using System.Collections;

public class Boid : MonoBehaviour
{
	private GameObject controller;

	private float minVelocity;
	private float maxVelocity;

	private float separationDist;
	private float separationDistanceSqrRoot;
	private float neighborhoodRadius;

	private GameObject chasee;
	private GameObject floor;

	private float steeringSpeed;
	private float maxVelocityDelta;

	private float cohesionFactor;
	private float separationFactor;
	private float alignmentFactor;
	private float targetFactor;


	void Start()
	{
		separationDistanceSqrRoot = Mathf.Sqrt(separationDist);
	}

	void Update()
	{
		Vector3 velocity = GetComponent<Rigidbody>().velocity;

		// Compute target velocity from flocking rules
		Vector3 targetVelocity = CohesionVector() * cohesionFactor;
		targetVelocity += SeparationVector() * separationFactor;
		targetVelocity += AlignmentVector() * alignmentFactor;
		targetVelocity += FollowChasee() * targetFactor;

		targetVelocity.y = 0;
		targetVelocity = targetVelocity.normalized * maxVelocity;

		// Compute steering for smoother movement
		Vector3 steering = targetVelocity - velocity;
		if (steering.magnitude > maxVelocityDelta)
			steering = steering.normalized * maxVelocityDelta;

		velocity += steering * steeringSpeed;

		// Enforce minimum and maximum speeds for the boids
		float speed = velocity.magnitude;
		if (speed > maxVelocity)
			velocity = velocity.normalized * maxVelocity;
		else if (speed < minVelocity)
			velocity = velocity.normalized * minVelocity;

		GetComponent<Rigidbody>().velocity = velocity;
	}

	void FixedUpdate()
	{
		// Enforce ground position
		Vector3 pos = transform.position;
		pos.y = floor.transform.position.y + 0.5f;
		transform.position = pos;
	}

	/// <summary>
	/// Makes the boid move toward the center of mass of the flock.
	/// </summary>
	/// <returns>The normalized cohesion vector.</returns>
	private Vector3 CohesionVector()
	{
		Vector3 center = Vector3.zero;
		uint neighborsCount = 0;

		BoidController boidController = controller.GetComponent<BoidController>();
		foreach (GameObject boid in boidController.boids)
		{
			// Do not take self into account
			if (boid != this.gameObject)
			{
				if (Vector3.Distance(transform.localPosition ,boid.transform.localPosition) < neighborhoodRadius)
				{
					center += boid.transform.localPosition;
					++neighborsCount;
				}
			}
		}

		if (neighborsCount > 0)
			center /= neighborsCount;

		center -= transform.localPosition;
		return center.normalized;
	}

	/// <summary>
	/// Makes the boid avoid clos boids.
	/// </summary>
	/// <returns>The normalized separation vector.</returns>
	private Vector3 SeparationVector()
	{
		Vector3 separation = Vector3.zero;

		BoidController boidController = controller.GetComponent<BoidController>();
		foreach (GameObject boid in boidController.boids)
		{
			// Do not take self into account
			if (boid != this.gameObject)
			{
				// Vector3 headingVect = transform.localPosition - boid.transform.localPosition;
				// float magnitude = headingVect.magnitude;
				float dist =  Vector3.Distance(transform.localPosition, boid.transform.localPosition);
				if (dist * dist  < separationDist)
				{
					Vector3 headingVect = transform.localPosition - boid.transform.localPosition;
					// Calculate scale
					float scale = separationDistanceSqrRoot / headingVect.magnitude;
					separation += headingVect * scale;

				}
			}
		}

		return separation.normalized;
	}

	/// <summary>
	/// Makes the boid move in the same direction as as the flock.
	/// </summary>
	/// <returns>The normalized alignment vector.</returns>
	private Vector3 AlignmentVector()
	{
		Vector3 alignment = Vector3.zero;
		uint neighborsCount = 0;

		BoidController boidController = controller.GetComponent<BoidController>();
		foreach (GameObject boid in boidController.boids)
		{
			// Do not take self into account
			if (boid != this.gameObject)
			{
				if (Vector3.Distance(transform.localPosition, boid.transform.localPosition) < neighborhoodRadius)
				{
					alignment += boid.GetComponent<Rigidbody>().velocity;
					++neighborsCount;
				}
			}
		}

		if (neighborsCount > 0)
			alignment /= neighborsCount;

		return alignment.normalized;
	}

	private Vector3 FollowChasee()
	{
		Vector3 direction = chasee.transform.localPosition - transform.position;
		return direction.normalized;
	}

	/// <summary>
	/// Sets the controller.
	/// </summary>
	/// <param name="controller">The boid controller.</param>
	public void SetController(GameObject controller)
	{
		this.controller = controller;
		BoidController boidController = this.controller.GetComponent<BoidController>();

		minVelocity = boidController.minVelocity;
		maxVelocity = boidController.maxVelocity;

		separationDist = boidController.separationDist;
		neighborhoodRadius = boidController.neighborhoodRadius;

		steeringSpeed = boidController.steeringSpeed;
		maxVelocityDelta = boidController.maxVelocityDelta;

		cohesionFactor = boidController.cohesionFactor;
		separationFactor = boidController.separationFactor;
		alignmentFactor = boidController.alignmentFactor;
		targetFactor = boidController.targetFactor;

		chasee = boidController.chasee;
		floor = boidController.floor;
	}
}