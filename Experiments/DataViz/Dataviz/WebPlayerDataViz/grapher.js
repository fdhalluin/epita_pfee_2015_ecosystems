function UpdateFrequencies(text) {
	var freq = {};
	for (var i = 0, len = text.length; i < len; i++) {
		var c = text[i]
		if (!freq[c]) {
			freq[c] = 0
		}
		freq[c] += 1;
	}

	var data = [];
	for (var key in freq) {
		data.push([key, freq[key]]);
	}

    var margin = {top: 20, right: 20, bottom: 30, left: 40},
	width = 480 - margin.left - margin.right,
	height = 320 - margin.top - margin.bottom;

    var x = d3.scale.ordinal()
	.rangeRoundBands([0, width], .1);

    var y = d3.scale.linear()
	.range([height, 0]);

    var xAxis = d3.svg.axis()
	.scale(x)
	.orient("bottom");

    var yAxis = d3.svg.axis()
	.scale(y)
	.orient("left")
	.ticks(10);

	var container = d3.select("#chart-container");
	container.select("svg").remove();

    var svg = container.append("svg")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	x.domain(data.map(function(d) { return d[0]; }));
	y.domain([0, d3.max(data, function(d) { return d[1]; })]);

	svg.append("g")
	    .attr("class", "x axis")
	    .attr("transform", "translate(0," + height + ")")
	    .call(xAxis);

	svg.append("g")
	    .attr("class", "y axis")
	    .call(yAxis)
	    .append("text")
	    .attr("transform", "rotate(-90)")
	    .attr("y", 6)
	    .attr("dy", ".71em")
	    .style("text-anchor", "end")
	    .text("Count");

	svg.selectAll(".bar")
	    .data(data)
	    .enter().append("rect")
	    .attr("class", "bar")
	    .attr("x", function(d) { return x(d[0]); })
	    .attr("width", x.rangeBand())
	    .attr("y", function(d) { return y(d[1]); })
	    .attr("height", function(d) { return height - y(d[1]); });
}

