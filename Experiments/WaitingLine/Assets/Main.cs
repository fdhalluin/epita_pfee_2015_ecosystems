﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Main : MonoBehaviour
{
	List<KeyValuePair<string, float>> loggedEventsAndWeight = new List<KeyValuePair<string, float>> ();
	Dictionary<string, float> sumOfWeightForEvents = new Dictionary<string, float> ();

	string inputEvent = "Default text...";
	string inputWeight = "0.5";


	int[] peopleInQueues = new int[256];
	public int numberOfQueues = 3;
	public int averageApparitionFrameCount = 20;
	public int averageHandlingFrameCount = 120;

	public void OnGUI ()
	{
		for (int i = 0; i < numberOfQueues; i++) {
			for (int j = 0; j < peopleInQueues [i]; j++)
				GUI.Box (new Rect (j * 50, i * 50, 50, 50), "P");
		}
	}

	public void Update ()
	{
		if (Random.Range (0, averageApparitionFrameCount) == 0) {
			int selectedQueue = Random.Range (0, numberOfQueues);
			peopleInQueues [selectedQueue] += 1;
		}
		for (int i = 0; i < numberOfQueues; i++) {
			if (Random.Range (0, averageHandlingFrameCount) == 0) {
				if (peopleInQueues [i] > 0)
					peopleInQueues [i] -= 1;
			}
		}

		if (Input.GetKeyDown (KeyCode.Return))
			LogEvent ();
	}

	private void DisplayLoggedEvents ()
	{
		inputEvent = GUI.TextField (new Rect (10, 10, 200, 20), inputEvent);
		inputWeight = GUI.TextField (new Rect (10, 40, 200, 20), inputWeight);
		if (GUI.Button (new Rect (10, 70, 200, 20), "Log Event"))
			LogEvent ();

		GUILayout.BeginArea (new Rect (250, 10, 200, 500));
		GUILayout.BeginVertical ();
		GUILayout.Box ("10 Latest Events");
		for (int i = loggedEventsAndWeight.Count - 1, j = 0; j < 10 && i >= 0; j++, i--)
			GUILayout.Label (loggedEventsAndWeight [i].Value + " : " + loggedEventsAndWeight [i].Key);
		GUILayout.EndVertical ();
		GUILayout.EndArea ();

		GUILayout.BeginArea (new Rect (460, 10, 200, 500));
		GUILayout.BeginVertical ();
		GUILayout.Box ("Top Events");

		List<string> displayedKeys = new List<string> ();
		List<KeyValuePair<string, float>> orderedSums = new List<KeyValuePair<string, float>> ();
		while (displayedKeys.Count != sumOfWeightForEvents.Keys.Count) {
			float highestWeight = 0f;
			string highestString = "";
			foreach (string key in sumOfWeightForEvents.Keys)
				if (sumOfWeightForEvents [key] > highestWeight && !displayedKeys.Contains (key)) {
					highestWeight = sumOfWeightForEvents [key];
					highestString = key;
				}
			displayedKeys.Add (highestString);
			orderedSums.Add (new KeyValuePair<string, float> (highestString, highestWeight));
		}
		foreach (KeyValuePair<string, float> kvp in orderedSums)
			GUILayout.Label (kvp.Value + " : " + kvp.Key);

		GUILayout.EndVertical ();
		GUILayout.EndArea ();

		// Bar graph
		if (orderedSums.Count > 0) {
			float max = orderedSums [0].Value;
			for (int i = 0; i < orderedSums.Count; i++)
				GUI.Box (new Rect (10 + i * 50, 300 - 200 * (orderedSums [i].Value / max), 50, 200 * (orderedSums [i].Value / max)), orderedSums [i].Key);
		}
	}

	private void LogEvent ()
	{
		string loggedEvent = (string)inputEvent.Clone ();
		float loggedWeight = float.Parse (inputWeight);
		
		loggedEventsAndWeight.Add (new KeyValuePair<string, float> (loggedEvent, loggedWeight));
		
		if (sumOfWeightForEvents.ContainsKey (loggedEvent))
			sumOfWeightForEvents [loggedEvent] += loggedWeight;
		else
			sumOfWeightForEvents [loggedEvent] = loggedWeight;
	}
}
