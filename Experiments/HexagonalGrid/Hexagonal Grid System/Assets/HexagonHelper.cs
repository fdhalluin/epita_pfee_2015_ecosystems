﻿using UnityEngine;
using System.Collections;

public static class HexagonHelper
{
	public static float DistanceFromIndex (int i1, int j1, int i2, int j2)
	{
		float x1 = i1 * 1.7f + 0.85f * (j1 % 2);
		float y1 = 1.45f * j1;
		float x2 = i2 * 1.7f + 0.85f * (j2 % 2);
		float y2 = 1.45f * j2;

		return Mathf.Sqrt ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}

	public static float DistanceToPoint (int i1, int j1, float x2, float y2)
	{
		GameObject hexagon = World.hexagons [i1, j1];
		float x1 = hexagon.transform.position.x;
		float y1 = hexagon.transform.position.z;

		return Mathf.Sqrt ((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
	}
}
