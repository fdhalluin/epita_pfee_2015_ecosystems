﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	float sensibility = 0.25f;
	private Vector3 lastMouse = new Vector3 (0, 0, 0);

	void Update ()
	{
		// Angle camera
		if (Input.GetKey (KeyCode.Space)) {
			lastMouse = Input.mousePosition - lastMouse;
			lastMouse = new Vector3 (-lastMouse.y * sensibility, lastMouse.x * sensibility, 0);
			lastMouse = new Vector3 (transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
			transform.eulerAngles = lastMouse;
		}
		lastMouse = Input.mousePosition;

		if (Input.GetKey (KeyCode.Z) || Input.GetKey (KeyCode.W))
			transform.Translate (new Vector3 (0, 0, 1));
		if (Input.GetKey (KeyCode.S))
			transform.Translate (new Vector3 (0, 0, -1));
		if (Input.GetKey (KeyCode.Q) || Input.GetKey (KeyCode.A))
			transform.Translate (new Vector3 (-1, 0, 0));
		if (Input.GetKey (KeyCode.D))
			transform.Translate (new Vector3 (1, 0, 0));
	}
}
