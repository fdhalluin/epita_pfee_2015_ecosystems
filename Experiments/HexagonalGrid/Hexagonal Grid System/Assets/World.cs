﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World : MonoBehaviour
{
	public Object hexagon;
	public Object city;

	public float perlinX;
	public float perlinY;
	public float perlinScale;
	public bool onlyMountains;
	public bool deepenWater;
	public float heightMult;

	public int minCityDistance;

	public bool recompute;

	public static GameObject[,] hexagons;
	public static int gridWidth = 50;
	public static int gridHalfWidth = 25;

	public List<GameObject> cities;

	float maxCellHeight;

	void Start ()
	{
		RecomputeMap ();
	}

	void Update ()
	{
		if (recompute) {
			recompute = false;

			RecomputeMap ();
		}
	}

	void RecomputeMap ()
	{
		// Clean up
		List<GameObject> children = new List<GameObject> ();
		foreach (Transform child in transform)
			children.Add (child.gameObject);
		children.ForEach (child => DestroyImmediate (child));

		hexagons = new GameObject[gridWidth, gridWidth];

		for (int i = -gridHalfWidth; i < gridHalfWidth; i++)
			for (int j = -gridHalfWidth; j < gridHalfWidth; j++) {
				GameObject newHexa = (GameObject)GameObject.Instantiate (hexagon,
					                     new Vector3 (i * 1.7f + 0.85f * (j % 2), -2, 1.45f * j),
					                     Quaternion.identity);
				newHexa.transform.parent = transform;
				hexagons [gridHalfWidth + i, gridHalfWidth + j] = newHexa;
			}

		GenerateHeightMapPerlin ();
		ColorHeightMap ();
		AddCities ();
		CompactMap ();
	}

	void AddCities ()
	{
		cities = new List<GameObject> ();

		float cityLowThresh = -0.15f * maxCellHeight;
		float cityHighThresh = 0.25f * maxCellHeight;

		for (int i = 5; i < gridWidth - 5; i++)
			for (int j = 5; j < gridWidth - 5; j++) {

				Transform t = hexagons [i, j].transform;

				bool isOnFlatTerrain = true;
				for (int flatX = -1; flatX <= 1; flatX++)
					for (int flatY = -1; flatY <= 1; flatY++) {
						Transform temp = hexagons [i + flatX, j + flatY].transform;
						if (temp.position.y < cityLowThresh || temp.position.y > cityHighThresh) {
							isOnFlatTerrain = false;
							break;
						}
					}

				if (isOnFlatTerrain) {

					bool farFromOtherCities = true;
					foreach (GameObject c in cities)
						if (Mathf.Abs (c.transform.position.x - t.position.x) < minCityDistance && Mathf.Abs (c.transform.position.z - t.position.z) < minCityDistance) {
							farFromOtherCities = false;
							break;
						}

					if (farFromOtherCities) {
						GameObject newCity = (GameObject)GameObject.Instantiate (city, 
							                     new Vector3 (t.position.x, t.position.y + 1, t.position.z),
							                     Quaternion.identity);
						newCity.transform.parent = this.transform;
						cities.Add (newCity);
					}
				}
			}
	}

	void CompactMap ()
	{
		if (onlyMountains) {
			float threshold = 0.5f * maxCellHeight;
			float waterThreshold = -0.2999f * maxCellHeight;
			for (int i = 0; i < gridWidth; i++)
				for (int j = 0; j < gridWidth; j++) {
					Vector3 old = World.hexagons [i, j].transform.position;
					if (old.y < threshold) {
						if (old.y < waterThreshold)
							World.hexagons [i, j].transform.position = new Vector3 (old.x, -0.5f, old.z);
						else
							World.hexagons [i, j].transform.position = new Vector3 (old.x, 0, old.z);
					} else
						World.hexagons [i, j].transform.position = new Vector3 (old.x, (old.y - threshold + 0.4f) * heightMult, old.z);
				}
		} else {
			for (int i = 0; i < gridWidth; i++)
				for (int j = 0; j < gridWidth; j++) {
					Vector3 old = World.hexagons [i, j].transform.position;
					World.hexagons [i, j].transform.position = new Vector3 (old.x, old.y * heightMult, old.z);
				}
		}
	}

	void GenerateHeightMapPeaks ()
	{
		for (int peak = 0; peak < 10; peak++) {
			int i2 = Random.Range (0, gridWidth);
			int j2 = Random.Range (0, gridWidth);
			float heightMult = Random.Range (2f, 10f);

			for (int i = 0; i < gridWidth; i++)
				for (int j = 0; j < gridWidth; j++) {
					float dist = HexagonHelper.DistanceFromIndex (i, j, i2, j2);
					World.hexagons [i, j].transform.Translate (new Vector3 (0,
						heightMult * 20 / (20 + dist),
						0));
				}
		}

		CenterHeightMap ();
	}

	void GenerateHeightMapPerlin ()
	{
		for (int i = 0; i < gridWidth; i++)
			for (int j = 0; j < gridWidth; j++)
				World.hexagons [i, j].transform.Translate (new Vector3 (0,
					5 * Mathf.PerlinNoise (perlinX + i * perlinScale, perlinY + j * perlinScale),
					0));

		CenterHeightMap ();
	}

	void CenterHeightMap ()
	{
		float avgHeight = 0;
		for (int i = 0; i < gridWidth; i++)
			for (int j = 0; j < gridWidth; j++)
				avgHeight += World.hexagons [i, j].transform.position.y;

		avgHeight /= (gridWidth * gridWidth);
		Vector3 translate = new Vector3 (0, -avgHeight, 0);

		for (int i = 0; i < gridWidth; i++)
			for (int j = 0; j < gridWidth; j++) {
				World.hexagons [i, j].transform.Translate (translate);
				if (World.hexagons [i, j].transform.position.y > maxCellHeight)
					maxCellHeight = World.hexagons [i, j].transform.position.y;
			}
	}

	void ColorHeightMap ()
	{
		float waterThreshold = -0.3f * maxCellHeight;
		float sandThreshold = -0.2f * maxCellHeight;
		float lightGrassThreshold = 0.3f * maxCellHeight;
		float darkGrassThreshold = 0.5f * maxCellHeight;
		float stoneThreshold = 0.7f * maxCellHeight;

		Color darkGreen = new Color (0, 0.7f, 0);

		for (int i = 0; i < gridWidth; i++)
			for (int j = 0; j < gridWidth; j++) {
				Transform t = World.hexagons [i, j].transform;
				float h = t.position.y;
					
				Color col;
				if (h < waterThreshold) {
					col = Color.blue;
					t.position = new Vector3 (t.position.x, waterThreshold, t.position.z);
				} else if (h < sandThreshold)
					col = Color.yellow;
				else if (h < lightGrassThreshold)
					col = Color.green;
				else if (h < darkGrassThreshold)
					col = darkGreen;
				else if (h < stoneThreshold)
					col = Color.gray;
				else
					col = Color.white;

				t.GetChild (0).GetComponent<Renderer> ().material.color = col;
				t.GetChild (1).GetComponent<Renderer> ().material.color = col;
				t.GetChild (2).GetComponent<Renderer> ().material.color = col;
			}
	}
}
