﻿using UnityEngine;
using System.Collections;

public class HexagonController : MonoBehaviour
{
	// When clicked
	void OnMouseOver ()
	{
		if (Input.GetMouseButtonDown (0)) {
			for (int i = 0; i < World.gridWidth; i++)
				for (int j = 0; j < World.gridWidth; j++) {
					float dist = HexagonHelper.DistanceToPoint (i, j, transform.position.x, transform.position.z);
					World.hexagons [i, j].transform.Translate (new Vector3 (0,
						10 / (10 + dist * dist),
						0));
				}
		}
	}
}
